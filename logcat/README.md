logcat
======

`logcat.sh` is a shell tool to help categorize build errors from log files by
interactive open coding (coding comes from qualitative research theory and
refers to the exploration and labeling of things).

Requirements
------------

* Running MongoDB server (with build data gathered using the crawler)
* Running Redis server (used for caching the categorizations)
* [jq](https://stedolan.github.io/jq/) - command-line JSON processor
* redis-cli
* mongo

Setup
-----

Edit the `logcat.properties` file according to your system. Mainly you will
have to point `REDIS_CLI` and `MONGO` to the location of the `redis-cli` and
`mongo` executable on your system respectively.

Usage
-----

Best to alias the script for easier usage before you get started

    alias lc="$(pwd)/logcat.sh"

Then run

    lc help

to get some

    lc meta

is also useful to see what data you currently have

Notes
-----

A note on log storage: logs are stored in gzipped format. When initializing a
session, all logs are unzipped for better `grep` performance. When clearing a
session, logs are zipped again to save disk space.

Why is this a shell script? Great question. It sort of grew...
