#!/bin/bash

source ./logcat.properties

# set backup defaults
if [ -z $JARVIS_LOG_DIR ]; then
    JARVIS_LOG_DIR="/data/jarvis/logs"
fi
if [ -z $REDIS_CLI ]; then
    REDIS_CLI="redis-cli"
fi
if [ -z $MONGO ]; then
    MONGO="mongo"
fi

c_black=$(tput setaf 0)
c_red=$(tput setaf 1)
c_green=$(tput setaf 2)
c_yellow=$(tput setaf 3)
c_lime_yellow=$(tput setaf 190)
c_powder_blue=$(tput setaf 153)
c_blue=$(tput setaf 4)
c_magenta=$(tput setaf 5)
c_cyan=$(tput setaf 6)
c_white=$(tput setaf 7)
c_bright=$(tput bold)
c_normal=$(tput sgr0)
c_blink=$(tput blink)
c_reverse=$(tput smso)
c_underline=$(tput smul)

True="${c_green}True${c_normal}"
False="${c_red}False${c_normal}"

assert_preconditions() {
    if [ ! -d $JARVIS_LOG_DIR ]; then
        echo "$JARVIS_LOG_DIR does not exist or is not a directory"
        exit 1
    fi
    if ! which $REDIS_CLI > /dev/null || ! $REDIS_CLI --version | grep "redis-cli" > /dev/null; then
        echo "REDIS_CLI needs to point to the redis-cli executable"
        echo "REDIS_CLI is currently $REDIS_CLI"
        exit 1
    fi
    if ! which $MONGO > /dev/null || ! $MONGO --version | grep "MongoDB shell" > /dev/null; then
        echo "MONGO needs to point to the mongo executable"
        echo "MONGO is currently $MONGO"
        exit 1
    fi
}

usage() {
    echo "Usage: logcat [COMMAND] [OPTION...]"
    echo "Categorize logs by their error types"
    echo
    echo "Some commands:"
    echo "  help          show this usage message"
    echo "  meta          show a table of all repositories in the MongoDB and two"
    echo "                columns indicating if the log dirs exist and if they have"
    echo "                been categorized"
    echo "  init <slug>   initialize a session to categorize logs of the given repo"
    echo "  info          show infos about the current session"
    echo "  dump          save the current categorization into $JARVIS_LOG_DIR"
    echo "  sample-uncat  give a random sample of an uncategorized log"
    echo "                use sample-uncat-tail to show the tail of the sample"
    echo "  search <pattern> search all uncategorized logs for the given pattern"
    echo "  categorize <pattern> <name> add all logs that contain the given pattern to"
    echo "                the given category name"
    echo "  clear-session clear the current session"
}

error() {
    echo "[ERROR]:" "$@"
    exit 1
}

redis() {
    $REDIS_CLI "$@"
}

predis() {
    $REDIS_CLI "$@" | cut -d')' -f 1
}

meval() {
    $MONGO --quiet jarvis --eval "$@"
}

require_redis() {
    if ! redis PING > /dev/null; then
        error "redis-server does not appear to be running"
    fi
}

require_session() {
    assert_preconditions
    require_redis

    rid=$(predis GET "logcat:session")

    if [ $rid ]; then
        export logs=$JARVIS_LOG_DIR/$rid
    fi

    if [ ! $logs ]; then
        echo "Please run init <slug> first to begin a categorization session"
        exit 2
    fi

    if [ ! -d $logs ]; then
        error "log directory $logs does not exist"
    fi
}


meta() {
    echo "${c_bright}Logdir:${c_normal} $JARVIS_LOG_DIR"
    echo
    echo "${c_bright}      id   logs  model slug${c_normal}"
    for obj in $(meval "db.repositoryjsonobject.find({},{ _id: 0, id: 1, slug: 1})" | jq . -c); do
        rid=$(echo $obj | jq .id)
        slug=$(echo $obj | jq .slug | cut -d'"' -f 2)

        logs=$False
        model=$False
        if [ -d $JARVIS_LOG_DIR/$rid ]; then logs=$True; fi
        if [ -f $JARVIS_LOG_DIR/logcat.$rid.json ]; then model=$True; fi

        printf "%8s %17s %17s %s\n" $rid $logs $model $slug
    done
}

id2log() {
    awk -v logdir="$logs" '{ print logdir "/" $1 ".txt"}'
}

log2id() {
    xargs basename -a | cut -d'.' -f 1
}

init() {
    if [ ! $1 ]; then
        echo "Usage: init <slug>"
        exit 2
    fi

    rid=$(meval "db.repositoryjsonobject.find({ slug: \"$1\" }, { _id: 0, id: 1 })" | jq ".id")

    if [ ! $rid ]; then
        error "No repository named '$1' found"
    fi

    echo "Initializing $1 ($rid)"

    redis SET logcat:session $rid > /dev/null
    require_session

    echo "Unzipping..."
    gunzip $logs/*.gz

    echo "Building index..."
    for id in $(find $logs/* -printf "%f\n" | cut -d'.' -f 1)
    do
        redis sadd all "$id" > /dev/null
    done

    redis scard all
    rebuild-uncat
}

rebuild-uncat() {
    lua="redis.call('sdiffstore', 'uncategorized', 'all', unpack(redis.call('hkeys', 'categories')));"
    redis EVAL "$lua" 0 > /dev/null

    redis scard uncategorized
}

ls-uncat() {
    redis smembers uncategorized | id2log
}

search() {
    grep -e "$1" $(ls-uncat)
}

search-stats() {
    found=$(grep -l -e "$1" -r $logs | wc -l)
    total=$(redis scard all)

    echo "$found / $total"
    echo "$found $total" | awk '{ printf ("%.0f%\n", $1/$2*100) }'
}

search-stats-mongo() {
    ids=$(grep -l -e "$1" -r $logs | log2id)

    found=$(echo "${ids[@]}" | wc -l)
    total=$(redis scard all)

    echo "$found / $total"
    echo "$found $total" | awk '{ printf ("%.0f%\n", $1/$2*100) }'

    idlist="[$(echo $ids | tr ' ' ',')]"
    builds=$(meval "db.buildjsonobject.find({ job_ids: { \$in: $idlist } }, { _id: 0, id: 1, duration: 1, state: 1, number: 1 }).sort({number:1}).toArray()")
    echo "$builds" | jq -r '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
}

search-sample() {
    grep -l -e "$1" $(ls-uncat) | sort -R | head -n1
}

search-sample-tail() {
    tail -n70 $(search-sample "$@")
}

search-sample-subl() {
    subl $(grep -l -e "$1" $(ls-uncat) | sort -R | head -n1)
}

sample-uncat() {
    redis srandmember uncategorized 1 | id2log
}

sample-uncat-tail() {
    tail -n70 $(sample-uncat)
}

sample-uncat-subl() {
    subl $(sample-uncat)
}

del-category() {
    key="category.$1"
    redis del "$key"
    redis hdel "categories" "$key"
    rebuild-uncat
}

clear-categories() {
    for k in $(redis hkeys categories | cut -f 1)
    do
        redis del $k
    done

    redis del categories
    rebuild-uncat
}

clear-session() {
    echo "Clearing redis..."
    clear-categories
    redis del all
    redis del uncategorized
    redis del logcat:session

    echo "Zipping..."
    gzip $logs/*.txt
}

info() {
    total=$(predis scard all)

    echo "Logs: $logs"
    echo "=================================="
    echo "Total:         $total"
    echo "Uncategorized: $(predis scard uncategorized)"
    echo
    echo "Categories"
    echo "=================================="

    ctot=0
    for key in $(predis hkeys "categories")
    do
        kcard=$(predis scard $key)
        ctot=$(( $ctot+$kcard ))
        perc=$(echo "$kcard $total" | awk '{ printf ("%.0f%\n", $1/$2*100) }')
        echo "$key: $kcard ($perc)"
        echo "         $(predis hget categories $key)"
    done
    echo "---------------------------------- $ctot"
}

model-json() {
    lua="local arr = {}
        for _,key in ipairs(redis.call('hkeys', 'categories')) do
            arr[key] = redis.call('smembers', key)
        end
        return cjson.encode(arr)
        "

    redis EVAL "$lua" 0 | jq .
}

dump() {
    lua="local arr = {}
        for _,key in ipairs(redis.call('hkeys', 'categories')) do
            arr[key] = {}
            arr[key]['pattern'] = redis.call('hget', 'categories', key)
            arr[key]['logs']    = redis.call('smembers', key)
        end

        return cjson.encode(arr)
        "

    rid=$(predis get logcat:session)
    redis EVAL "$lua" 0 | jq . > $JARVIS_LOG_DIR/logcat.$rid.json
}

## categorize <pattern> <name>
categorize() {
    if [ 1 -eq $(predis exists "category.$2") ]; then
        pattern="$(predis hget categories "category.$2") OR $1"
    else
        pattern="$1"
    fi

    grep -l -e "$1" $(ls-uncat) | log2id | xargs $REDIS_CLI sadd "category.$2"
    redis hset categories "category.$2" "$pattern"
    rebuild-uncat
}

## some shorthands

c()   { categorize "$@"; }
s()   { search "$@"; }
sut() { sample-uncat-tail "$@"; }
sus() { sample-uncat-subl "$@"; }

## main

if [ ! $1 ]; then
    usage
    exit 2
fi

case "$1" in
    "help")   usage; exit 0 ;;
    "--help") usage; exit 0 ;;
    "meta")   meta; exit 0  ;;
    "init")   assert_preconditions; require_redis; ;;
    *)        require_session ;;
esac

"$@"
