#!/bin/bash

USERNAME="ec2-user"
HOSTNAME="jarvis-core.duckdns.org"

TMPDIR=$(mktemp -d)

if ! pgrep mongod; then
	echo "Make sure mongod is running"
	read
fi

echo "Dumping remote data..."
mongodump -h "$HOSTNAME" -d "jarvis" --gzip -o $TMPDIR

echo "Restoring locally..."
mongorestore --gzip -d "jarvis" $TMPDIR/jarvis --drop > /dev/null

echo "Cleanup..."
rm -r $TMPDIR

echo "Pulling logs"
rsync -avz $USERNAME@$HOSTNAME:/data/jarvis/logs/ /data/jarvis/logs
