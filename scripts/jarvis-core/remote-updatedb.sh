#!/bin/bash

USERNAME="ec2-user"
HOSTNAME="jarvis-core.duckdns.org"

SCRIPT=./updatedb.sh

if [ ! -f $SCRIPT ]; then
	echo "Script $SCRIPT not found"
	exit 1
fi

if ! ssh -q $USERNAME@$HOSTNAME exit; then
	echo "$HOSTNAME appears to be down"
	exit 1
fi

echo "Running $SCRIPT remotely..."
ssh $USERNAME@$HOSTNAME 'bash -s' < $SCRIPT
