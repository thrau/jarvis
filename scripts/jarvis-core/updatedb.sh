#!/bin/bash

echo "Starting mongod"
if ! pgrep mongod; then
	sh $HOME/start-mongo.sh
fi

echo "Starting redis-server"
if ! pgrep redis-server; then
	sh $HOME/start-redis.sh
fi

echo "Starting jarvis"
if ! pgrep java; then
	sh $HOME/start-jarvis.sh

	echo "Waiting a few seconds for jarvis to start ..."
	sleep 10
fi

echo "Sending updatedb command to jarvis via CRaSH ..."
echo "jarvis updatedb" | sshpass -p "password" ssh -tt -p 2000 user@localhost

tail -f /tmp/jarvis.log
