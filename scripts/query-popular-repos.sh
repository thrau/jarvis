#!/usr/bin/zsh

## This script fetches popular java repos via the github api and then queries their state on travis

get_popular_repos() {
    cache=/tmp/get_popular_repos

    if [ ! -f $cache ]; then
        touch $cache

        query="https://api.github.com/search/repositories?q=language%3Ajava&sort=stars&order=desc"

        for i in `seq 1 5`
        do
            next="$query&page=$i"
            curl -s $next | jq -r '.items[].full_name' >> $cache
        done
    fi

    cat $cache
}

query_travis() {
    url="https://api.travis-ci.org/repos/$1"
    header="Accept: application/vnd.travis-ci.2+json"

    curl -H $header -s $url
}

for repo in $(get_popular_repos)
do
    obj=$(query_travis $repo)

    builds=$(echo $obj | jq -r '.repo.last_build_number')

    echo "$repo,$builds"
done
