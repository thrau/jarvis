#!/usr/bin/zsh

mongo="/opt/mongodb/bin/mongo"

get_repositories() {
    $mongo --quiet jarvis <<EOF    
    db.repositoryjsonobject.find().map(function (repo) {
        return { id: repo.id, slug: repo.slug }
    });
EOF
}

get_slugs() {
    get_repositories | jsawk -e 'return this.slug' -a 'return this.join("\n")'
}


get_slug() {
    id=$1
    get_repositories | jsawk "if (this.id !== $id) return null" -a 'return this[0]' -a 'return this.slug'
}

get_ids() {
    get_repositories | jsawk -e 'return this.id' -a 'return this.join("\n")'
}

clone_repositories() {
    for repo in `get_slugs`
    do
        mkdir -p repos/$repo

        git clone https://github.com/$repo.git repos/$repo &
    done
}


get_commits() {
    id=$1

    arr=$($mongo --quiet jarvis <<EOF
    db.buildjsonobject.aggregate([
        {
            \$match: {
                "repository_id": $id,
                "state": { \$in: ["passed"] }
            }
        },
        {
            \$project: {
                sha: "\$commit.sha"
            }
        }
    ]).map(function(doc) { return doc.sha });
EOF
)
    echo $arr | jsawk -a 'return this.join("\n")'
}

for id in `get_ids`
do
    slug=$(get_slug $id)
    dir=repos/$slug

    if [ ! -d $dir ]; then
        echo "$dir does not exist"
        break;
    fi

    ok=0
    nok=0

    for sha in $(get_commits $id)
    do
        if  git -C $dir cat-file -e $sha 2> /dev/null
        then
            ok=$((ok+1))
        else
            nok=$((nok+1))
        fi
    done

    prc=$(printf '%.0f' $(( $nok. / ($nok+$ok) * 100 )))

    echo "$slug [$nok/$ok ($prc%)]"
done
