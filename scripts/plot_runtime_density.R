library(ggplot2)

plotRuntimeDensity <- function(repo, dat) {
    rdf <- subset(dat, slug == repo)

    colors = c("canceled" = "gray", "errored" = "red", "failed" = "orange", "passed" = "green")

    ggplot(rdf, aes(x = duration)) +
        geom_density(aes(group=state, color = state)) +
        scale_color_manual(name="Result", values = colors) +
        xlab("duration (s)") +
        ggtitle(paste("Runtime duration distribution\n\n", repo)) +
        theme_bw()
}

save_plotRuntimeDensity <- function(repo, dat) {
    name <- paste("runtime_density_", gsub("/", "-", repo), ".svg", sep="")
    path <- file.path("/home/thomas/uni/da/data/graphs", name)
    plot <- plotRuntimeDensity(repo, dat)

    svg(path, width=16, height=11)
    print(plot)
    dev.off()
}

df <- read.csv(
    "/home/thomas/uni/da/data/buildstats.csv",
    header = TRUE,
    sep = ";",
    colClasses=c('numeric','factor','numeric','numeric','factor','factor','factor','numeric'),
    row.names=NULL
)

# out, liars!
df <- df[ df$duration < quantile(df$duration, 0.95), ]

repos <- unique(df$slug)

for (repo in repos) {
    save_plotRuntimeDensity(repo, df)
}
