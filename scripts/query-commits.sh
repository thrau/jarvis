#!/bin/bash

mongo="/opt/mongodb/bin/mongo"
meval="$mongo --quiet jarvis --eval"

repos=$($mongo --quiet jarvis <<EOF    

db.repositoryjsonobject.find().forEach(function (repo) {
    print(repo.id + "," + repo.slug);
});
EOF
)

for repo in $repos;
do
    id=$(echo $repo | cut -d',' -f 1)
    name=$(echo $repo | cut -d',' -f 2)

    echo $repo;

    cmd="db.buildjsonobject.aggregate([{ \$match: { 'repository_id': $id} }, { \$match: { 'event_type': 'push', 'state': {\$in: ['failed', 'errored'] }} }, { \$project: { sha: '\$commit.sha'} } ]).map(function(doc){ return doc.sha })"

    $meval "printjson($cmd)" > commits/commits_$id.js
done
