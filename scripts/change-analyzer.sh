#!/usr/bin/zsh

autoload colors
colors

alias mongo="/opt/mongodb/bin/mongo --quiet jarvis"
alias meval="mongo --eval"

get_repository_id() { 
    meval "db.repositoryjsonobject.find({ 'slug': '$1' }).map(function(item) { return item.id })[0]"
}

get_builds() {
    id=$1

    mongo <<EOF    
    db.buildjsonobject.aggregate([
    {\$match: {
        "event_type": "push",
        "repository_id": $id
    }},
    {\$project: {
        "_id": 0,
        "id": 1,
        "state": 1,
        "event_type": 1,
        "commit.sha": "\$commit.sha",
        "commit.branch": "\$commit.branch",
        "commit.author_email": "\$commit.author_email",
        "commit.compare_url": "\$commit.compare_url"
    }}
    ]).toArray();
EOF
}


slug="orientechnologies/orientdb"

repo="repos/$slug"


for obj in $(get_builds $(get_repository_id $slug) | jq -c '.[]')
do
    url=$(echo $obj | jq -r '.commit.compare_url')

    pattern="([a-z0-9]+\.\.\.[a-z0-9]+)$"

    if [[ ! $url =~ $pattern ]]; then
        # /compare/2.1.6
        # /compare/ee7a4268cbca^...84a655fce06d
        # /commit/94b7b1e67f8e
        continue;
    fi

    range=$match[1]

    if ! git -C $repo rev-parse $range &> /dev/null; then
        # commit range doesn't exist
        continue;
    fi

    sha=$(echo $obj | jq -r '.commit.sha')
    state=$(echo $obj | jq -r '.state')
    len_range="$(git -C $repo log --oneline $range | wc -l)"
    len_parents=$(git -C $repo cat-file -p $sha | grep parent | wc -l)

    if [ $len_parents -gt 1 ]; then
        # merge commit, those are special
        continue;
    fi


    # echo $fg[blue]"[$sha]"$reset_color

    print_stats() {
        movs=$(git -C $repo diff --diff-filter=R --name-status -M $range | wc -l)
        adds=$(git -C $repo diff --diff-filter=A --name-status -M $range | wc -l)
        dels=$(git -C $repo diff --diff-filter=D --name-status -M $range | wc -l)
        mods=$(git -C $repo diff --diff-filter=M --name-status -M $range | wc -l)

        #echo "$adds,$dels,$mods,$movs"
        diffstat=$(git -C $repo diff --stat $range | tail -n 1)

        inserts=$(echo $diffstat | grep -o -P "([0-9]+) insert" | grep -o "[0-9]\+")
        deletions=$(echo $diffstat | grep -o -P "([0-9]+) delet" | grep -o "[0-9]\+")

        if [ ! $inserts ]; then inserts=0; fi
        if [ ! $deletions ]; then deletions=0; fi
        
        echo "$state,$movs,$adds,$dels,$mods,$inserts,$deletions"
    }

    print_stats

done
