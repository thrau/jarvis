Scripts
=======

This is a collection of both useful and useless scripts. Some were created
with a specific purpose, some were created for playing around with ideas, and
were then kept for the sake of not deleting them. Which makes no sense. Yet
neither do most of these scripts.

* `query-popular-repos.sh`

  Fetches Java repositories from GitHub ordered by their popularity, and does
  a join-query on Travis-CI to fetch the amount of builds the repo has
  executed.

* `buildrepos.py`

  Lists the repositories contained in the MongoDB, and displays whether for
  each repository, a error categorization and file-type categorization model
  exists.

* TODO: add more descriptions ...
