#!/usr/bin/env python3
import os.path

import pymongo

client = pymongo.MongoClient("mongodb://localhost:27017")

for repo in client.jarvis.repositoryjsonobject.find():
    rid = repo['id']
    exists = os.path.exists("/data/jarvis/logs/logcat.%d.json" % rid)
    filetypes = os.path.exists("/data/jarvis/filetypes/filetypes.%d.py" % rid)
    print("%7s %-38s (%s,%s)" % (rid, repo['slug'], exists, filetypes))

client.close()
