JARVIS
======

Just A Rather Very Irrelevant System

Jarvis is a [polyglot] system that contains applications and code to perform,
or assist with, the following tasks:

* Crawl over Travis-CI repositories using [travis4j] and store data in MongoDB
* Categorize logs by their error types
* Data extraction over git repository and build data
* Export data for exploratory analysis using R or WEKA

  [polyglot]: http://memeagora.blogspot.co.at/2006/12/polyglot-programming.html
  [travis4j]: https://github.com/thrau/travis4j

Modules
-------

Most modules require build-data access and thus rely on a running MongoDB and
Redis instance. Best to keep them running while doing stuff with jarvis.

### crawler

The crawler is a spring-boot application that allows a user to issue commands
to crawl Travis-CI for data.

### logcat

`logcat.sh` is a shell tool to help categorize build errors from log files by
interactive open coding (coding comes from qualitative research theory and
refers to the exploration and labeling of things).

### scripts

A collection of scripts and hacks. Some are useful, some are not and just kept
for documentation purposes. Also contains R scripts to generate graphs from
exported csv data (difficult to use at the moment).

### vision

The data analysis framework written in python. It contains various executables
that allow data preparation and extraction.

Workflow
--------

The goal is to gather data, do data transformation, data extraction and export
the data in a way that it can be used by machine learning tools, such as WEKA,
to do data mining and predictions over the data.

To that end, a workflow using JARVIS could look as follows:

* Start the crawler and resolve build and log data of repositories
* Clone the corresponding git repositories
* Categorize the downloaded Travis logs using logcat
* Enrich the build-data stored in the MongoDB with the generated categories
* (Exploratory data analysis with various R helpers)
* Data extraction from revision control and build data using vision
* Import the extracted data into your data mining tool (e.g. WEKA) to do magic
