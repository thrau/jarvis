package org.rauschig.jarvis.mongo;

import java.util.List;

import org.travis4j.model.Entity;

/**
 * WriteManyRequest.
 */
public class WriteManyRequest<T extends Entity> extends AbstractWriteRequest<List<T>> {

    public WriteManyRequest(List<T> payload) {
        super(payload);
    }

    @Override
    public void accept(WriteRequestVisitor visitor) {
        visitor.visit(this);
    }
}
