package org.rauschig.jarvis.mongo;

import java.util.List;

import org.travis4j.model.Entity;

/**
 * UpdateManyRequest.
 */
public class UpdateManyRequest<T extends Entity> extends AbstractWriteRequest<List<T>> {

    public UpdateManyRequest(List<T> payload) {
        super(payload);
    }

    @Override
    public void accept(WriteRequestVisitor visitor) {
        visitor.visit(this);
    }
}
