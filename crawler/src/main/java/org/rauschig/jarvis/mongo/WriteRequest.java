package org.rauschig.jarvis.mongo;

/**
 * WriteRequest.
 */
public interface WriteRequest<T> {
    T getPayload();

    void accept(WriteRequestVisitor visitor);
}
