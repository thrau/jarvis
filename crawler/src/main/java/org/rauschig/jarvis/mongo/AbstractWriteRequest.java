package org.rauschig.jarvis.mongo;

import com.google.common.base.MoreObjects;

/**
 * AbstractWriteRequest.
 */
public abstract class AbstractWriteRequest<T> implements WriteRequest<T> {

    private T payload;

    public AbstractWriteRequest(T payload) {
        this.payload = payload;
    }

    @Override
    public T getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("payload", payload)
                .toString();
    }
}
