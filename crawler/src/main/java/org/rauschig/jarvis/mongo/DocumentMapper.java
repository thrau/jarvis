package org.rauschig.jarvis.mongo;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bson.Document;
import org.travis4j.EntityVisitor;
import org.travis4j.model.Build;
import org.travis4j.model.Commit;
import org.travis4j.model.Entity;
import org.travis4j.model.Job;
import org.travis4j.model.Log;
import org.travis4j.model.Repository;
import org.travis4j.model.User;
import org.travis4j.model.json.AbstractJsonObject;

/**
 * DocumentMapper.
 */
public class DocumentMapper {

    public Document toDocument(Entity entity) {
        Converter converter = new Converter();
        entity.accept(converter);
        return converter.getResult();
    }

    public static class Converter implements EntityVisitor {
        public Document result;

        public Document getResult() {
            return result;
        }

        public Document convert(Entity entity) {
            return convert((AbstractJsonObject) entity); // FIXME unchecked type
        }

        public Document convert(AbstractJsonObject json) {
            // FIXME performance
            return Document.parse(json.getJson().toString());
        }

        @Override
        public void visit(Repository entity) {
            result = convert(entity);
            result.put("_id", entity.getId());
        }

        @Override
        public void visit(User entity) {
            result = convert(entity);
            result.put("_id", entity.getId());
        }

        @Override
        public void visit(Build entity) {
            result = convert(entity);
            result.get("config", Document.class).remove(".result");
            result.put("_id", entity.getId());
            result.put("commit", convert(entity.getCommit()));
        }

        @Override
        public void visit(Commit entity) {
            result = convert(entity);
            result.put("_id", entity.getId());
        }

        @Override
        public void visit(Log entity) {
            result = convert(entity);
            result.put("_id", entity.getId());

            Stream<String> body = entity.getBody();
            if (body != null) {
                result.put("body", body.collect(Collectors.toList()));
            }
        }

        @Override
        public void visit(Job entity) {
            result = convert(entity);
            result.put("_id", entity.getId());
            result.get("config", Document.class).remove(".result");
        }
    }

}
