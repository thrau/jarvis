package org.rauschig.jarvis.mongo;

import org.travis4j.model.Entity;

/**
 * WriteOneRequest.
 */
public class WriteOneRequest<T extends Entity> extends AbstractWriteRequest<T> {

    public WriteOneRequest(T payload) {
        super(payload);
    }

    @Override
    public void accept(WriteRequestVisitor visitor) {
        visitor.visit(this);
    }
}
