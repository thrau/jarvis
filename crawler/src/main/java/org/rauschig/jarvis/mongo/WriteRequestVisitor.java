package org.rauschig.jarvis.mongo;

/**
 * WriteRequestVisitor.
 */
public interface WriteRequestVisitor {
    void visit(WriteOneRequest<?> request);

    void visit(WriteManyRequest<?> request);

    void visit(UpdateManyRequest<?> request);

    void visit(UpdateOneRequest<?> request);
}
