package org.rauschig.jarvis.mongo;

import org.travis4j.model.Entity;

/**
 * UpdateOneRequest.
 */
public class UpdateOneRequest<T extends Entity> extends AbstractWriteRequest<T> {

    public UpdateOneRequest(T payload) {
        super(payload);
    }

    @Override
    public void accept(WriteRequestVisitor visitor) {
        visitor.visit(this);
    }
}
