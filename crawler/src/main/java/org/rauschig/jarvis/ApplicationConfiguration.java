package org.rauschig.jarvis;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.bson.Document;
import org.rauschig.jarvis.comm.LocalResultQueueLocator;
import org.rauschig.jarvis.comm.RequestEnvelope;
import org.rauschig.jarvis.comm.ResultQueueLocator;
import org.rauschig.jarvis.config.MongoConfiguration;
import org.rauschig.jarvis.config.RedissonConfiguration;
import org.rauschig.jarvis.mongo.WriteRequest;
import org.rauschig.jarvis.travis.FailedRequest;
import org.rauschig.jarvis.travis.LogDownloadRequest;
import org.rauschig.jarvis.travis.Request;
import org.rauschig.jarvis.travis.Result;
import org.redisson.RedissonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.travis4j.TravisClient;
import org.travis4j.api.Travis;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * ApplicationConfiguration.
 */
@Configuration
@Import({ RedissonConfiguration.class, MongoConfiguration.class })
public class ApplicationConfiguration {

    @Bean
    public Path logsPath() {
        return Paths.get("/data/jarvis/logs");
    }

    @Bean
    public MongoCollection<Document> repositories(MongoDatabase db) {
        return db.getCollection("repositoryjsonobject");
    }

    @Bean
    public MongoCollection<Document> builds(MongoDatabase db) {
        return db.getCollection("buildjsonobject");
    }

    @Bean
    public MongoCollection<Document> jobs(MongoDatabase db) {
        return db.getCollection("jobjsonobject");
    }

    @Bean
    public BlockingQueue<Request> travisRequests(RedissonClient redisson) {
        return redisson.getBlockingQueue("jarvis:travisRequests");
    }

    @Bean
    public BlockingQueue<Result> travisResults() {
        return new LinkedBlockingQueue<>();
    }

    @Bean
    public BlockingQueue<WriteRequest<?>> writeRequests() {
        return new LinkedBlockingQueue<>();
    }

    @Bean
    public List<FailedRequest> failedTravisRequests(RedissonClient redisson) {
        return redisson.getList("jarvis:failedTravisRequests");
    }

    @Bean
    public BlockingQueue<RequestEnvelope> dlRequestQueue(RedissonClient redisson) {
        return redisson.getBlockingQueue("jarvis:dlRequestQueue");
    }

    @Bean
    public BlockingQueue<LogDownloadRequest> dlLogRequestQueue(RedissonClient redisson) {
        return redisson.getBlockingQueue("jarvis:dlLogRequestQueue");
    }

    @Bean
    public ResultQueueLocator resultQueueLocator() {
        return new LocalResultQueueLocator();
    }

    @Bean(destroyMethod = "close")
    public Travis travis() {
        return new TravisClient();
    }

}
