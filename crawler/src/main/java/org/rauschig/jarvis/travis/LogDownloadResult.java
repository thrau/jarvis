package org.rauschig.jarvis.travis;

import java.nio.file.Path;

import com.google.common.base.MoreObjects;

/**
 * LogDownloadResult.
 */
public class LogDownloadResult implements Result<Path> {

    private long repositoryId;
    private long jobId;
    private Path path;

    LogDownloadResult() {
        // bean creation
    }

    public LogDownloadResult(long repositoryId, long jobId, Path path) {
        this.repositoryId = repositoryId;
        this.jobId = jobId;
        this.path = path;
    }

    public long getRepositoryId() {
        return repositoryId;
    }

    public long getJobId() {
        return jobId;
    }

    public boolean isFileNotFound() {
        return path == null;
    }

    @Override
    public Path get() {
        return path;
    }

    @Override
    public void accept(ResultVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("repositoryId", repositoryId)
                .add("jobId", jobId)
                .add("path", path)
                .toString();
    }
}
