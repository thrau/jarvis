package org.rauschig.jarvis.travis;

import org.travis4j.model.Log;

/**
 * LogResult.
 */
public class LogResult extends AbstractResult<Log> {

    public LogResult(Log value) {
        super(value);
    }

    @Override
    public void accept(ResultVisitor visitor) {
        visitor.visit(this);
    }
}
