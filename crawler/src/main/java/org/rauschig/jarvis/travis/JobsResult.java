package org.rauschig.jarvis.travis;

import java.util.List;

import org.travis4j.model.Job;

/**
 * JobsResult.
 */
public class JobsResult extends AbstractListResult<Job> {

    public JobsResult(List<Job> value) {
        super(value);
    }

    @Override
    public void accept(ResultVisitor visitor) {
        visitor.visit(this);
    }
}
