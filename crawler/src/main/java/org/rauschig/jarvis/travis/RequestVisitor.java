package org.rauschig.jarvis.travis;

/**
 * RequestVisitor.
 */
public interface RequestVisitor {
    void visit(RepositoryRequest request);

    void visit(BuildsRequest request);

    void visit(LogRequest request);

    void visit(JobsRequest request);

    void visit(LogDownloadRequest request);
}
