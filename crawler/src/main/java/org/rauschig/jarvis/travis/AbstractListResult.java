package org.rauschig.jarvis.travis;

import java.util.List;

/**
 * AbstractListResult.
 */
public abstract class AbstractListResult<T> extends AbstractResult<List<T>> {
    public AbstractListResult(List<T> value) {
        super(value);
    }
}
