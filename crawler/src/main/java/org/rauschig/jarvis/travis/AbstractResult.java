package org.rauschig.jarvis.travis;

/**
 * AbstractResult.
 */
public abstract class AbstractResult<T> implements Result<T> {

    private final T value;

    public AbstractResult(T value) {
        this.value = value;
    }

    @Override
    public T get() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("%s[%s]", getClass().getSimpleName(), value);
    }
}
