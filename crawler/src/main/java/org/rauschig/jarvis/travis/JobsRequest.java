package org.rauschig.jarvis.travis;

import com.google.common.base.MoreObjects;

/**
 * JobsRequest.
 */
public class JobsRequest implements Request {

    private static final long serialVersionUID = 1L;

    private long buildId;

    JobsRequest() {
    }

    public JobsRequest(long buildId) {
        this.buildId = buildId;
    }

    public long getBuildId() {
        return buildId;
    }

    @Override
    public void accept(RequestVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("buildId", buildId)
                .toString();
    }
}
