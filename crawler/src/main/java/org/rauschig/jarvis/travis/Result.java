package org.rauschig.jarvis.travis;

/**
 * Result.
 */
public interface Result<T> {
    T get();

    void accept(ResultVisitor visitor);
}
