package org.rauschig.jarvis.travis;

import com.google.common.base.MoreObjects;

public class LogDownloadRequest implements Request {

    private static final long serialVersionUID = 1L;

    private long repositoryId;
    private long jobId;

    LogDownloadRequest() {
        // bean creation
    }

    public LogDownloadRequest(long repositoryId, long jobId) {
        this.repositoryId = repositoryId;
        this.jobId = jobId;
    }

    public long getRepositoryId() {
        return repositoryId;
    }

    public long getJobId() {
        return jobId;
    }

    @Override
    public void accept(RequestVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("repositoryId", repositoryId)
                .add("jobId", jobId)
                .toString();
    }
}
