package org.rauschig.jarvis.travis;

import com.google.common.base.MoreObjects;

/**
 * LogRequest.
 */
public class LogRequest implements Request {

    private static final long serialVersionUID = 1L;

    private long logId;

    LogRequest() {
    }

    public LogRequest(long logId) {
        this.logId = logId;
    }

    public long getLogId() {
        return logId;
    }

    @Override
    public void accept(RequestVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("logId", logId)
                .toString();
    }
}
