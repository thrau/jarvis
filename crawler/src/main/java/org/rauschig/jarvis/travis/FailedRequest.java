package org.rauschig.jarvis.travis;

import java.io.Serializable;
import java.time.Instant;

import org.slf4j.Logger;

/**
 * FailedRequest.
 */
public class FailedRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Request request;
    private Instant time;
    private Exception cause;

    FailedRequest() {

    }

    public FailedRequest(Request request, Exception cause) {
        this(request, Instant.now(), cause);
    }

    public FailedRequest(Request request, Instant time, Exception cause) {
        this.request = request;
        this.time = time;
        this.cause = cause;
    }

    public Request getRequest() {
        return request;
    }

    public Instant getTime() {
        return time;
    }

    public Exception getCause() {
        return cause;
    }

    public void log(Logger logger) {
        logger.error("At {} request {} failed", time, request, cause);
    }

    @Override
    public String toString() {
        return String.format("[%s] %s = %s", time, request, cause.getMessage());
    }
}
