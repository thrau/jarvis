package org.rauschig.jarvis.travis;

import org.travis4j.model.Repository;

/**
 * RepositoryResult.
 */
public class RepositoryResult extends AbstractResult<Repository> {

    public RepositoryResult(Repository value) {
        super(value);
    }

    @Override
    public void accept(ResultVisitor visitor) {
        visitor.visit(this);
    }
}
