package org.rauschig.jarvis.travis;

import java.io.Serializable;

/**
 * Request.
 */
public interface Request extends Serializable {
    void accept(RequestVisitor visitor);
}
