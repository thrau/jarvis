package org.rauschig.jarvis.travis;

import com.google.common.base.MoreObjects;

/**
 * RepositoryRequest.
 */
public class RepositoryRequest implements Request {

    private static final long serialVersionUID = 1L;

    private String owner;
    private String name;

    RepositoryRequest() {

    }

    public RepositoryRequest(String owner, String name) {
        this.owner = owner;
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("owner", owner)
                .add("name", name)
                .toString();
    }

    @Override
    public void accept(RequestVisitor visitor) {
        visitor.visit(this);
    }
}
