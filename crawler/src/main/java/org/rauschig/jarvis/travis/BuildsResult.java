package org.rauschig.jarvis.travis;

import java.util.List;

import org.travis4j.model.Build;

/**
 * BuildsResult.
 */
public class BuildsResult extends AbstractListResult<Build> {

    public BuildsResult(List<Build> value) {
        super(value);
    }

    @Override
    public void accept(ResultVisitor visitor) {
        visitor.visit(this);
    }
}
