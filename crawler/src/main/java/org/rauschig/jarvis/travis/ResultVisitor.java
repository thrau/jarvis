package org.rauschig.jarvis.travis;

/**
 * ResultVisitor.
 */
public interface ResultVisitor {
    void visit(RepositoryResult result);

    void visit(BuildsResult result);

    void visit(LogResult result);

    void visit(JobsResult result);

    void visit(LogDownloadResult result);
}
