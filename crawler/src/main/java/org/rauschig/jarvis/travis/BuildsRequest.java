package org.rauschig.jarvis.travis;

import java.util.List;

import org.travis4j.model.Build;
import org.travis4j.model.Repository;

import com.google.common.base.MoreObjects;

/**
 * BuildsRequest.
 */
public class BuildsRequest implements Request {

    private static final long serialVersionUID = 1L;

    private long repositoryId;
    private long offset;

    BuildsRequest() {
    }

    public BuildsRequest(Repository repository) {
        this(repository.getId(), repository.getLastBuildNumber());
    }

    public BuildsRequest(long repositoryId, long offset) {
        this.repositoryId = repositoryId;
        this.offset = offset;
    }

    public long getRepositoryId() {
        return repositoryId;
    }

    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("repositoryId", repositoryId)
                .add("offset", offset)
                .toString();
    }

    @Override
    public void accept(RequestVisitor visitor) {
        visitor.visit(this);
    }

    public static BuildsRequest next(BuildsResult result) {
        List<Build> builds = result.get();

        if (builds == null || builds.isEmpty()) {
            return null;
        }

        Build top = builds.get(0); // FIXME
        Long repositoryId = top.getRepositoryId();
        long currentOffset = top.getNumber();
        long nextOffset = currentOffset - (builds.size() - 1);

        return (nextOffset > 1) ? new BuildsRequest(repositoryId, nextOffset) : null;
    }
}
