package org.rauschig.jarvis.spider.strategy;

import org.rauschig.jarvis.comm.RequestDispatcher;
import org.rauschig.jarvis.spider.CrawlingStrategy;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.JobsResult;
import org.rauschig.jarvis.travis.LogDownloadResult;
import org.rauschig.jarvis.travis.LogResult;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.base.MoreObjects;

/**
 * LoggingStrategy.
 */
@Scope("prototype")
@Component
public class LoggingStrategy implements CrawlingStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(LoggingStrategy.class);

    private Logger log;

    public LoggingStrategy() {
        this(LOG);
    }

    public LoggingStrategy(Logger log) {
        this.log = log;
    }

    @Override
    public void visit(RepositoryResult result) {
        log.debug("Handling RepositoryResult {}", result);
    }

    @Override
    public void visit(BuildsResult result) {
        log.debug("Handling BuildsResult {}", result);
    }

    @Override
    public void visit(LogResult result) {
        log.debug("Handling LogResult {}", result);
    }

    @Override
    public void visit(JobsResult result) {
        log.debug("Handling JobsResult {}", result);
    }

    @Override
    public void visit(LogDownloadResult result) {
        log.debug("Handling LogDownloadResult {}", result);
    }

    @Override
    public void setRequestDispatcher(RequestDispatcher requestDispatcher) {

    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("logger", log.getName())
                .toString();
    }
}
