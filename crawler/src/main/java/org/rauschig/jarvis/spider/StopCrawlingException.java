package org.rauschig.jarvis.spider;

/**
 * StopCrawlingException.
 */
public class StopCrawlingException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public StopCrawlingException() {
        super();
    }

    public StopCrawlingException(String message) {
        super(message);
    }
}
