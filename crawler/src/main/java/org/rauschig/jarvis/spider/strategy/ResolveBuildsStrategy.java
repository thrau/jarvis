package org.rauschig.jarvis.spider.strategy;

import org.rauschig.jarvis.spider.StopCrawlingException;
import org.rauschig.jarvis.travis.BuildsRequest;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.travis4j.model.Repository;

/**
 * ResolveBuildsStrategy.
 */
@Scope("prototype")
@Component
public class ResolveBuildsStrategy extends AbstractCrawlingStrategy {

    @Override
    public void visit(RepositoryResult result) {
        Repository repository = result.get();

        // resolve first builds page
        requestDispatcher.dispatch(new BuildsRequest(repository));
    }

    @Override
    public void visit(BuildsResult result) {
        // request next builds page
        BuildsRequest next = BuildsRequest.next(result);
        if (next != null) {
            requestDispatcher.dispatch(next);
        } else {
            throw new StopCrawlingException("No builds left");
        }
    }

}
