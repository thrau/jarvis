package org.rauschig.jarvis.spider;

import java.util.concurrent.BlockingQueue;

import org.rauschig.jarvis.travis.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.MoreObjects;

/**
 * A spider uses a crawling strategy and listens to its individual dlResultQueue to let the strategy handle the result.
 */
public class Spider implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(Spider.class);

    private int id;
    private String description;

    private CrawlingStrategy strategy;

    private BlockingQueue<Result<?>> dlResultQueue;

    private boolean running;

    public Spider(int id, CrawlingStrategy strategy, BlockingQueue<Result<?>> dlResultQueue) {
        this.id = id;
        this.strategy = strategy;
        this.dlResultQueue = dlResultQueue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public boolean isRunning() {
        return running;
    }

    @Override
    public void run() {
        running = true;

        while (true) {
            Result next;

            try {
                next = dlResultQueue.take();
            } catch (InterruptedException e) {
                LOG.info("Interrupted crawler");
                break;
            }

            try {
                next.accept(strategy);
            } catch (StopCrawlingException e) {
                LOG.info("Stop crawling instruction received {}", e.getMessage());
                break;
            } catch (Exception e) {
                LOG.error("Crawler caught exception while running strategy", e);
            }
        }
        running = false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("running", running)
                .add("description", description)
                .add("strategy", strategy)
                .toString();
    }
}
