package org.rauschig.jarvis.spider.strategy;

import java.util.ArrayList;
import java.util.List;

import org.rauschig.jarvis.spider.CrawlingStrategy;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class StrategyFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public <T extends CrawlingStrategy> T create(Class<T> strategy) {
        return applicationContext.getBean(strategy);
    }

    @SafeVarargs
    public final CompoundStrategy create(Class<? extends CrawlingStrategy>... strategies) {
        List<CrawlingStrategy> result = new ArrayList<>(strategies.length);

        for (Class<? extends CrawlingStrategy> strategy : strategies) {
            result.add(create(strategy));
        }

        return new CompoundStrategy(result);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
