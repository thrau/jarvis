package org.rauschig.jarvis.spider;

import org.rauschig.jarvis.comm.RequestDispatcher;
import org.rauschig.jarvis.travis.ResultVisitor;

/**
 * CrawlingStrategy.
 */
public interface CrawlingStrategy extends ResultVisitor {
    void setRequestDispatcher(RequestDispatcher requestDispatcher);
}
