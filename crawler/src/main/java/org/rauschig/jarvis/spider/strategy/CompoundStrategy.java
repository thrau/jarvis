package org.rauschig.jarvis.spider.strategy;

import java.util.Arrays;
import java.util.List;

import org.rauschig.jarvis.comm.RequestDispatcher;
import org.rauschig.jarvis.spider.CrawlingStrategy;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.JobsResult;
import org.rauschig.jarvis.travis.LogDownloadResult;
import org.rauschig.jarvis.travis.LogResult;
import org.rauschig.jarvis.travis.RepositoryResult;

import com.google.common.base.MoreObjects;

/**
 * CompoundStrategy.
 */
public class CompoundStrategy implements CrawlingStrategy {

    private List<CrawlingStrategy> strategies;

    public CompoundStrategy(CrawlingStrategy... strategies) {
        this(Arrays.asList(strategies));
    }

    public CompoundStrategy(List<CrawlingStrategy> strategies) {
        this.strategies = strategies;
    }

    @Override
    public void visit(RepositoryResult result) {
        strategies.forEach(result::accept);
    }

    @Override
    public void visit(BuildsResult result) {
        strategies.forEach(result::accept);
    }

    @Override
    public void visit(LogResult result) {
        strategies.forEach(result::accept);
    }

    @Override
    public void visit(JobsResult result) {
        strategies.forEach(result::accept);
    }

    @Override
    public void visit(LogDownloadResult result) {
        strategies.forEach(result::accept);
    }

    @Override
    public void setRequestDispatcher(RequestDispatcher requestDispatcher) {
        strategies.forEach(s -> s.setRequestDispatcher(requestDispatcher));
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("strategies", strategies)
                .toString();
    }
}
