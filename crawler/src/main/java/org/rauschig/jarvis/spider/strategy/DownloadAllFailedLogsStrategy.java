package org.rauschig.jarvis.spider.strategy;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.bson.Document;
import org.rauschig.jarvis.service.LogStorageService;
import org.rauschig.jarvis.spider.StopCrawlingException;
import org.rauschig.jarvis.travis.LogDownloadRequest;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.travis4j.model.Repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

@Component
@Scope("prototype")
public class DownloadAllFailedLogsStrategy extends AbstractCrawlingStrategy {

    @Autowired
    private LogStorageService logStorageService;

    @Resource
    private MongoCollection<Document> builds;

    @Override
    public void visit(RepositoryResult result) {
        Repository repository = result.get();

        Set<Integer> unfetchedJobIds = getCandidates(repository.getId());

        for (Integer jobId : unfetchedJobIds) {
            requestDispatcher.dispatch(new LogDownloadRequest(repository.getId(), jobId));
        }

        throw new StopCrawlingException();
    }

    protected Set<Integer> getCandidates(long repositoryId) {
        Set<Integer> allJobIds = getAllJobIds(repositoryId, true);
        Set<Integer> fetchedJobIds = logStorageService.getFetchedJobIds(repositoryId);

        allJobIds.removeAll(fetchedJobIds);

        return allJobIds;
    }

    @SuppressWarnings("unchecked")
    protected Set<Integer> getAllJobIds(long repositoryId, boolean failedOnly) {
        FindIterable<Document> documents = builds
                .find(new Document("repository_id", repositoryId))
                .projection(new Document("job_ids", "1").append("state", "1"));

        Set<Integer> jobs = new HashSet<>();
        for (Document document : documents) {
            if (failedOnly) {
                String state = document.getString("state");
                if (!("failed".equals(state) || "errored".equals(state))) {
                    continue;
                }
            }
            jobs.addAll(document.get("job_ids", List.class));
        }
        return jobs;
    }

}
