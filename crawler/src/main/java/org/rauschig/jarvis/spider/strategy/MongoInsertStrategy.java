package org.rauschig.jarvis.spider.strategy;

import org.rauschig.jarvis.mongo.WriteManyRequest;
import org.rauschig.jarvis.mongo.WriteOneRequest;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.JobsResult;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("prototype")
@Component
public class MongoInsertStrategy extends AbstractCrawlingStrategy {
    @Override
    public void visit(RepositoryResult result) {
        requestDispatcher.dispatch(new WriteOneRequest<>(result.get()));
    }

    @Override
    public void visit(BuildsResult result) {
        requestDispatcher.dispatch(new WriteManyRequest<>(result.get()));
    }

    @Override
    public void visit(JobsResult result) {
        requestDispatcher.dispatch(new WriteManyRequest<>(result.get()));
    }
}
