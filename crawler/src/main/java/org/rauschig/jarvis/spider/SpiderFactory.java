package org.rauschig.jarvis.spider;

/**
 * SpiderFactory.
 */
public interface SpiderFactory {
    Spider createSpider(CrawlingStrategy strategy);
}
