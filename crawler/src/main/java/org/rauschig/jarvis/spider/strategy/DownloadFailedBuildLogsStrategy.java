package org.rauschig.jarvis.spider.strategy;

import java.util.List;

import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.LogDownloadRequest;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.travis4j.model.Build;

/**
 * DownloadFailedBuildLogsStrategy.
 */
@Component
@Scope("prototype")
public class DownloadFailedBuildLogsStrategy extends AbstractCrawlingStrategy {

    @Override
    public void visit(BuildsResult result) {
        List<Build> builds = result.get();

        for (Build build : builds) {
            if ("failed".equals(build.getState()) || "errored".equals(build.getState())) {
                for (Long jobId : build.getJobIds()) {
                    requestDispatcher.dispatch(new LogDownloadRequest(build.getRepositoryId(), jobId));
                }
            }
        }

    }
}
