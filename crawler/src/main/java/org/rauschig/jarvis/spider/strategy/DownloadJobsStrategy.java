package org.rauschig.jarvis.spider.strategy;

import javax.annotation.Resource;

import org.bson.Document;
import org.rauschig.jarvis.mongo.WriteManyRequest;
import org.rauschig.jarvis.travis.JobsRequest;
import org.rauschig.jarvis.travis.JobsResult;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.travis4j.model.Repository;

import com.mongodb.client.MongoCollection;

/**
 * DownloadJobsStrategy.
 */
@Scope("prototype")
@Component
public class DownloadJobsStrategy extends AbstractCrawlingStrategy {

    @Resource
    private MongoCollection<Document> builds;

    @Resource
    private MongoCollection<Document> jobs;

    @Override
    public void visit(RepositoryResult result) {
        Repository repository = result.get();

        for (Document build : builds.find(new Document("repository_id", repository.getId()))) {
            Integer buildId = build.getInteger("id");

            if (jobs.count(new Document("build_id", buildId)) == 0) {
                requestDispatcher.dispatch(new JobsRequest(buildId));
            }
        }
    }

    @Override
    public void visit(JobsResult result) {
        requestDispatcher.dispatch(new WriteManyRequest<>(result.get()));
    }
}
