package org.rauschig.jarvis.spider.strategy;

import org.rauschig.jarvis.comm.RequestDispatcher;
import org.rauschig.jarvis.spider.CrawlingStrategy;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.JobsResult;
import org.rauschig.jarvis.travis.LogDownloadResult;
import org.rauschig.jarvis.travis.LogResult;
import org.rauschig.jarvis.travis.RepositoryResult;

/**
 * AbstractCrawlingStrategy.
 */
public abstract class AbstractCrawlingStrategy implements CrawlingStrategy {

    protected RequestDispatcher requestDispatcher;

    @Override
    public void setRequestDispatcher(RequestDispatcher requestDispatcher) {
        this.requestDispatcher = requestDispatcher;
    }

    @Override
    public void visit(RepositoryResult result) {

    }

    @Override
    public void visit(BuildsResult result) {

    }

    @Override
    public void visit(LogResult result) {

    }

    @Override
    public void visit(JobsResult result) {

    }

    @Override
    public void visit(LogDownloadResult result) {

    }

    public String toString() {
        return getClass().getSimpleName();
    }
}
