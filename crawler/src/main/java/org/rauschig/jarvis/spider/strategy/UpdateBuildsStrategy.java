package org.rauschig.jarvis.spider.strategy;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.bson.Document;
import org.rauschig.jarvis.mongo.UpdateOneRequest;
import org.rauschig.jarvis.mongo.WriteOneRequest;
import org.rauschig.jarvis.spider.StopCrawlingException;
import org.rauschig.jarvis.travis.BuildsRequest;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.travis4j.model.Build;
import org.travis4j.model.Repository;

import com.mongodb.client.MongoCollection;

/**
 * UpdateBuildsStrategy.
 */
@Scope("prototype")
@Component
public class UpdateBuildsStrategy extends AbstractCrawlingStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateBuildsStrategy.class);

    private Map<Long, Long> limits;

    @Resource
    private MongoCollection<Document> builds;

    public UpdateBuildsStrategy() {
        this.limits = new HashMap<>();
    }

    @Override
    public void visit(RepositoryResult result) {
        Repository repository = result.get();

        String nr = builds.find(new Document("repository_id", repository.getId()))
                .sort(new Document("id", -1))
                .projection(new Document("number", "1"))
                .iterator().tryNext()
                .getString("number");

        limits.put(repository.getId(), Long.parseLong(nr));

        // update repository data
        requestDispatcher.dispatch(new UpdateOneRequest<>(repository));
        requestDispatcher.dispatch(new BuildsRequest(repository));
    }

    @Override
    public void visit(BuildsResult result) {

        LOG.debug("Handling buildresult");

        for (Build build : result.get()) {
            if (build.getNumber() <= limits.get(build.getRepositoryId())) {
                LOG.debug("Done updating {}", build.getRepositoryId());
                limits.remove(build.getRepositoryId());
                return;
//                throw new StopCrawlingException();
            }

            requestDispatcher.dispatch(new WriteOneRequest<>(build));
        }

        // request next builds page
        BuildsRequest next = BuildsRequest.next(result);
        if (next != null) {
            requestDispatcher.dispatch(next);
        } else {
//            throw new StopCrawlingException();
        }
    }
}
