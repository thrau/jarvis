package org.rauschig.jarvis.spider.strategy;

import javax.annotation.Resource;

import org.bson.Document;
import org.rauschig.jarvis.mongo.WriteManyRequest;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.JobsRequest;
import org.rauschig.jarvis.travis.JobsResult;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.travis4j.model.Build;

import com.mongodb.client.MongoCollection;

/**
 * Crawls jobs of BuildResults that haven't been downloaded yet.
 */
@Component
@Scope("prototype")
public class CrawlJobsStrategy extends AbstractCrawlingStrategy {

    @Resource
    private MongoCollection<Document> jobs;

    @Override
    public void visit(BuildsResult result) {
        for (Build build : result.get()) {
            Long buildId = build.getId();

            if (jobs.count(new Document("build_id", buildId)) == 0) {
                requestDispatcher.dispatch(new JobsRequest(buildId));
            }
        }
    }

    @Override
    public void visit(JobsResult result) {
        requestDispatcher.dispatch(new WriteManyRequest<>(result.get()));
    }
}
