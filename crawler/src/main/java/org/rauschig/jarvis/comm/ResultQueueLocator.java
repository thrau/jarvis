package org.rauschig.jarvis.comm;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;

import org.rauschig.jarvis.travis.Result;

/**
 * ResultQueueLocator.
 */
public interface ResultQueueLocator {
    BlockingQueue<Result<?>> getResultQueue(int spiderId);

    Collection<BlockingQueue<Result<?>>> getAllResultQueues();
}
