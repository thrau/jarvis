package org.rauschig.jarvis.comm;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.rauschig.jarvis.travis.Result;

/**
 * LocalResultQueueLocator.
 */
public class LocalResultQueueLocator implements ResultQueueLocator {

    private final Map<Integer, BlockingQueue<Result<?>>> registry;

    public LocalResultQueueLocator() {
        this(new HashMap<>());
    }

    protected LocalResultQueueLocator(Map<Integer, BlockingQueue<Result<?>>> registry) {
        this.registry = registry;
    }

    @Override
    public BlockingQueue<Result<?>> getResultQueue(int spiderId) {
        synchronized (registry) {
            if (!registry.containsKey(spiderId)) {
                registry.put(spiderId, new LinkedBlockingQueue<>(Integer.MAX_VALUE));
            }
            return registry.get(spiderId);
        }
    }

    @Override
    public Collection<BlockingQueue<Result<?>>> getAllResultQueues() {
        synchronized (registry) {
            return registry.values();
        }
    }
}
