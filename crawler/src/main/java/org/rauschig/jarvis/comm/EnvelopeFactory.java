package org.rauschig.jarvis.comm;

import org.rauschig.jarvis.travis.Request;

/**
 * EnvelopeFactory.
 */
public interface EnvelopeFactory {
    RequestEnvelope createEnvelope(Request request);
}
