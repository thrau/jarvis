package org.rauschig.jarvis.comm;

import java.io.Serializable;

import org.rauschig.jarvis.travis.Request;

import com.google.common.base.MoreObjects;

/**
 * RequestEnvelope.
 */
public class RequestEnvelope implements Envelope<Request>, Serializable {

    private static final long serialVersionUID = 1L;

    private int spiderId;
    private Request request;

    RequestEnvelope() {
        // for redisson bean creation
    }

    public RequestEnvelope(int spiderId, Request request) {
        this.spiderId = spiderId;
        this.request = request;
    }

    @Override
    public int getSpiderId() {
        return spiderId;
    }

    @Override
    public Request getPayload() {
        return request;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("spiderId", spiderId)
                .add("request", request)
                .toString();
    }
}
