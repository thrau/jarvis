package org.rauschig.jarvis.comm;

import org.rauschig.jarvis.travis.Request;

/**
 * DefaultEnvelopeFactory.
 */
public class DefaultEnvelopeFactory implements EnvelopeFactory {

    private int id;

    public DefaultEnvelopeFactory(int id) {
        this.id = id;
    }

    @Override
    public RequestEnvelope createEnvelope(Request request) {
        return new RequestEnvelope(id, request);
    }

}
