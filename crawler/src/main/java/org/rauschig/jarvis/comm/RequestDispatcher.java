package org.rauschig.jarvis.comm;

import java.util.concurrent.BlockingQueue;

import org.rauschig.jarvis.mongo.WriteRequest;
import org.rauschig.jarvis.travis.Request;

/**
 * RequestDispatcher.
 */
public class RequestDispatcher {

    private EnvelopeFactory envelopeFactory;
    private BlockingQueue<RequestEnvelope> dlRequestQueue;

    private BlockingQueue<WriteRequest<?>> writeRequests;

    public RequestDispatcher(EnvelopeFactory envelopeFactory, BlockingQueue<RequestEnvelope> dlRequestQueue,
            BlockingQueue<WriteRequest<?>> writeRequests) {
        this.envelopeFactory = envelopeFactory;
        this.dlRequestQueue = dlRequestQueue;

        this.writeRequests = writeRequests;
    }

    public RequestEnvelope envelop(Request request) {
        return envelopeFactory.createEnvelope(request);
    }

    public boolean dispatch(Request request) {
        return dlRequestQueue.add(envelop(request));
    }

    public boolean dispatch(WriteRequest<?> writeRequest) {
        return writeRequests.add(writeRequest);
    }

}
