package org.rauschig.jarvis.comm;

/**
 * Envelope.
 */
public interface Envelope<T> {

    int getSpiderId();

    T getPayload();
}
