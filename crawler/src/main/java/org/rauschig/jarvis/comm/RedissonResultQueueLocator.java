package org.rauschig.jarvis.comm;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import org.rauschig.jarvis.travis.Result;
import org.redisson.RedissonClient;

/**
 * RedissonResultQueueLocator.
 */
public class RedissonResultQueueLocator implements ResultQueueLocator {

    private RedissonClient redissonClient;

    public RedissonResultQueueLocator(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
    }

    @Override
    public BlockingQueue<Result<?>> getResultQueue(int spiderId) {
        return redissonClient.getBlockingQueue("jarvis:dlResultQueue:" + spiderId);
    }

    @Override
    public List<BlockingQueue<Result<?>>> getAllResultQueues() {
        List<BlockingQueue<Result<?>>> result = new ArrayList<>();

        for (String key : redissonClient.getKeys().getKeysByPattern("jarvis:dlResultQueue:*")) {
            result.add(redissonClient.getBlockingDeque(key));
        }

        return result;
    }
}
