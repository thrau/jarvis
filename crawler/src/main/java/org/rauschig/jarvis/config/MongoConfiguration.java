package org.rauschig.jarvis.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 * MongoConfiguration.
 */
@Configuration
@EnableConfigurationProperties({ MongoProperties.class })
public class MongoConfiguration {

    @Autowired
    private MongoProperties mongoProperties;

    @Bean(destroyMethod = "close")
    public MongoClient mongoClient() {
        return new MongoClient(mongoProperties.getHost(), mongoProperties.getPort());
    }

    @Bean
    public MongoDatabase mongoDb(MongoClient mongoClient) {
        return mongoClient.getDatabase(mongoProperties.getDatabase());
    }

}
