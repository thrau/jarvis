package org.rauschig.jarvis.config;

import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.RedissonClient;
import org.redisson.SingleServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * RedissonConfiguration.
 */
@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class RedissonConfiguration {

    @Autowired
    private RedisProperties redisProperties;

    @Bean
    public Config redissonConfig() {
        Config cfg = new Config();
        SingleServerConfig server = cfg.useSingleServer();
        server.setAddress(String.format("%s:%s", redisProperties.getHost(), redisProperties.getPort()));
        return cfg;
    }

    @Bean(destroyMethod = "shutdown")
    public RedissonClient redisson(Config redissonConfig) {
        return Redisson.create(redissonConfig);
    }

}
