package org.rauschig.jarvis.util;

import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Threads.
 */
public final class Threads {

    private static final Logger LOG = LoggerFactory.getLogger(Threads.class);

    private Threads() {
        // util class
    }

    public static void stop(Thread thread) {
        if (thread.isAlive()) {
            thread.interrupt();
            try {
                LOG.info("Waiting on thread {} to finish", thread);
                thread.join();
            } catch (InterruptedException e) {
                LOG.info("Interrupted while waiting on thread {} to finish", thread, e);
            }
        } else {
            LOG.info("Thread {} was already stopped earlier", thread);
        }
    }

    public static void sleep(long ms) {
        sleep(ms, null);
    }

    public static void sleep(long ms, Consumer<InterruptedException> handler) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            if (handler != null) {
                handler.accept(e);
            }
        }
    }

}
