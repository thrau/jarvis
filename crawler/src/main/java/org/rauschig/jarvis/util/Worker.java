package org.rauschig.jarvis.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Worker.
 */
public abstract class Worker extends Thread {

    private static final Logger LOG = LoggerFactory.getLogger(Worker.class);

    public Worker(String name) {
        super(name);
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (isInterrupted()) {
                    break;
                }
                boolean cont = next();
                if (!cont) {
                    break;
                }
            } catch (InterruptedException e) {
                LOG.info("Worker {} interrupted, stopping", this);
                break;
            }
        }

        LOG.info("Ending worker {} loop", this);
    }

    protected abstract boolean next() throws InterruptedException;
}
