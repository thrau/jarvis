package org.rauschig.jarvis.service;

/**
 * Daemon.
 */
public interface Daemon {
    void start();

    void stop();
}
