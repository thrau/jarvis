package org.rauschig.jarvis.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.util.function.Function;

import org.rauschig.jarvis.travis.BuildsRequest;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.JobsRequest;
import org.rauschig.jarvis.travis.JobsResult;
import org.rauschig.jarvis.travis.LogDownloadRequest;
import org.rauschig.jarvis.travis.LogDownloadResult;
import org.rauschig.jarvis.travis.LogRequest;
import org.rauschig.jarvis.travis.LogResult;
import org.rauschig.jarvis.travis.RepositoryRequest;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.rauschig.jarvis.travis.Request;
import org.rauschig.jarvis.travis.RequestVisitor;
import org.rauschig.jarvis.travis.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.travis4j.TravisClient;
import org.travis4j.api.Travis;
import org.travis4j.model.Log;

/**
 * Executes Requests on the Travis API.
 */
@Component
public class RequestExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(RequestExecutor.class);

    private Travis travis;
    private S3LogDownloader s3LogDownloader;

    @Autowired
    public RequestExecutor(Travis travis, S3LogDownloader s3LogDownloader) {
        this.travis = travis;
        this.s3LogDownloader = s3LogDownloader;
    }

    public Result execute(Request request) {
        return new TravisRequestHandler().acceptAndGet(request);
    }

    public class TravisRequestHandler implements RequestVisitor {

        private Result<?> result;

        public Result<?> acceptAndGet(Request request) {
            request.accept(this);
            return result;
        }

        @Override
        public void visit(RepositoryRequest request) {
            result = execute(RepositoryResult::new,
                    travis -> travis.repositories().getRepository(request.getOwner(), request.getName()));
        }

        @Override
        public void visit(BuildsRequest request) {
            result = execute(BuildsResult::new,
                    travis -> travis.builds().getBuilds(request.getRepositoryId(), request.getOffset()));
        }

        @Override
        public void visit(JobsRequest request) {
            result = execute(JobsResult::new, travis -> travis.jobs().getJobsOfBuild(request.getBuildId()));
        }

        @Override
        public void visit(LogRequest request) {
            result = execute(LogResult::new, travis -> {
                Log log = travis.logs().getLog(request.getLogId());
                log.getBody(); // triggers the body to be resolved and cached before travis is, and therefore the http
                               // stream, closed.
                    return log;
                });
        }

        @Override
        public void visit(LogDownloadRequest request) {
            Path path;
            try {
                path = s3LogDownloader.download(request.getRepositoryId(), request.getJobId());
            } catch (FileNotFoundException e) {
                path = null;
            } catch (IOException e) {
                throw new UncheckedIOException("Error while executing request", e);
            }

            result = new LogDownloadResult(request.getRepositoryId(), request.getJobId(), path);
        }

        public <T, R> R execute(Function<T, R> resultFactory, Function<Travis, T> request) {
            try (TravisClient travis = new TravisClient()) {
                T payload = request.apply(travis);
                return resultFactory.apply(payload);
            } catch (IOException e) {
                throw new UncheckedIOException("Error while executing request", e);
            }
        }
    }
}
