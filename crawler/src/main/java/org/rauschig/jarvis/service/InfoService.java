package org.rauschig.jarvis.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import javax.annotation.Resource;

import org.rauschig.jarvis.comm.RequestEnvelope;
import org.rauschig.jarvis.comm.ResultQueueLocator;
import org.rauschig.jarvis.mongo.WriteRequest;
import org.rauschig.jarvis.spider.Spider;
import org.rauschig.jarvis.travis.FailedRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Metrics.
 */
@Service
public class InfoService {

    @Autowired
    private CrawlerEngine crawlerEngine;

    @Resource
    private BlockingQueue<WriteRequest<?>> writeRequests;

    @Resource
    private BlockingQueue<RequestEnvelope> dlRequestQueue;

    @Autowired
    private ResultQueueLocator resultQueueLocator;

    @Resource
    private List<FailedRequest> failedTravisRequests;

    public void printDebugInfo(PrintWriter writer) throws IOException {

        long spiders = crawlerEngine.getSpiders().values().stream().filter(Spider::isRunning).count();
        writer.format("Active spiders:                %d%n", spiders);
        printSpiderInfo(writer);

        int results = resultQueueLocator.getAllResultQueues().stream()
                .map(Collection::size).reduce(Integer::sum)
                .orElse(0);
        writer.format("Pending travis results:        %d%n", results);
        writer.format("Pending travis request:        %d%n", dlRequestQueue.size());
        writer.format("Failed travis request:         %d%n", failedTravisRequests.size());
        writer.format("Results waiting to be written: %d%n", writeRequests.size());
    }

    public void printFailedRequests(PrintWriter writer) {
        writer.format("Failed travis requests: %d%n", failedTravisRequests.size());

        for (FailedRequest request : failedTravisRequests) {
            writer.format(request.toString()).format("%n");
        }
    }

    public void printSpiderInfo(PrintWriter writer) {
        for (Spider spider : crawlerEngine.getSpiders().values()) {
            writer.format("    %s%n", spider);
        }
    }
}
