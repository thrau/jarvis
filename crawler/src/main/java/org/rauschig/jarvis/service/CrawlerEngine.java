package org.rauschig.jarvis.service;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.bson.Document;
import org.rauschig.jarvis.comm.RequestEnvelope;
import org.rauschig.jarvis.spider.CrawlingStrategy;
import org.rauschig.jarvis.spider.Spider;
import org.rauschig.jarvis.spider.SpiderFactory;
import org.rauschig.jarvis.spider.strategy.CrawlJobsStrategy;
import org.rauschig.jarvis.spider.strategy.DownloadAllFailedLogsStrategy;
import org.rauschig.jarvis.spider.strategy.DownloadFailedBuildLogsStrategy;
import org.rauschig.jarvis.spider.strategy.DownloadJobsStrategy;
import org.rauschig.jarvis.spider.strategy.LoggingStrategy;
import org.rauschig.jarvis.spider.strategy.MongoInsertStrategy;
import org.rauschig.jarvis.spider.strategy.ResolveBuildsStrategy;
import org.rauschig.jarvis.spider.strategy.StrategyFactory;
import org.rauschig.jarvis.spider.strategy.UpdateBuildsStrategy;
import org.rauschig.jarvis.travis.RepositoryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.util.concurrent.MoreExecutors;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

/**
 * Crawler spawns spiders.
 */
@Service
public class CrawlerEngine implements Daemon {

    private static final Logger LOG = LoggerFactory.getLogger(CrawlerEngine.class);

    @Autowired
    private SpiderFactory spiderFactory;

    @Autowired
    private StrategyFactory strategyFactory;

    @Resource
    private BlockingQueue<RequestEnvelope> dlRequestQueue;

    @Resource
    private MongoCollection<Document> builds;

    @Resource
    private MongoCollection<Document> repositories;

    private Map<Integer, Spider> spiders;

    private ExecutorService executor;

    public CrawlerEngine() {
        spiders = new ConcurrentHashMap<>();
    }

    public void resolve(String owner, String name) {
        CrawlingStrategy strategy = strategyFactory.create(
                LoggingStrategy.class,
                MongoInsertStrategy.class,
                DownloadFailedBuildLogsStrategy.class,
                ResolveBuildsStrategy.class);

        Spider spider = hatch(strategy);
        dispatchRepositoryRequest(spider, owner, name);
    }

    /**
     * The most high-level update command. It updates everything of a repository.
     */
    public void updateRepository(String owner, String name) {
        CrawlingStrategy strategy = strategyFactory.create(
                LoggingStrategy.class,
                UpdateBuildsStrategy.class,
                DownloadFailedBuildLogsStrategy.class,
                CrawlJobsStrategy.class);

        Spider spider = hatch(strategy);
        dispatchRepositoryRequest(spider, owner, name);
    }

    public void resolveJobs(String owner, String name) {
        CrawlingStrategy strategy = strategyFactory.create(
                LoggingStrategy.class,
                DownloadJobsStrategy.class);

        Spider spider = hatch(strategy);
        dispatchRepositoryRequest(spider, owner, name);
    }

    public void updateBuilds(String owner, String name) {
        CrawlingStrategy strategy = strategyFactory.create(
                LoggingStrategy.class,
                UpdateBuildsStrategy.class);

        Spider spider = hatch(strategy);
        dispatchRepositoryRequest(spider, owner, name);
    }

    public void downloadLogs(String owner, String name) {
        CrawlingStrategy strategy = strategyFactory.create(
                LoggingStrategy.class,
                DownloadAllFailedLogsStrategy.class);

        Spider spider = hatch(strategy);
        dispatchRepositoryRequest(spider, owner, name);
    }

    public void updateAllRepositoryBuilds() {
        FindIterable<Document> repositories = this.repositories.find();

        StreamSupport.stream(repositories.spliterator(), false)
                .map(d -> d.getString("slug"))
                .map(slug -> slug.split("/"))
                .forEach(arr -> updateBuilds(arr[0], arr[1]));
    }

    /**
     * Calls {@link #updateRepository(String, String)} for every repository in the database.
     */
    public void updateDb() {
        FindIterable<Document> repositories = this.repositories.find();

        StreamSupport.stream(repositories.spliterator(), false)
                .map(d -> d.getString("slug"))
                .map(slug -> slug.split("/"))
                .forEach(arr -> updateRepository(arr[0], arr[1]));
    }

    public Map<Integer, Spider> getSpiders() {
        return Collections.unmodifiableMap(spiders);
    }

    @PostConstruct
    @Override
    public void start() {
        LOG.info("Starting CrawlerEngine");
        executor = Executors.newCachedThreadPool();
    }

    @PreDestroy
    @Override
    public void stop() {
        LOG.info("Stopping CrawlerEngine");
        MoreExecutors.shutdownAndAwaitTermination(executor, 5, TimeUnit.SECONDS);
        LOG.info("CrawlerEngine stopped");
    }

    /**
     * Creates a new {@link Spider} with the given {@link CrawlingStrategy}, and runs it using the engine's Executor.
     * 
     * @param strategy the strategy
     * @return the spider after it was started
     */
    private Spider hatch(CrawlingStrategy strategy) {
        Spider spider = spiderFactory.createSpider(strategy);

        spiders.put(spider.getId(), spider);
        executor.execute(spider);

        return spider;
    }

    /**
     * Throws the first stone. All crawling strategies react to a {@code RepositoryResult}.
     * 
     * @param spider the spider that should receive the result
     * @param owner the repository owner
     * @param name the repository name
     */
    private void dispatchRepositoryRequest(Spider spider, String owner, String name) {
        RepositoryRequest request = new RepositoryRequest(owner, name);
        LOG.info("Starting crawling {}/{}, adding initial request {}", owner, name, request);
        spider.setDescription(String.format("slug: %s/%s", owner, name));
        dlRequestQueue.add(new RequestEnvelope(spider.getId(), request));
    }

}
