package org.rauschig.jarvis.service;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

@Service
public class LogStorageService {

    @Resource
    private Path logsPath;

    public Set<Integer> getFetchedJobIds(long repositoryId) {
        Path repoLogsPath = getRepositoryLogPath(repositoryId);

        try {
            return Files.list(repoLogsPath)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .map(s -> s.split("\\.")[0])
                    .map(Integer::parseInt)
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Path getRepositoryLogPath(Object repositoryId) {
        Path path = logsPath.resolve(String.valueOf(repositoryId));

        if (Files.exists(path)) {
            if (Files.isDirectory(path)) {
                return path;
            } else {
                throw new IllegalStateException("Path exists, but is a file " + path);
            }
        } else {
            try {
                return Files.createDirectories(path);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    public Path getLogFilePath(Path parent, Object jobId) {
        return parent.resolve(String.format("%s.txt", jobId));
    }

    public Path getLogFilePath(Object repositoryId, Object jobId) {
        return getLogFilePath(getRepositoryLogPath(repositoryId), jobId);
    }
}
