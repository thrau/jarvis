package org.rauschig.jarvis.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.bson.Document;
import org.rauschig.jarvis.mongo.DocumentMapper;
import org.rauschig.jarvis.mongo.UpdateManyRequest;
import org.rauschig.jarvis.mongo.UpdateOneRequest;
import org.rauschig.jarvis.mongo.WriteManyRequest;
import org.rauschig.jarvis.mongo.WriteOneRequest;
import org.rauschig.jarvis.mongo.WriteRequest;
import org.rauschig.jarvis.mongo.WriteRequestVisitor;
import org.rauschig.jarvis.util.Threads;
import org.rauschig.jarvis.util.Worker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.travis4j.model.Entity;

import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

/**
 * MongoWriter.
 */
@Service
public class MongoWriter implements Daemon {

    private static final Logger LOG = LoggerFactory.getLogger(MongoWriter.class);

    @Autowired
    private MongoDatabase db;

    @Resource
    private BlockingQueue<WriteRequest<?>> writeRequests;

    private Worker worker;

    private DocumentMapper mapper;

    private Map<String, MongoCollection<Document>> collectionCache;

    @Override
    @PostConstruct
    public void start() {
        mapper = new DocumentMapper();
        collectionCache = new ConcurrentHashMap<>();

        worker = new WriterWorker();
        LOG.info("Starting {}", worker.getName());
        worker.start();
    }

    @Override
    @PreDestroy
    public void stop() {
        LOG.info("Stopping {}", worker.getName());
        Threads.stop(worker);
    }

    public class WriterWorker extends Worker implements WriteRequestVisitor {

        public WriterWorker() {
            super("MongoWriter-worker");
        }

        @SuppressWarnings("unchecked")
        @Override
        protected boolean next() throws InterruptedException {
            LOG.debug("Waiting on next write request ...");
            WriteRequest<?> request = writeRequests.take();

            LOG.debug("Writing into mongo: {}", request.getPayload());
            try {
                request.accept(this);
            } catch (Exception e) {
                LOG.error("Error while writing to mongo {}", request, e);
            }

            return true;
        }

        @Override
        public void visit(WriteManyRequest<?> request) {
            List<? extends Entity> payload = request.getPayload();

            if (payload.isEmpty()) {
                return;
            }

            // FIXME hacky, but i need to "insert ifnotexist" many, which is not that easy with bulk insert
            payload.forEach(e -> new WriteOneRequest<>(e).accept(this));

            // Entity head = payload.get(0);
            // MongoCollection<Document> collection = getCollection(head);
            //
            // List<Document> documents = payload.stream()
            // .map(mapper::toDocument)
            // .collect(Collectors.toList());
            //
            // collection.insertMany(documents);
        }

        @Override
        public void visit(WriteOneRequest<?> request) {
            Entity entity = request.getPayload();
            Document document = mapper.toDocument(entity);

            // FIXME how to get a collection name mapping easily?
            MongoCollection<Document> collection = getCollection(entity);

            try {
                collection.insertOne(document);
            } catch (MongoWriteException e) {
                if (e.getError().getCode() == 11000) {
                    // that's okay, 11000 means it's a duplicate
                    LOG.info("Not inserting duplicate document: {}", e.getMessage());
                } else {
                    throw e;
                }
            }
        }

        @Override
        public void visit(UpdateOneRequest<?> request) {
            Entity entity = request.getPayload();

            try {
                replace(entity);
            } catch (Exception e) {
                LOG.error("Error while replacing document in mongo {}", request, e);
            }
        }

        @Override
        public void visit(UpdateManyRequest<?> request) {
            List<? extends Entity> payload = request.getPayload();

            for (Entity entity : payload) {
                try {
                    replace(entity);
                } catch (Exception e) {
                    LOG.error("Error while replacing document in mongo {}", request, e);
                }
            }
        }

        private void replace(Entity entity) {
            MongoCollection<Document> collection = getCollection(entity);
            Document document = mapper.toDocument(entity);

            collection.replaceOne(new Document("id", document.getInteger("id")), document);
        }

        private MongoCollection<Document> getCollection(Entity entity) {
            String name = entity.getClass().getSimpleName().toLowerCase();

            if (!collectionCache.containsKey(name)) {
                collectionCache.put(name, db.getCollection(name));
            }

            return collectionCache.get(name);
        }

    }
}
