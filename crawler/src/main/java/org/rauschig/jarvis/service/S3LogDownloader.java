package org.rauschig.jarvis.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.GZIPOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.io.ByteStreams;

/**
 * S3LogDownloader.
 */
@Service
public class S3LogDownloader {

    private LogStorageService logStorageService;

    @Autowired
    public S3LogDownloader(LogStorageService logStorageService) {
        this.logStorageService = logStorageService;
    }

    public Path download(long repositoryId, long jobId) throws IOException {
        Path logFile = logStorageService.getLogFilePath(repositoryId, jobId);
        Path gzLogFile = logFile.getParent().resolve(logFile.getFileName().toString() + ".gz");

        URL logResource = getLogResource(jobId);

        try (InputStream in = new BufferedInputStream(logResource.openStream())) {

            try (GZIPOutputStream out = new GZIPOutputStream(Files.newOutputStream(gzLogFile))) {
                ByteStreams.copy(in, out);
            }

            return gzLogFile;
        }
    }

    private URL getLogResource(long jobId) {
        URI logResource = URI.create("http://s3.amazonaws.com/archive.travis-ci.org/jobs/" + jobId + "/log.txt");

        try {
            return logResource.toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
