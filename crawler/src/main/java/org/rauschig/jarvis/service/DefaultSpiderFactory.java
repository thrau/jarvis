package org.rauschig.jarvis.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Resource;

import org.rauschig.jarvis.comm.DefaultEnvelopeFactory;
import org.rauschig.jarvis.comm.RequestDispatcher;
import org.rauschig.jarvis.comm.RequestEnvelope;
import org.rauschig.jarvis.comm.ResultQueueLocator;
import org.rauschig.jarvis.mongo.WriteRequest;
import org.rauschig.jarvis.spider.CrawlingStrategy;
import org.rauschig.jarvis.spider.Spider;
import org.rauschig.jarvis.spider.SpiderFactory;
import org.rauschig.jarvis.travis.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultSpiderFactory implements SpiderFactory {

    private AtomicInteger idGenerator;

    @Autowired
    private ResultQueueLocator resultQueueLocator;

    @Resource
    private BlockingQueue<WriteRequest<?>> writeRequests;

    @Resource
    private BlockingQueue<RequestEnvelope> dlRequestQueue;

    public DefaultSpiderFactory() {
        this(new AtomicInteger());
    }

    public DefaultSpiderFactory(AtomicInteger idGenerator) {
        this.idGenerator = idGenerator;
    }

    @Override
    public Spider createSpider(CrawlingStrategy strategy) {
        int id = idGenerator.incrementAndGet();
        BlockingQueue<Result<?>> resultQueue = resultQueueLocator.getResultQueue(id);

        RequestDispatcher requestDispatcher =
            new RequestDispatcher(new DefaultEnvelopeFactory(id), dlRequestQueue, writeRequests);
        strategy.setRequestDispatcher(requestDispatcher);

        return new Spider(id, strategy, resultQueue);
    }
}
