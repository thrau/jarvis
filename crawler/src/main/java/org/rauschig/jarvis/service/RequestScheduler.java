package org.rauschig.jarvis.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.rauschig.jarvis.comm.RequestEnvelope;
import org.rauschig.jarvis.comm.ResultQueueLocator;
import org.rauschig.jarvis.travis.Request;
import org.rauschig.jarvis.travis.Result;
import org.rauschig.jarvis.util.Threads;
import org.rauschig.jarvis.util.Worker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.util.concurrent.RateLimiter;

@Service
public class RequestScheduler implements Daemon {
    private static final Logger LOG = LoggerFactory.getLogger(RequestScheduler.class);

    @Resource
    private BlockingQueue<RequestEnvelope> dlRequestQueue;

    @Autowired
    private RequestExecutor requestExecutor;

    @Autowired
    private ResultQueueLocator resultQueueLocator;

    private ExecutorService executor;
    private AtomicLong executionTaskCounter;
    private RateLimiter rateLimiter;
    private Semaphore semaphore;

    private Thread worker;

    public RequestScheduler() {
        this(5, 2);
    }

    public RequestScheduler(int maxParallelRequests, double rps) {
        this.semaphore = new Semaphore(maxParallelRequests);
        this.executor = Executors.newFixedThreadPool(maxParallelRequests);
        this.rateLimiter = RateLimiter.create(rps);
        this.executionTaskCounter = new AtomicLong(0);
    }

    @PostConstruct
    @Override
    public void start() {
        worker = new WorkerTask("DownloadScheduler-worker");
        worker.start();
    }

    @PreDestroy
    @Override
    public void stop() {
        LOG.info("Stopping DownloadScheduler");
        Threads.stop(worker);
        LOG.info("DownloadScheduler stopped");
    }

    public boolean isRunning() {
        return worker != null && worker.isAlive();
    }

    private class WorkerTask extends Worker {

        public WorkerTask(String name) {
            super(name);
        }

        protected boolean next() throws InterruptedException {
            LOG.debug("Acquiring permit from rate limiter...");
            rateLimiter.acquire();

            LOG.debug("Acquiring permit from semaphore...");
            semaphore.acquire();

            try {
                LOG.debug("Taking next queue element...");
                RequestEnvelope requestEnvelope = dlRequestQueue.take();

                LOG.debug("Submitting new ExecutionTask for request {}", requestEnvelope);
                ExecutionTask executionTask =
                    new ExecutionTask(requestEnvelope, executionTaskCounter.incrementAndGet());
                executor.submit(executionTask);
            } catch (Exception e) {
                semaphore.release();
                throw e;
            }

            return true;
        }
    }

    private class ExecutionTask implements Runnable {

        // TODO timeout

        private final long id;
        private final RequestEnvelope requestEnvelope;

        public ExecutionTask(RequestEnvelope requestEnvelope, long id) {
            this.requestEnvelope = requestEnvelope;
            this.id = id;
        }

        @Override
        public void run() {
            LOG.info("Running request {}: {}", id, requestEnvelope);
            Result result;
            Request request = requestEnvelope.getPayload();

            try {
                Instant then = Instant.now();
                result = requestExecutor.execute(request);
                LOG.info("Request {} took {}ms", request, then.until(Instant.now(), ChronoUnit.MILLIS));
            } catch (Exception e) {
                LOG.error("Error executing request {}", requestEnvelope);
                return;
            } finally {
                semaphore.release();
            }

            LOG.debug("Request {} returned result {}", id, result);

            BlockingQueue<Result<?>> resultQueue = resultQueueLocator.getResultQueue(requestEnvelope.getSpiderId());
            resultQueue.add(result);
        }
    }
}
