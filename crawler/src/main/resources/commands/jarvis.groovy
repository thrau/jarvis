package commands

import org.crsh.cli.Argument
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.rauschig.jarvis.service.CrawlerEngine
import org.rauschig.jarvis.service.InfoService

import java.util.concurrent.BlockingQueue

@Usage("J.A.R.V.I.S. Commands")
class jarvis {

    @Usage("Resolves builds and logs of a repository")
    @Command
    def resolve(InvocationContext ctx,
                @Argument @Usage("Owner of the repo") String owner,
                @Argument @Usage("Name of the repo") String name) {

        if (owner == null || name == null) {
            throw new IllegalArgumentException("Please enter both name and owner")
        }

        getBean(ctx, CrawlerEngine.class).resolve(owner, name)
    }

    @Usage("Update builds, jobs and logs of a repository")
    @Command
    def update(InvocationContext ctx,
               @Argument @Usage("Owner of the repo") String owner,
               @Argument @Usage("Name of the repo") String name) {

        if (owner == null || name == null) {
            throw new IllegalArgumentException("Please enter both name and owner")
        }

        getBean(ctx, CrawlerEngine.class).updateRepository(owner, name)
    }

    @Usage("Update builds of a repository")
    @Command
    def update_builds(InvocationContext ctx,
                      @Argument @Usage("Owner of the repo") String owner,
                      @Argument @Usage("Name of the repo") String name) {

        if (owner == null || name == null) {
            throw new IllegalArgumentException("Please enter both name and owner")
        }

        getBean(ctx, CrawlerEngine.class).updateBuilds(owner, name)
    }

    @Usage("Resolve all jobs of all builds of a repository")
    @Command
    def resolve_jobs(InvocationContext ctx,
                     @Argument @Usage("Owner of the repo") String owner,
                     @Argument @Usage("Name of the repo") String name) {

        if (owner == null || name == null) {
            throw new IllegalArgumentException("Please enter both name and owner")
        }

        getBean(ctx, CrawlerEngine.class).resolveJobs(owner, name)
    }

    @Usage("Update builds of all repositories")
    @Command
    def update_all(InvocationContext ctx) {
        getBean(ctx, CrawlerEngine.class).updateAllRepositoryBuilds()
    }

    @Usage("Update builds, jobs and logs of every repository")
    @Command
    def updatedb(InvocationContext ctx) {
        getBean(ctx, CrawlerEngine.class).updateDb()
    }

    @Usage("Downloads logs for all available builds (that have not been downloaded)")
    @Command
    def download_logs(InvocationContext ctx,
                      @Argument @Usage("Owner of the repo") String owner,
                      @Argument @Usage("Name of the repo") String name) {

        if (owner == null || name == null) {
            throw new IllegalArgumentException("Please enter both name and owner")
        }

        getBean(ctx, CrawlerEngine.class).downloadLogs(owner, name)
    }

    @Command
    @Usage("List failed requests")
    def list_failed(InvocationContext ctx) {
        def info = getBean(ctx, InfoService.class);
        return printToString(info.&printFailedRequests)
    }

    @Command
    @Usage("Clear all failed requests")
    def clear_failed(InvocationContext ctx) {
        def queue = getBean(ctx, "failedTravisRequests", Collection.class);
        def size = queue.size()
        queue.clear()
        return sprintf("Cleared approximately %d requests", size);
    }

    @Command
    @Usage("Show some information about the current system state")
    def info(InvocationContext ctx) {
        def info = getBean(ctx, InfoService.class);
        return printToString(info.&printDebugInfo)
    }

    @Command
    @Usage("Drops all pending travis requests")
    def drop(InvocationContext ctx) {
        def queue = getBean(ctx, "travisRequests", BlockingQueue.class);
        def size = queue.size()
        queue.clear()

        return sprintf("Dropped approximately %d requests", size);
    }

    private static String printToString(Closure consumer) {
        StringWriter str = new StringWriter();
        consumer(new PrintWriter(str));
        return str.toString();
    }

    private static <T> T getBean(InvocationContext ctx, Class<T> bean) {
        return ctx.getAttributes().get("spring.beanfactory").getBean(bean)
    }

    private static <T> T getBean(InvocationContext ctx, String bean, Class<T> type) {
        return ctx.getAttributes().get("spring.beanfactory").getBean(bean, type);
    }

}
