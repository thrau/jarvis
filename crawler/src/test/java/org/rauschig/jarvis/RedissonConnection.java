package org.rauschig.jarvis;

import org.junit.rules.ExternalResource;
import org.redisson.Redisson;
import org.redisson.RedissonClient;

/**
 * RedissonConnection.
 */
public class RedissonConnection extends ExternalResource {

    private RedissonClient redissonClient;

    public RedissonClient getClient() {
        return redissonClient;
    }

    @Override
    protected void before() throws Throwable {
        redissonClient = Redisson.create();
    }

    @Override
    protected void after() {
        redissonClient.getKeys().deleteByPattern("*");
        redissonClient.shutdown();
    }

}
