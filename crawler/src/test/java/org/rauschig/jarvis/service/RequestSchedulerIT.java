package org.rauschig.jarvis.service;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.junit.Test;
import org.rauschig.jarvis.comm.RequestEnvelope;
import org.rauschig.jarvis.comm.ResultQueueLocator;
import org.rauschig.jarvis.travis.RepositoryRequest;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.rauschig.jarvis.travis.Result;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * RequestSchedulerIT.
 */
public class RequestSchedulerIT {
    @Resource
    BlockingQueue<RequestEnvelope> dlRequestQueue;

    @Autowired
    ResultQueueLocator resultQueueLocator;

    @Autowired
    RequestScheduler requestScheduler;

    @Test
    public void test() throws Exception {
        Thread.sleep(1000);
        assertTrue(requestScheduler.isRunning());

        RequestEnvelope envelope = new RequestEnvelope(1, new RepositoryRequest("thrau", "jarchivelib"));
        dlRequestQueue.add(envelope);

        BlockingQueue<Result<?>> resultQueue = resultQueueLocator.getResultQueue(1);

        Result<?> result = resultQueue.poll(3, TimeUnit.SECONDS);
        assertNotNull(result);
        assertThat(result, instanceOf(RepositoryResult.class));

    }
}
