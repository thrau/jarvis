package org.rauschig.jarvis.service;

import java.util.concurrent.BlockingQueue;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.rauschig.jarvis.Application;
import org.rauschig.jarvis.mongo.WriteOneRequest;
import org.rauschig.jarvis.mongo.WriteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.travis4j.api.Travis;
import org.travis4j.model.Repository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class MongoWriterIT {

    @Autowired
    private MongoWriter writer;

    @Resource
    private BlockingQueue<WriteRequest<?>> writeRequests;

    @Autowired
    private Travis travis;

    @Test
    public void test() throws Exception {
        System.out.println(writeRequests);

        Repository repository = travis.repositories().getRepository("thrau", "jarchivelib");
        writeRequests.add(new WriteOneRequest<>(repository));
        Thread.sleep(10000);
    }
}
