package org.rauschig.jarvis.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.rauschig.jarvis.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * CrawlerIT.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class CrawlerIT {

    @Autowired
    CrawlerEngine crawlerEngine;

    @Test
    public void test() throws Exception {
        crawlerEngine.resolve("flex-oss", "flex-oss-parent");
        Thread.sleep(30000);
    }
}