package org.rauschig.jarvis.travis;

import com.google.common.base.MoreObjects;

/**
 * MockRequest.
 */
public class MockRequest implements Request {

    private static final long serialVersionUID = 1L;

    @Override
    public void accept(RequestVisitor visitor) {

    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .toString();
    }
}
