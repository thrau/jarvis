package org.rauschig.jarvis.travis;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.rauschig.jarvis.service.RequestExecutor;
import org.travis4j.TravisClient;
import org.travis4j.model.Build;
import org.travis4j.model.Repository;

/**
 * BuildsRequestUT.
 */
public class BuildsRequestUT {

    private TravisClient travis;
    private RequestExecutor requestExecutor;

    @Before
    public void setUp() throws Exception {
        travis = new TravisClient();
        requestExecutor = new RequestExecutor(travis, null);
    }

    @After
    public void tearDown() throws Exception {
        travis.close();
    }

    @Test
    public void test() throws Exception {
        Repository repo = travis.repositories().getRepository("thrau", "jarchivelib");

        BuildsRequest request = new BuildsRequest(repo);
        BuildsResult result = (BuildsResult) requestExecutor.execute(request);

        for (Build build : result.get()) {
            System.out.println(build.getNumber());
        }
        System.out.println("--");

        while ((request = BuildsRequest.next(result)) != null) {
            result = (BuildsResult) requestExecutor.execute(request);
            for (Build build : result.get()) {
                System.out.println(build.getNumber());
            }
            System.out.println("--");
        }

    }
}
