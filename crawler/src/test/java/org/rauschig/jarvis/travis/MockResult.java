package org.rauschig.jarvis.travis;

import java.io.Serializable;

/**
 * MockResult.
 */
public class MockResult extends AbstractResult<String> implements Serializable {

    private static final long serialVersionUID = 1L;

    MockResult() {
        this(null);
    }

    public MockResult(String value) {
        super(value);
    }

    @Override
    public void accept(ResultVisitor visitor) {

    }
}
