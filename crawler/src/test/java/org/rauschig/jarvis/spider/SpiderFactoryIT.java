package org.rauschig.jarvis.spider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.rauschig.jarvis.Application;
import org.rauschig.jarvis.service.DefaultSpiderFactory;
import org.rauschig.jarvis.spider.strategy.LoggingStrategy;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * SpiderFactoryIT.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class SpiderFactoryIT {

    @Autowired
    private DefaultSpiderFactory spiderFactory;

    @Test
    public void test() throws Exception {
        Spider spider = spiderFactory.createSpider(new LoggingStrategy(LoggerFactory.getLogger("TestLogger")));

        assertNotNull(spider);
        assertEquals(1, spider.getId());
    }

}
