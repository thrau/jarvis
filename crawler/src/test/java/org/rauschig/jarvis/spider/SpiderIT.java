package org.rauschig.jarvis.spider;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.rauschig.jarvis.Application;
import org.rauschig.jarvis.service.DefaultSpiderFactory;
import org.rauschig.jarvis.comm.RequestEnvelope;
import org.rauschig.jarvis.spider.strategy.CompoundStrategy;
import org.rauschig.jarvis.spider.strategy.LoggingStrategy;
import org.rauschig.jarvis.spider.strategy.ResolveBuildsStrategy;
import org.rauschig.jarvis.travis.RepositoryRequest;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * SpiderIT.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class SpiderIT {

    @Resource
    BlockingQueue<RequestEnvelope> dlRequestQueue;

    @Autowired
    DefaultSpiderFactory spiderFactory;

    @Test
    public void test() throws Exception {
        CrawlingStrategy strategy = new CompoundStrategy(
                new LoggingStrategy(LoggerFactory.getLogger("CrawlerLogger")),
                new ResolveBuildsStrategy()
            );

        Spider spider = spiderFactory.createSpider(strategy);

        Thread spiderThread = new Thread(spider);
        spiderThread.start();

        dlRequestQueue.add(new RequestEnvelope(spider.getId(), new RepositoryRequest("flex-oss", "flex-oss-parent")));

        System.out.println("waiting for thread to finish...");
        spiderThread.join(TimeUnit.MINUTES.toMillis(1));
        System.out.println("done!");
    }

}
