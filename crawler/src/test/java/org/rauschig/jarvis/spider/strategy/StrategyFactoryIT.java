package org.rauschig.jarvis.spider.strategy;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.rauschig.jarvis.Application;
import org.rauschig.jarvis.travis.BuildsResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * StrategyFactoryIT.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class StrategyFactoryIT {

    @Autowired
    private StrategyFactory factory;

    @Test
    public void test() throws Exception {
        LoggingStrategy strategy = factory.create(LoggingStrategy.class);
        strategy.visit(new BuildsResult(new ArrayList<>()));
    }
}
