package org.rauschig.jarvis.spider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import org.rauschig.jarvis.spider.strategy.AbstractCrawlingStrategy;
import org.rauschig.jarvis.travis.BuildsResult;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.rauschig.jarvis.travis.Result;

/**
 * SpiderTest.
 */
public class SpiderTest {

    private BlockingQueue<Result<?>> dlResultQueue;
    private AtomicInteger calls;

    @Before
    public void setUp() throws Exception {
        dlResultQueue = new ArrayBlockingQueue<>(2);
        calls = new AtomicInteger(0);
    }

    @Test
    public void simpleDefaultCase_behavesCorrectly() throws Exception {
        RepositoryResult result = new RepositoryResult(null);
        dlResultQueue.add(result);

        CrawlingStrategy strategy = new AbstractCrawlingStrategy() {
            @Override
            public void visit(RepositoryResult incoming) {
                System.out.println("got result");
                assertSame(result, incoming);
                calls.incrementAndGet();
                throw new StopCrawlingException();
            }
        };

        assertEquals(1, dlResultQueue.size());
        Spider spider = new Spider(1, strategy, dlResultQueue);

        Thread spiderThread = new Thread(spider);

        spiderThread.start();
        spiderThread.join(1000);

        assertEquals(1, calls.get());
    }

    @Test
    public void strategyThrowsException_spiderContinuesRunning() throws Exception {
        dlResultQueue.add(new RepositoryResult(null));
        dlResultQueue.add(new BuildsResult(null));

        CrawlingStrategy strategy = new AbstractCrawlingStrategy() {
            @Override
            public void visit(RepositoryResult result) {
                calls.incrementAndGet();
                throw new RuntimeException("Oops");
            }

            @Override
            public void visit(BuildsResult result) {
                calls.incrementAndGet();
                throw new StopCrawlingException();
            }
        };

        Spider spider = new Spider(1, strategy, dlResultQueue);
        Thread spiderThread = new Thread(spider);
        spiderThread.start();
        spiderThread.join(1000);

        assertEquals(2, calls.get());
    }

    @Test
    public void interruptSpiderThread_stopsLoop() throws Exception {
        CrawlingStrategy strategy = new AbstractCrawlingStrategy() {

        };

        Spider spider = new Spider(1, strategy, dlResultQueue);
        Thread spiderThread = new Thread(spider);
        spiderThread.start();

        Thread.sleep(100);
        spiderThread.interrupt();
    }
}
