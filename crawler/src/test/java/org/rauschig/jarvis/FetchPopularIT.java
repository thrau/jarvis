package org.rauschig.jarvis;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.junit.Test;
import org.travis4j.TravisClient;
import org.travis4j.model.Repository;

/**
 * FetchPopularIT.
 */
public class FetchPopularIT {
    @Test
    public void test() throws Exception {
        String[] repos = {
            "sakai-mirror/melete",
            "elasticsearch/elasticsearch",
            "brianchandotcom/liferay-portal",
            "sakai-mirror/ambrosia",
            "sakai-mirror/mneme",
            "TEAMMATES/repo",
            "netty/netty",
            "hazelcast/hazelcast",
            "WhisperSystems/TextSecure",
            "wildfly/wildfly",
            "spring-projects/spring-boot",
            "openmicroscopy/openmicroscopy",
            "libgdx/libgdx",
            "fabric8io/fabric8",
            "b3log/b3log-solo",
            "jcabi/jcabi-github",
            "openhab/openhab",
            "orientechnologies/orientdb",
            "square/okhttp",
            "M66B/XPrivacy",
            "neo4j/neo4j",
            "JetBrains/kotlin",
            "VoltDB/voltdb",
            "wordpress-mobile/WordPress-Android",
            "Netflix/RxJava",
            "kframework/k",
            "crate/crate",
            "cgeo/cgeo",
            "JetBrains/intellij-community",
            "mulesoft/mule",
            "jclouds/jclouds",
            "amplab/tachyon",
            "liferay/liferay-portal",
            "kuali/kc",
            "infinispan/infinispan",
            "square/picasso",
            "JabRef/jabref",
            "mocleiri/kuali-student",
            "facebook/presto",
            "dotCMS/dotCMS",
            "SpongePowered/SpongeAPI",
            "dkomanov/fizteh-java-2014",
            "micdoodle8/Galacticraft",
            "Gooru/Gooru-Web",
            "google/iosched",
            "brianchandotcom/liferay-plugins",
            "BuildCraft/BuildCraft",
            "spring-projects/spring-framework",
            "nostra13/Android-Universal-Image-Loader",
            "caelum/vraptor4",
        };

        List<Repository> result = Arrays.stream(repos)
                .map(r -> r.split("/"))
                .map(r -> {
                    try (TravisClient c = new TravisClient()) {
                        return c.getRepository(r[0], r[1]);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(r -> r.getActive() != null)
                .filter(r -> r.getLastBuildNumber() != null)
                .collect(Collectors.toList());

        for (Repository repository : result) {
            System.out.println(repository.getSlug());
        }

    }
}
