package org.rauschig.jarvis;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.rauschig.jarvis.service.RequestExecutor;
import org.rauschig.jarvis.service.S3LogDownloader;
import org.rauschig.jarvis.travis.RepositoryRequest;
import org.rauschig.jarvis.travis.RepositoryResult;
import org.rauschig.jarvis.travis.Result;
import org.travis4j.TravisClient;
import org.travis4j.model.Repository;

/**
 * RequestExecutorUT.
 */
public class RequestExecutorUT {

    private RequestExecutor executor;
    private TravisClient travis;

    @Before
    public void setUp() throws Exception {
        travis = new TravisClient();
        executor = new RequestExecutor(travis, null);
    }

    @After
    public void tearDown() throws Exception {
        travis.close();
    }

    @Test
    public void repositoryRequest_returnsCorrectRepository() throws Exception {
        Result<?> result = executor.execute(new RepositoryRequest("thrau", "jarchivelib"));

        assertThat(result, instanceOf(RepositoryResult.class));
        Repository repository = ((RepositoryResult) result).get();
        assertEquals("thrau/jarchivelib", repository.getSlug());
    }
}
