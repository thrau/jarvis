package org.rauschig.jarvis.comm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.junit.Test;
import org.rauschig.jarvis.mongo.WriteRequest;
import org.rauschig.jarvis.travis.MockRequest;

public class RequestDispatcherTest {

    @Test
    public void test() throws Exception {
        EnvelopeFactory envelopeFactory = new DefaultEnvelopeFactory(42);
        BlockingQueue<RequestEnvelope> dlRequestQueue = new ArrayBlockingQueue<>(10);
        BlockingQueue<WriteRequest<?>> writeRequests = new ArrayBlockingQueue<>(10);

        RequestDispatcher requestDispatcher = new RequestDispatcher(envelopeFactory, dlRequestQueue, writeRequests);

        MockRequest r1 = new MockRequest();
        MockRequest r2 = new MockRequest();

        requestDispatcher.dispatch(r1);
        requestDispatcher.dispatch(r2);

        assertEquals(2, dlRequestQueue.size());
        assertEquals(0, writeRequests.size());

        RequestEnvelope re1 = dlRequestQueue.take();
        RequestEnvelope re2 = dlRequestQueue.take();

        assertEquals(42, re1.getSpiderId());
        assertEquals(42, re2.getSpiderId());

        assertSame(r1, re1.getPayload());
        assertSame(r2, re2.getPayload());
    }
}
