package org.rauschig.jarvis.comm;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.junit.Rule;
import org.junit.Test;
import org.rauschig.jarvis.RedissonConnection;
import org.rauschig.jarvis.travis.RepositoryRequest;
import org.rauschig.jarvis.travis.Request;

public class DlRequestQueueUT {

    @Rule
    public RedissonConnection redisson = new RedissonConnection();

    @Test
    public void test() throws Exception {
        BlockingQueue<RequestEnvelope> queue = redisson.getClient().getBlockingQueue("jarvis:dlRequestQueue");

        queue.put(new RequestEnvelope(1, new RepositoryRequest("thrau", "jarchivelib")));

        RequestEnvelope take = queue.poll(10, TimeUnit.SECONDS);
        assertEquals(1, take.getSpiderId());
        Request payload = take.getPayload();
        assertNotNull(payload);
        assertThat(payload, instanceOf(RepositoryRequest.class));
        assertEquals("thrau", ((RepositoryRequest) payload).getOwner());
        assertEquals("jarchivelib", ((RepositoryRequest) payload).getName());
    }
}
