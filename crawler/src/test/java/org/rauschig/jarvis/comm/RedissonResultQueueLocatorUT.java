package org.rauschig.jarvis.comm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.BlockingQueue;

import org.junit.Rule;
import org.junit.Test;
import org.rauschig.jarvis.RedissonConnection;
import org.rauschig.jarvis.travis.MockResult;
import org.rauschig.jarvis.travis.Result;
import org.redisson.RedissonClient;

public class RedissonResultQueueLocatorUT {

    @Rule
    public RedissonConnection redisson = new RedissonConnection();

    @Test
    public void test() throws Exception {
        RedissonClient redissonClient = redisson.getClient();

        RedissonResultQueueLocator redissonResultQueueLocator = new RedissonResultQueueLocator(redissonClient);

        BlockingQueue<Result<?>> rq1 = redissonResultQueueLocator.getResultQueue(1);
        BlockingQueue<Result<?>> rq2 = redissonResultQueueLocator.getResultQueue(2);

        assertNotNull(rq1);
        assertNotNull(rq2);

        assertTrue(rq1.isEmpty());
        assertTrue(rq2.isEmpty());

        rq1.add(new MockResult("r1"));
        rq2.add(new MockResult("r2"));

        assertFalse(rq1.isEmpty());
        assertFalse(rq2.isEmpty());

        assertEquals(2, redissonResultQueueLocator.getAllResultQueues().size());

        redissonClient.getKeys().deleteByPattern("jarvis:dlResultQueue:*");
        assertEquals(0, redissonResultQueueLocator.getAllResultQueues().size());
    }

}
