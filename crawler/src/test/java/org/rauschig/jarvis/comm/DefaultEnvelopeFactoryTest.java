package org.rauschig.jarvis.comm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.Test;
import org.rauschig.jarvis.travis.MockRequest;
import org.rauschig.jarvis.travis.Request;

public class DefaultEnvelopeFactoryTest {
    @Test
    public void test() throws Exception {
        DefaultEnvelopeFactory envelopeFactory = new DefaultEnvelopeFactory(42);

        Request request = new MockRequest();

        RequestEnvelope envelope = envelopeFactory.createEnvelope(request);

        assertSame(request, envelope.getPayload());
        assertEquals(42, envelope.getSpiderId());

        System.out.println(envelope);
    }

}
