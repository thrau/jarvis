Crawler
=======

The jarvis crawler is a spring-boot application that is used to crawl data
from Travis-CI.

It provides a basic shell application via crash that allows you to initiate
crawling.

    ssh -p 2000 user@localhost
    password
    jarvis --help

for example

    jarvis resolve google iosched

will start mining the build data for the repository google/iosched
(repository, and build data, no logs yet).


Build
-----

Navigate into the jarvis root directory and run

    mvn clean install


Running
-------

### Requirements

* Running *MongoDB* (>= 3.2.x) instance (a db `jarvis` will be created)
* Running *Redis* (>= 3.2.x) instance (used by the crawlers to queue requests)
* A writable directory `/data/jarvis/logs` (this is where logs will be stored)

Use default configs when running the db instances (Mongo on `:27017`, Redis on
`:6389`).


### Running the application

After building, execute the spring-boot application using

    java -jar crawler/target/jarvis-crawler-0.2.0-SNAPSHOT.jar

FYI: stopping the application takes a while.


### Executing commands

The jarvis crawler provides a CRaSH application to issue commands and dispatch
new crawlers to do different things.

Connect to the application using ssh (password is `password`)

    ssh -p 2000 user@localhost

Run

    jarivs --help

To get a list of executable commands.


#### Resolve a repository

Resolving a repository means that all build data will be downloaded into
MongoDB for the given repository.

Execute

    jarvis resolve <owner> <repo-name>

To start the crawling process (best to use a small repo for testing, crawling
takes long due to the API request limits)


#### Download logs

Once build data has been downloaded (check `jarvis info` if the spider running
state is `false`) for the repository, use

    jarvis download_logs <owner> <repo-name>

To download the logs for all builds stored in the local db. Logs will be
downloaded into `/data/jarvis/logs/<repo-id>`
