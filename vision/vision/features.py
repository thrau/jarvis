from collections import Counter, defaultdict

from vision.commitclassifier import CommitAnalyzer, ChangeAnalyzer
from vision.data import *
from vision.filetypeclassifier import FileTypeClassifier, FileTypeModelLoader
from vision.metrics import CommitMetrics


def ratio(fn):
    def calc_ratio(*args):
        obs = dict(fn(*args))
        total = sum(obs.values())

        if total > 0:
            for k in obs:
                obs[k] /= total

        return obs

    return calc_ratio


class FileChurnColumnGroup(GitAwareColumnGroup):
    def get_names(self):
        return self.git.files

    def get_observation(self, build: Build):
        git = self.git

        changes = git.change_index[build.sha]

        obs = dict()
        for file in git.files:
            val = 0

            if file in changes:
                (flag, a, d) = changes[file]
                if a == "-" or d == "-":
                    a, d = -1, -1
                val = (int(a) + int(d))

            obs[file] = val

        return obs


class FileChangeColumnGroup(FileChurnColumnGroup):
    def get_observation(self, build: Build):
        return {k: v != 0 for k, v in super().get_observation(build).items()}


class ResultColumnGroup(TopologyAwareColumnGroup):
    def get_names(self):
        return ['is_original', 'state', 'result']

    def get_observation(self, build: Build) -> dict:
        return {
            'is_original': build.sha in self.topology.original_errors,
            'state': build.state,
            'result': build.result
        }


class IsMerge(GitAwareColumn):
    def get_name(self):
        return "is_merge"

    def get_observation(self, build: Build):
        return len(self.git.tree[build.sha]) > 1


class CommitSize(GitAwareColumn):
    def get_name(self):
        return "size"

    def get_observation(self, build: Build):
        return len(self.git.change_index[build.sha])


class CommitActivity(GitAwareColumn):
    def get_name(self):
        return "commit_activity"

    def get_observation(self, build: Build):
        return CommitMetrics(self.git.get_commit(build.sha)).activity()


class CommitStereotype(TopologyAwareColumn):
    analyzer = FileTypeClassifier()

    def init(self):
        self.analyzer.add_loader(FileTypeModelLoader(self.topology.builds))

    def get_name(self):
        return "commit_type"

    def get_observation(self, build: Build):
        return CommitAnalyzer(self.topology.git.get_commit(build.sha), self.analyzer).get_stereotype()


class ModificationEntropy(GitAwareColumn):
    def get_name(self):
        return "modification_entropy"

    def get_observation(self, build: Build):
        return CommitMetrics(self.git.get_commit(build.sha)).modification_entropy()


class AbstractFileTypeColumnGroup(TopologyAwareColumnGroup):
    analyzer = FileTypeClassifier()

    def init(self):
        self.analyzer.add_loader(FileTypeModelLoader(self.topology.builds))

    def get_names(self):
        return self.analyzer.get_type_names()


class FileTypeCount(AbstractFileTypeColumnGroup):
    def get_observation(self, build: Build) -> dict:
        names = self.get_names()
        result = dict(zip(names, [0] * len(names)))

        for file in self.topology.git.change_index[build.sha]:
            t = self.analyzer.classify(file)
            result[t] += 1

        return result


class FileTypeRatio(FileTypeCount):
    @ratio
    def get_observation(self, build: Build) -> dict:
        return super().get_observation(build)


class FileTypeChangeRatio(FileTypeCount):
    @ratio
    def get_observation(self, build: Build) -> dict:
        names = self.get_names()
        result = dict(zip(names, [0] * len(names)))

        for file, change in self.topology.git.change_index[build.sha].items():
            flag, a, d = change
            if a == "-" or d == "-":
                continue
            a, d = int(a), int(d)

            t = self.analyzer.classify(file)
            result[t] += (a + d)

        return result


class Reputation(TopologyAwareColumn):
    authors = dict()

    def init(self):
        git = self.topology.git

        authors = dict()

        for sha, attr in git.attribute_index.items():
            a = attr['ae']
            if a not in authors:
                authors[a] = {
                    'builds': list(),
                    'commits': list()
                }

            authors[a]['commits'].append(sha)

            if sha in self.topology.build_index:
                authors[a]['builds'].append(self.topology.build_index[sha])

        for author, attrs in authors.items():
            passed = [build for build in attrs['builds'] if build.result == 'passed']
            total = len(attrs['builds'])
            attrs['rep'] = len(passed) / total if total > 0 else 1

        self.authors = authors

    def get_name(self):
        return "reputation"

    def get_observation(self, build: Build):
        attrs = self.topology.git.attribute_index[build.sha]
        author = attrs['ae']

        return self.authors[author]['rep']


class BuildHistory(TopologyAwareColumnGroup):
    def get_names(self):
        return [
            "prev_1_result",
            "prev_2_result"
        ]

    def get_observation(self, build: Build) -> dict:
        prev = self.topology.get_previous(build)

        if not prev:
            return dict()

        pprev = self.topology.get_previous(prev)

        if not pprev:
            return dict()

        return {
            'prev_1_result': prev.result,
            'prev_2_result': pprev.result
        }


class Intent(TopologyAwareColumn):
    def get_observation(self, build: Build):
        prev = self.topology.get_previous(build)

        if not prev:
            return "forward"

        if len(self.topology.git.tree[build.sha]) > 1:
            return "merge"

        if prev.is_passed:
            return "forward"
        if prev.is_broken:
            return "fix"


class BuildClimate(TopologyAwareColumn):
    r = 10
    climate = dict()

    def init(self):
        builds = self.topology.get_topology_sorted_builds()
        n = len(builds)
        r = self.r

        for i in range(0, n):
            k = min(r, n - i)

            self.climate[builds[i].sha] = builds[i:(i + k)]

    def get_name(self):
        return "climate"

    def get_observation(self, build: Build):
        previous_builds = self.climate[build.sha]
        # FIXME: should consider distance to last broken build
        return Counter([build.is_broken() for build in previous_builds])[True] / self.r


class MultiCategoricalFileTypeChangeType(AbstractFileTypeColumnGroup):
    names = []

    def init(self):
        super().init()
        self.names = [(ft, ct)
                      for ft in self.analyzer.get_type_names()
                      for ct in ChangeAnalyzer.change_types]

    def get_observation(self, build: Build) -> dict:
        commit = self.topology.git.get_commit(build.sha)
        analyzer = CommitAnalyzer(commit, self.analyzer)

        changes = analyzer.aggregate_file_type_changes()

        return {(ft, ct): "-" if (ft not in changes or ct not in changes[ft]) else self.get_action(changes[ft][ct])
                for ft, ct in self.names}

    def get_names(self):
        return self.names

    @staticmethod
    def get_action(ad):
        d, a = ad

        if a == 0:
            if d == 0:
                return "-"
            else:
                return "a"
        else:
            if d == 0:
                return "d"
            else:
                return "m"


class MultiCategoricalFileTypeChangeTypeRatio(MultiCategoricalFileTypeChangeType):
    @ratio
    def get_observation(self, build: Build) -> dict:
        commit = self.topology.git.get_commit(build.sha)
        analyzer = CommitAnalyzer(commit, self.analyzer)

        changes = analyzer.aggregate_file_type_changes()

        tadd = lambda p: p[0] + p[1]

        obs = {(ft, ct): 0 if (ft not in changes or ct not in changes[ft]) else tadd(changes[ft][ct])
               for ft, ct in self.names}

        return obs


SHA = SimpleColumn("sha", lambda b: b.sha)
Author = SimpleColumn("author", lambda b: b.build['commit']['author_email'])
Number = SimpleColumn("number", lambda b: b.number)
Duration = SimpleColumn("duration", lambda b: b.build['duration'])
EventType = SimpleColumn("event_type", lambda b: b.build['event_type'])
Result = SimpleColumn("result", lambda b: b.result)

""" Hassan and Zhang 2006 """
BinaryResult = SimpleColumn("BinaryResult", lambda b: b.is_passed())

Time = SimpleColumn("Time", lambda b: b.date.hour)
WeekDay = SimpleColumn("WeekDay", lambda b: b.date.weekday())
MonthDay = SimpleColumn("MonthDay", lambda b: b.date.day)


class Experience(GitAwareColumn):
    author_index = dict()

    def init(self):
        for sha in self.git.topology:
            author = self.git.attribute_index[sha]['ae']

            if author not in self.author_index:
                self.author_index[author] = list()

            self.author_index[author].append(sha)

    def get_observation(self, build: Build):
        author = build.commit.attributes['ae']

        total = len(self.author_index[author])
        i = self.author_index[author].index(build.sha)
        exp = total - i

        return exp


class NumberOfFiles(TopologyAwareColumn):
    def get_observation(self, build: Build):
        shas = self.topology.build_range_index[build.sha]
        return sum([CommitMetrics(self.git.get_commit(sha)).num_files() for sha in shas])


class FileChangeHistory(TopologyAwareColumnGroup):
    def get_names(self):
        return [
            "MinFileChangeHistory",
            "MaxFileChangeHistory",
            "AvgFileChangeHistory"
        ]

    def get_observation(self, build: Build):
        shas = self.topology.build_range_index[build.sha]

        file_changes = defaultdict(int)
        for sha in shas:
            files = set(self.git.change_index[sha].keys())
            for file in files:
                changes = self.git.get_previous_changes(sha, file)

                file_changes[file] = max(file_changes[file], len(changes))

        if not file_changes:
            return dict()
        else:
            return {
                "MinFileChangeHistory": min(file_changes.values()),
                "MaxFileChangeHistory": max(file_changes.values()),
                "AvgFileChangeHistory": sum(file_changes.values()) / len(file_changes)
            }


class HistoricalFactors(TopologyAwareColumnGroup):
    def get_names(self):
        return [
            "DaysSinceLastFail",
            "BuildsSinceLastFail",
            "PreviousResult"
        ]

    def get_last_broken(self, build):
        # FIXME: does not consider tree topology
        ptr = self.topology.get_previous(build)
        i = 0

        while ptr and ptr.is_passed:
            ptr = self.topology.get_previous(ptr)
            i += 1

        return i, ptr

    def get_observation(self, build: Build) -> dict:
        prev = self.topology.get_previous(build)
        if not prev:
            return dict()

        tdays = 0
        pcnt, prev_broken = self.get_last_broken(build)
        if prev_broken:
            tdays = (build.date - prev_broken.date).days

        return {
            "DaysSinceLastFail": tdays,
            "BuildsSinceLastFail": pcnt,
            "PreviousResult": prev.is_passed
        }
