import os
import random

import pandas as pd

from vision.buildtopology import BuildTopologyAnalyzer
from vision.common import cached
from vision.git import Git


def objects_to_data_frame(objs, columns):
    data = list()

    for obj in objs:
        data.append({column: getattr(obj, column) for column in columns})

    return pd.DataFrame(data, columns=columns)


def dump_csv(df: pd.DataFrame, path, prefix, slug, **kwargs):
    fname = "{}___{}.csv".format(prefix, slug.replace("/", "$"))
    path = os.path.join(path, fname)
    df.to_csv(path, index=False, **kwargs)
    return path


def load_csv(path, prefix, slug, **kwargs):
    fname = "{}___{}.csv".format(prefix, slug.replace("/", "$"))
    path = os.path.join(path, fname)

    return pd.read_csv(path, **kwargs) if os.path.exists(path) else None


class DataFrameFactory:
    def __init__(self, topology):
        self.columns = list()
        self.topology = topology
        self.git = topology.git

    def add(self, column):
        self.columns.append(self.as_instance(column))

    def as_instance(self, column):
        # reflection magic for those extra 'wtf' debug sessions
        if isinstance(column, type):
            if issubclass(column, GitAwareColumn):
                column = column(self.git)
            elif issubclass(column, GitAwareColumnGroup):
                column = column(self.git)
            elif issubclass(column, TopologyAwareColumn):
                column = column(self.topology)
            elif issubclass(column, TopologyAwareColumnGroup):
                column = column(self.topology)
            else:
                raise ValueError("Unknown type %s" % column)

        return column

    def column_names(self):
        names = list()
        for col in self.columns:
            if isinstance(col, ColumnGroup):
                names += col.get_names()
            elif isinstance(col, Column):
                names.append(col.get_name())
            else:
                raise ValueError("The fuck is this? %s" % type(col))
        return names

    def column_meta_names(self):
        names = list()
        for col in self.columns:
            if isinstance(col, ColumnGroup):
                names.append(type(col).__name__)
            elif isinstance(col, Column):
                names.append(col.get_name())
            else:
                raise ValueError("The fuck is this? %s" % type(col))
        return names

    def __create_observation(self, data):
        obs = dict()
        for col in self.columns:
            if isinstance(col, ColumnGroup):
                obs.update(col.get_observation(data))
            elif isinstance(col, Column):
                obs[col.get_name()] = col.get_observation(data)
            else:
                raise ValueError("The fuck is this? %s" % type(col))
        return obs

    def create_data_frame(self, data_set, indexfn=None):
        index_data = None
        if indexfn is not None:
            index_data = [indexfn(data) for data in data_set]

        return pd.DataFrame(self.create_data(data_set), index=index_data, columns=self.column_names())

    def create_data(self, data_set):
        for column in self.columns:
            column.init()

        return [self.__create_observation(data) for data in data_set]


class BuildDataFrameFactory(DataFrameFactory):
    def create_data_frame(self, data_set=None, indexfn=lambda b: b.sha):
        return super().create_data_frame(None, indexfn)

    def create_resampled_data_frame(self):
        passed = [build for build in self.builds if build.is_passed()]
        broken = [build for build in self.builds if build.is_broken()]

        sample = random.sample(passed, min(len(broken), len(passed)))
        builds = sorted(sample + broken, key=lambda b: b.number)

        index = [build.sha for build in builds]
        return pd.DataFrame(self.create_data(builds), index=index, columns=self.column_names())

    def create_data(self, data_set=None):
        if data_set is None:
            data_set = self.builds

        return super().create_data(data_set)

    @cached
    def builds(self):
        """
        Returns a sorted (by number) and truncated (have to be in the git change index) list of builds.

        :return: list[Build]
        """
        builds = [build for build in self.topology.build_list if build.sha in self.git.change_index]
        return sorted(builds, key=lambda b: b.number)


class ColumnGroup:
    def init(self):
        pass

    def get_names(self):
        raise NotImplementedError

    def get_observation(self, data) -> dict:
        raise NotImplementedError


class Column:
    def init(self):
        pass

    def get_name(self):
        return type(self).__name__

    def get_observation(self, data):
        raise NotImplementedError


class SimpleColumn(Column):
    def __init__(self, name, fn=None):
        self.name = name

        if fn is None:
            def fn(d):
                return getattr(d, name)

        self.fn = fn

    def get_name(self):
        return self.name

    def get_observation(self, data):
        return self.fn(data)


class GitAwareColumn(Column):
    def __init__(self, git: Git):
        self.git = git


class TopologyAwareColumn(Column):
    def __init__(self, topology: BuildTopologyAnalyzer):
        self.topology = topology
        self.builds = topology.builds
        self.git = topology.git


class TopologyAwareColumnGroup(ColumnGroup):
    def __init__(self, topology: BuildTopologyAnalyzer):
        self.topology = topology
        self.builds = topology.builds
        self.git = topology.git


class GitAwareColumnGroup(ColumnGroup):
    def __init__(self, git: Git):
        self.git = git
