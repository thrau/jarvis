import pickle

from vision.git import Git


def __get_dump_path(path: str):
    return "/tmp/vision.cache.%s.dump" % path.replace("/", "$")


def dump_git(git):
    path = __get_dump_path(git.path)
    attrs = ['tree', 'log', 'attribute_index', 'change_index', 'patch_index', 'rename_index']
    save_cache(git, attrs, path)


def load_git(path: str, dump_if_not_exist=True) -> Git:
    dump_path = __get_dump_path(path)
    git = Git(path)
    try:
        load_cache(git, dump_path)
    except FileNotFoundError:
        if dump_if_not_exist:
            dump_git(git)

    return git


def load_cache(obj, path):
    with open(path, 'rb') as f:
        data = pickle.load(f)

    for attr, value in data.items():
        var = "__lazy__" + attr
        setattr(obj, var, value)


def save_cache(obj, attrs, path):
    cache = dict()
    for attr in attrs:
        cache[attr] = getattr(obj, attr)

    with open(path, 'wb') as f:
        pickle.dump(cache, f)
