from collections import defaultdict
from enum import Enum

from vision.builds import Build, BuildRepo, PullRequest, DateFilteredPullRequest
from vision.common import cached, lindex, nub, utc_now
from vision.git import Git, CommitRange, Commit


class BuildType(Enum):
    PUSH = 1
    PR = 2
    MERGE = 3
    PR_MERGE = 4
    UNKNOWN = 5


class TopoBuild(Build):
    """
    This class represents a Build that can be linked to a commit within the git tree.
    """

    def __init__(self, topo, build):
        self.topo = topo
        self.git, self.builds = topo.git, topo.builds

        if isinstance(build, int):
            obj = self.builds.build_index[build].build
            if obj is None:
                raise ValueError("No build with id {} found".format(build))
        elif isinstance(build, dict):
            obj = build
        elif isinstance(build, Build):
            obj = build.build
        else:
            raise ValueError("Unknown build type")

        super().__init__(obj)

    @cached
    def build_type(self):
        if self.is_push:
            # PUSH, PR_MERGE, MERGE
            if self.is_in_tree:
                # check reliable git commit information
                if self.git_commit.is_pr_merge:
                    return BuildType.PR_MERGE
                elif self.git_commit.is_merge:
                    return BuildType.MERGE
            else:
                if self.commit.summary.startswith("Merge pull request #"):
                    return BuildType.PR_MERGE
                elif self.commit.summary.startswith("Merge"):
                    # this is a guess heuristics
                    return BuildType.MERGE

            # TODO: squash based PR merges are unfortunately not detected
            return BuildType.PUSH

        elif self.is_pull_request:
            return BuildType.PR

        return BuildType.UNKNOWN

    @property
    def git_commit(self) -> Commit:
        return self.git.commits[self.sha] if self.is_in_tree else None

    @property
    def sha(self):
        return self.git.tags[self.tag] if self.is_tagged else None

    @property
    def tag(self):
        return "jarvis-{}".format(self.id)

    @property
    def is_tagged(self):
        return self.tag in self.git.tags

    @property
    def is_in_tree(self):
        return self.sha in self.git.tree

    @cached
    def pull_request(self) -> PullRequest:
        if self.is_pull_request:
            return DateFilteredPullRequest(self.pull_request_number, self.builds, self.started_at)

    @cached
    def prev_build_range(self):
        return [CommitRange(self.git, sha, self.sha) for sha in self.prev_built_commits]

    @cached
    def prev_built_commits(self):
        return self.topo.find_prev_built_commits(self.sha) if self.is_in_tree else list()

    @cached
    def prev_builds(self):
        lists = [self.topo.commit_builds_index[sha] for sha in self.prev_built_commits]
        return [b for b in [self.get_most_frequent(builds) for builds in lists] if b is not None]

    @cached
    def last_broken(self):
        marked = set()
        stack = list()

        failed = list()

        stack.append(self)

        while stack:
            cur = stack.pop()

            if cur.id in marked:
                continue
            marked.add(cur.id)

            if cur.is_broken and cur is not self:
                failed.append(cur)
                continue

            stack.extend(reversed(cur.prev_builds))

        return self.get_most_frequent(failed)

    def get_most_frequent(self, builds):
        th = self.started_at if self.started_at else utc_now()

        def key(b):
            return th if b.started_at is None else b.started_at

        l = [b for b in builds if key(b) < th]
        return max(l, key=key) if len(l) >= 1 else None

    def __str__(self):
        r = self.sha[:7] if self.is_in_tree else "not in tree"
        return "build#{} ({}) {} [{}]".format(self.number, self.id, self.state, r)


class PullRequestBuild(TopoBuild):
    """
    Special case of TopoBuild where the wrapped Build represents a build within a pull request. These are special
    because they build the 'pr-merge-commits' that GitHub creates.
    """

    def __init__(self, topo, build):
        super().__init__(topo, build)

    @cached
    def sha_range(self):
        summary = self.git.attribute_index[self.sha]['s']
        source, target = summary.split(" into ")
        return source[6:], target  # remove 'Merge ' prefix from source

    @cached
    def commit_range(self) -> CommitRange:
        return CommitRange(self.git, *self.sha_range[::-1]) if self.is_in_tree else None

    @cached
    def prev_pr_build(self):
        nr = self.pull_request_number

        builds = self.builds.pull_request_index[nr]
        builds.sort(key=lambda b: b['id'])

        for i in range(1, len(builds)):
            if builds[i]['id'] == self.id:
                return self.topo.topo_build_index[builds[i - 1]['id']]

        return None

    @cached
    def prev_built_commits(self):
        """
        This considers the topology of the fake merge commit, and the "shadowed" build commits. It does not care
        explicitly about the previous pull request build.

        :return:
        """
        commits = list()

        if self.is_in_tree:
            commits.extend(self.topo.find_prev_built_commits(self.sha_range[1], check_root=True))

            for sha in self.topo.find_prev_built_commits(self.sha_range[0], use_shadowed=True):
                if sha not in commits:
                    commits.append(sha)

        return commits

    @cached
    def prev_builds(self):
        """
        Unlike prev_built_commits, this takes, if possible, on the left branch of the merge commit the previous pr
        build. This way, also rebased pull requests get it in the list of previous builds, although it is technically
        not reachable.
        :return:
        """
        commits = list()

        if self.is_in_tree:
            prev = self.prev_pr_build
            if prev is None:
                commits = self.prev_built_commits
            else:
                commits.extend(self.topo.find_prev_built_commits(self.sha_range[1], check_root=True))
                if prev.sha not in commits:
                    commits.append(prev.sha)

        lists = [self.topo.commit_builds_index_shadowed[sha] for sha in nub(commits)]
        return [b for b in [self.get_most_frequent(builds) for builds in lists] if b is not None]

    @cached
    def merge_base(self):
        return self.git.merge_base(*self.sha_range)

    def __str__(self):
        r = self.commit_range if self.is_in_tree else "not in tree"
        return "build#{} ({}) {} [pr#{} ({})]".format(self.number, self.id, self.state, self.pull_request_number, r)


class BuildTopologyAnalyzer:
    def __init__(self, git: Git, builds: BuildRepo):
        self.git = git
        self.builds = builds

    def get_pr_builds(self, pr):
        return [self.topo_build_index[build.id] for build in pr.builds]

    @cached
    def builds_sorted(self):
        return sorted(self.topo_build_index.values(), key=lambda b: b.id)

    @cached
    def topo_build_index(self):
        """
        Returns an dict of int -> TopoBuild (build id to TopoBuild). In case that the build is a pull request build, it
        will contain a PullRequestBuild instance at the respective id.

        :return: dict(int, TopoBuild)
        """
        return {bid: PullRequestBuild(self, build) if build.is_pull_request else TopoBuild(self, build)
                for bid, build in self.builds.build_index.items()}

    @cached
    def commit_builds_index(self):
        """
        Returns an index sha -> list(TopoBuild). With this, one can lookup the (possibly multiple) builds that were
        triggered by the respective commit.
        :return:
        """
        return lindex(self.topo_build_index.values(), lambda b: b.sha)

    @cached
    def commit_builds_index_shadowed(self):
        updated = defaultdict(list)

        updated.update(self.commit_builds_index)

        for build in self.topo_build_index.values():
            if build.is_pull_request and build.is_in_tree:
                source = build.sha_range[0]
                updated[source].append(build)

        return dict(updated)

    def find_prev_built_commits(self, sha, check_root=False, use_shadowed=False):
        """
        Traverses the tree from the given commit and finds, for each branch, commits that have already been included in
        a build.
        :param sha:
        :param check_root:
        :param use_shadowed: whether or not to check shadowed PR builds
        :return:
        """
        # TODO: ugh
        index = self.commit_builds_index if not use_shadowed else self.commit_builds_index_shadowed

        ret = list()
        marked = set()
        stack = list()
        take = check_root

        stack.append(sha)

        while len(stack) > 0:
            sha = stack.pop()
            marked.add(sha)

            if not sha:
                continue

            if sha in index and take:
                if sha not in ret:
                    ret.append(sha)
            else:
                for ancestor in reversed(self.git.tree[sha]):
                    if ancestor not in marked:
                        stack.append(ancestor)

            if not take:
                take = True

        return ret
