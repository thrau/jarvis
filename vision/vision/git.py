import os
from collections import defaultdict

import sh
import whatthepatch

from vision.commitclassifier import classify, ChangeAnalyzer
from vision.common import cached, join, dmap, nub, as_date


def parse_rename(path: str):
    """
    Parses a rename encoded in the `git log --numstat` file renaming syntax and returns a tuple of source and target.

    parse_rename("path/{A => B}") == ("path/A", "path/B")

    :param path: the rename string
    :return:
    """
    # we assume that file names do not contain curly braces
    arr = path.split(" => ")

    if '{' not in arr[0]:
        return arr

    prefix, a = [p.strip("/") for p in arr[0].split("{")]
    b, suffix = [p.strip("/") for p in arr[1].split("}")]

    pa = [p for p in [prefix, a, suffix] if p]
    pb = [p for p in [prefix, b, suffix] if p]

    return os.path.join(*pa), os.path.join(*pb)


def is_sha(string: str):
    return len(string) == 40 and string.isalnum()


def git_path(slug: str):
    # TODO parameterize path using an env. variable or smth.
    return os.path.join("/data/jarvis/repos", slug)


class Git:
    path = None

    def __init__(self, path):
        self.path = path

    def get_path(self):
        return self.path

    def exists(self):
        return os.path.exists(self.path)

    def call(self, *args):
        cmd = sh.git("-C", self.get_path(), "--no-pager", args)
        cmd.call_args["decode_errors"] = 'replace'
        return cmd

    def read(self, *args):
        return self.call(*args).splitlines()

    def get_commit(self, sha):
        return self.commits[sha] if sha in self.tree else Commit(self, sha)

    def get_remote_slug(self, name="origin"):
        lines = [line for line in self.read("remote", "-v")
                 if line.startswith(name) and line.endswith("(fetch)")]

        if len(lines) != 1:
            raise RuntimeError("No remote named", name)

        url = lines[0][len(name) + 1:-8].strip()
        if url.endswith(".git"):
            url = url[:-4]

        i = url.find(":")
        if i:
            url = url[i + 1:]

        parts = url.split("/")
        return "/".join(parts[-2:])

    @cached
    def commits(self):
        return {sha: Commit(self, sha) for sha in self.tree}

    @cached
    def log(self):
        """
        Returns a list of tuples where each tuple contains a log entry with its sha, date and author.
        :return: a list of tuples
        """
        return self.__resolve_log()

    @cached
    def tree(self):
        """
        Returns a dictionary that maps sha -> [sha], that represents the history tree. Each value contains a list of its
        direct predecessors.

        :return: dict[str, list[str]]
        """
        return Indexer(self).create_tree_index()

    @cached
    def tags(self):
        """
        Returns a dictionary that maps tag -> sha.

        :return: dict[str,str]
        """
        return Indexer(self).create_tag_index()

    @cached
    def attribute_index(self):
        return Indexer(self).create_attribute_index()

    @cached
    def change_index(self):
        return Indexer(self).create_change_index()

    @cached
    def patch_index(self):
        return Indexer(self).create_patch_index()

    @cached
    def rename_index(self):
        return Indexer(self).create_rename_index()

    @cached
    def file_history(self):
        files = defaultdict(list)
        add_to = defaultdict(list)

        for sha in self.topology:
            changes = self.change_index[sha]
            renames = self.rename_index[sha] if sha in self.rename_index else None

            if renames:
                # "also add changes that were done to f1 to f2"
                for f1, (f2, _) in renames.items():
                    add_to[f1].append(f2)

                    if f2 in add_to:
                        # unfold rename history
                        add_to[f1].extend(add_to[f2])

            for file in changes:
                files[file].append(sha)

                if file in add_to:
                    for other in add_to[file]:
                        files[other].append(sha)

        return dmap(files, nub)

    @cached
    def head(self):
        return self.call("rev-parse", "HEAD").strip()

    @cached
    def initial_commit(self):
        shas = self.read("rev-list", "--topo-order", "--max-parents=0", "HEAD")
        return shas[-1]

    @cached
    def files(self):
        return self.get_files("HEAD")

    @cached
    def topology(self):
        return self.read("rev-list", "--all", "--topo-order", "HEAD")

    @cached
    def reachable(self):
        return set(self.read("rev-list", "HEAD"))

    def merge_base(self, *shas):
        try:
            return self.read("merge-base", *shas)[0]
        except sh.ErrorReturnCode:
            return None

    def get_files(self, ref="HEAD"):
        return self.read("ls-tree", "-r", "--name-only", ref)

    def get_commit_info(self, sha):
        output = self.call("show", "--no-patch", '--format="%H;%ai;%ae;%s"', sha).strip().strip('"').split(";")

        return {
            'sha': output[0],
            'date': as_date(output[1]),
            'author': output[2],
            'subject': " ".join(output[3:])
        }

    def get_commit_count(self):
        return int(self.call("rev-list", "HEAD", "--all", "--count"))

    def get_age(self):
        first = self.get_commit_info(self.initial_commit)['date']
        last = self.get_commit_info(self.head)['date']
        return (last - first).days

    def get_previous_changes(self, sha, file):
        """
        Returns all commits that, previous to the given sha (in topology order), changed the given file.
        :param sha: the current point in the history
        :param file: the file
        :return: a list of shas
        """
        # all commits that, previous to the current commit, changed the file
        history = self.file_history[file]
        pos = history.index(sha)
        changes = history[pos:]
        return changes

    def classify_commits(self):
        return [(sha, classify(msg)) for sha, _, msg in self.log]

    def get_commit_activity_stats(self):
        stats = {}

        for (_, activity) in self.classify_commits():
            if activity not in stats:
                stats[activity] = 0

            stats[activity] += 1

        return stats

    def prune_index(self, index):
        """
        Assumes an index that maps a sha to some other value. The method returns a dict that contains only those values,
        whose sha is also in the tree of this repository.

        :param index: the index to prune
        :return: a pruned index
        """
        return {key: v for key, v in index.items() if key in self.tree}

    def __resolve_log(self):
        def line2entry(entry):
            cols = entry.strip().strip("'").split(";")

            if len(cols) < 3:
                return None

            sha = cols[0]
            d = as_date(cols[1])
            m = " ".join(cols[2:])

            return sha, d, m

        lines = [line2entry(line) for line in self.read("log", "--all", "--format='%H;%ci;%s'")]
        return [line for line in lines if line is not None]


class Indexer:
    def __init__(self, git):
        self.git = git

    def create_tree_index(self):
        rows = self.git.read("log", "--all", "--format=%H;%P")
        return {sha: [s for s in parents.split(' ') if s]
                for sha, parents in [row.strip().split(";") for row in rows]}

    def create_change_index(self):
        """
        Combines the values of the numstat index and the name_status index into one.

        :return: dict
        """
        numstat = self.create_numstat_index()
        name_status = self.create_name_status_index()

        index = dict()
        for sha in numstat:
            index[sha] = join(numstat[sha], name_status[sha], lambda ad, flag: (flag, ad[0], ad[1]))

        return index

    def create_name_status_index(self):
        lines = self.git.read("log", "--all", "--format=%H", "--name-status", "-M")

        index = dict()

        sha = None
        for line in lines:
            if len(line) == 0:
                continue

            if is_sha(line):
                sha = line
                index[sha] = dict()
                continue

            arr = line.split("\t")
            if len(arr) == 2:
                (flag, f) = arr
            elif len(arr) == 3:
                (flag, f1, f2) = arr
                # p = int(flag[1:])
                # flag = "R" if p == 100 else "M"
                f, flag = f2, "R"
            else:
                raise ValueError("Unparsable line %s" % line)

            index[sha][f] = flag

        return index

    def create_numstat_index(self):
        lines = self.git.read("log", "--all", "-w", "--format=%H", "--numstat", "-M")

        index = dict()

        sha = None
        for line in lines:
            if len(line) == 0:
                continue

            if is_sha(line):
                sha = line
                index[sha] = dict()
                continue

            (a, d, f) = line.split("\t")

            if " => " in f:  # renames
                _, f = parse_rename(f)

            index[sha][f] = (a, d)

        return index

    def create_attribute_index(self):
        keys = ["H", "P", "ae", "an", "ai", "s", 'ce', 'ci']
        delim = "_____"

        def parse_attrs(line):
            return dict(zip(keys, [col.strip() for col in line.split(delim)]))

        format = "--format=" + delim.join([' %' + a for a in keys])
        # need the whitespace for possible empty columns so zip works properly
        lines = self.git.read("log", "--all", "-w", "--no-renames", format)

        index = dict()

        for line in lines:
            attrs = parse_attrs(line)
            index[attrs['H']] = attrs

        return index

    def create_patch_index(self):
        difflines = self.git.read("log", "--all", "--format=@@@@%H", "-M", "--patch")

        patches = dict()
        patch = list()

        sha = None
        for line in difflines:
            if not isinstance(line, str):
                continue

            if line.startswith("@@@@"):
                if patch:  # dump previous patch, special case for first line
                    patches[sha] = PatchParser(patch).diffs()

                sha = line[4:]
                patch.clear()
                continue

            if not sha:  # special case for first lines
                continue

            patch.append(line)

        # special case for last element
        patches[sha] = PatchParser(patch).diffs()

        return patches

    def create_rename_index(self):
        lines = self.git.read("log", "--all", "--format=%H", "--diff-filter=R", "--name-status", "-M")

        index = dict()

        sha = None
        for line in lines:
            if len(line) == 0:
                continue

            if is_sha(line):
                sha = line
                index[sha] = dict()
                continue

            r, f1, f2 = line.split("\t")
            index[sha][f1] = (f2, int(r[1:]))

        return index

    def create_tag_index(self):
        index = dict()

        for line in self.git.read("show-ref", "--tags"):
            (sha, ref) = line.split(" ", 1)
            ref = ref[10:]
            index[ref] = sha

        return index


class Commit:
    def __init__(self, git: Git, sha):
        if not is_sha(sha):
            raise ValueError("not a valid sha: {}", sha)

        self.git = git
        self.sha = sha

    @property
    def exists(self):
        return self.sha in self.git.tree

    @property
    def attributes(self):
        return self.git.attribute_index[self.sha]

    @property
    def file_stats(self):
        return self.git.change_index[self.sha]

    @property
    def date(self):
        return as_date(self.attributes['ai'])

    @property
    def committer_date(self):
        return as_date(self.attributes['ci'])

    @property
    def is_merge(self):
        return len(self.parent_shas) >= 2

    @cached
    def merge_base(self):
        if self.is_merge:
            mb = self.git.merge_base(*self.parent_shas)
            return self.git.commits[mb]

    @property
    def parents(self):
        return [self.git.commits[sha] for sha in self.parent_shas]

    @property
    def parent_shas(self):
        return self.git.tree[self.sha]

    @property
    def is_pr_merge(self):
        if self.exists:
            return self.attributes['s'].startswith("Merge pull request #")

    @property
    def pr_merge_number(self):
        if self.is_pr_merge:
            return int(self.attributes['s'][20:].split(" ", 1)[0])

    def __str__(self, *args, **kwargs):
        return "Commit[{} '{}']".format(self.sha[:7], self.attributes['s'])


class CommitRange:
    def __init__(self, git, notin, isin):
        self.notin = notin
        self.isin = isin
        self.git = git

    @property
    def exists(self):
        return self.notin in self.git.tree and self.isin in self.git.tree

    @property
    def range(self):
        return self.notin, self.isin

    @cached
    def flat(self):
        """
        Gives access to a flat list of commits within this range. If the range is linear (there are no commits with
        multiple ancestors in the range), then the commits are calculated using the Git class's tree index. Otherwise,
        a call to `git log` is made.

        :return: a list of SHAs
        """
        if self.notin == self.isin:
            return []
        elif self.is_linear:
            return self.__linflat()
        else:
            return self.git.read("log", "--format=%H", "--topo-order", self.format_range())

    @cached
    def is_linear(self):
        cur = self.isin

        while cur != self.notin:
            ancestors = self.git.tree[cur]

            if len(ancestors) == 0:
                return True
            if len(ancestors) > 1:
                return False

            cur = ancestors[0]

        return True

    def format_range(self, connect=".."):
        return "{}{}{}".format(self.notin, connect, self.isin)

    def __linflat(self):
        """
        Assumes that the range is linear and returns a flat list of reachable commits.

        :return: a list of SHAs
        """
        hist = list()

        cur = self.isin
        hist.append(cur)
        while True:
            ancestors = self.git.tree[cur]

            if len(ancestors) == 0:
                return hist
            cur = ancestors[0]

            if cur == self.notin:
                break

            hist.extend(ancestors)

        return hist

    def __str__(self):
        return "{}..{}".format(self.notin[0:7], self.isin[0:7])

    @classmethod
    def of(cls, notin, isin):
        return CommitRange(notin.git, notin.sha, isin.sha)


class PatchParser:
    #       'file'      []
    # patch <>---- diff <>---- change

    def __init__(self, patch):
        self.patch = whatthepatch.parse_patch(patch)

    def diffs(self):
        return dict([(self.file(diff), list(self.changes(diff))) for diff in self.patch
                     if diff.header])

    @staticmethod
    def changes(diff):
        file = PatchParser.file(diff)

        for d, a, line in diff.changes:
            if d is None:
                t = 1
                l = a
            elif a is None:
                t = -1
                l = d
            else:
                continue

            yield (l, t, ChangeAnalyzer.get_change_type(line, file))

    @staticmethod
    def file(diff):
        if diff.header.new_path == "/dev/null":
            path = diff.header.old_path
        else:
            path = diff.header.new_path

        if path.startswith("b/") or path.startswith("a/"):
            # let's hope that no one uses 'a/' or 'b/' as a real level 1 directory
            return path[2:]

        return path
