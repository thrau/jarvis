from enum import Enum

from vision.git import CommitRange


class Scenario(Enum):
    A = ([True, True, True, True])
    B = ([True, True, True, False])
    C = ([True, True, False, True])
    D = ([True, True, False, False])
    E = ([True, False, True, True])  # *
    F = ([True, False, True, False])  # *
    G = ([True, False, False, True])  # *
    H = ([True, False, False, False])
    I = ([False, True, True, True])
    J = ([False, True, True, False])
    K = ([False, True, False, True])
    L = ([False, True, False, False])
    M = ([False, False, True, True])  # *
    N = ([False, False, True, False])
    O = ([False, False, False, True])  # *
    P = ([False, False, False, False])
    SIMPLE_1 = ([True])
    SIMPLE_2 = ([False])
    NONE = ([])

    @property
    def is_implausible(self):
        return self in [Scenario.E, Scenario.F, Scenario.G, Scenario.M, Scenario.O]


def is_case1(b):
    """
    Pulling into foundations: the target of the merge is the merge-base of the source and target.
    :param b: the current build
    :return:
    """
    return b.sha_range[1] == b.merge_base


def is_case2(b1, b2):
    """
    The PR has advanced: Changes have been introduced to the PR since the last build. The source of the previous build
    is therefore different from the source of the current build.
    :param b1: the previous build
    :param b2: the current build
    :return:
    """
    s1, _ = b1.sha_range
    s2, _ = b2.sha_range

    return s1 != s2


def is_case3(b1, b2):
    """
    Upstream has advanced: Since the last build, changes ware made in the branch being merged into. The target of the
    previous build is therefore different from the target of the current build.
    :param b1: the previous build
    :param b2: the current build
    :return:
    """
    _, t1 = b1.sha_range
    _, t2 = b2.sha_range

    return t1 != t2


def is_case4(b1, b2):
    """
    The history of the PR was changed: a special case of Case 2, which states that the branch of the history of the PR
    was somehow re-written, either via rebase, amend or similar commands.
    This means, that the previous builds has commits that are not reachable from the current build.
    :param b1: the previous build
    :param b2: the current build
    :return:
    """
    commits = [sha for sha in CommitRange(b1.git, b2.sha, b1.sha).flat if sha != b1.sha]

    return len(commits) > 0


def determine_scenario(pr_build):
    """
    Determines the scenario for a single PullRequestBuild
    :param pr_build: PullRequestBuild
    :return:
    """
    b2 = pr_build
    b1 = pr_build.prev_pr_build

    preds = []

    if b2.is_in_tree:
        preds = [is_case1(b2)]

        if b1 is not None:
            if b1.sha == b2.sha and b1.sha_range == b2.sha_range:
                # rebuild of the same commit
                return determine_scenario(b1)
            elif b1.is_in_tree:
                preds.append(is_case2(b1, b2))
                preds.append(is_case3(b1, b2))
                preds.append(is_case4(b1, b2))
            else:
                # This avoids subsequent pr builds to get only a "SIMPLE" scenario assigned if the previous build
                # doesn't exist in the tree
                preds = []

    return Scenario(preds)


def determine_scenarios(pr, topo):
    """
    Determines the scenarios of all builds in the pr.

    :param pr: PullRequest
    :param topo: BuildTopologyAnalyzer
    :return:
    """
    builds = topo.get_pr_builds(pr)

    scenarios = list()

    for i in range(0, len(builds)):
        b2 = builds[i]
        preds = []

        if b2.is_in_tree:
            preds = [is_case1(b2)]

            if i > 0:
                b1 = builds[i - 1]

                if b1.sha == b2.sha and b1.sha_range == b2.sha_range:
                    # rebuild of the same commit
                    preds = scenarios[-1]
                elif b1.is_in_tree:
                    preds.append(is_case2(b1, b2))
                    preds.append(is_case3(b1, b2))
                    preds.append(is_case4(b1, b2))
                else:
                    preds = []

        scenarios.append(Scenario(preds))

    return scenarios
