import hashlib
import os
import pickle
from collections import defaultdict
from datetime import timezone, datetime
from time import time

import dateutil.parser
import pytz


def cached(fn):
    """
    Property decorator that caches the result of the function call into an attribute of the object.

    class Foo:
        @cached
        def myfoo(self):
            do_things()

    in this scenario, when access foo.myfoo, the function body will only be executed once, then the cached value is
    returned

    :param fn: the decorated function that is used as factory
    :return: a property that lazy-loads the value
    """

    def lazy(obj):
        var = "__lazy__" + fn.__name__

        if not hasattr(obj, var):
            setattr(obj, var, fn(obj))

        return getattr(obj, var)

    return property(lazy)


def st_time(func):
    """
     st decorator to calculate the total time of a func

    :param func: the decorated function
    :return: a decorator function
    """

    def st_func(*args, **kwargs):
        t1 = time()
        r = func(*args, **kwargs)
        t2 = time()
        print("Function=%s, Time=%s" % (func.__name__, t2 - t1))
        return r

    return st_func


def as_date(string):
    return dateutil.parser.parse(string)


def as_utc_date(string):
    return dateutil.parser.parse(string).astimezone(timezone.utc)


def utc_now():
    return pytz.utc.localize(datetime.now())


def pseudo_id(string, l=7):
    return hashlib.md5(string.encode("UTF-8")).hexdigest()[:l]


def join(m1, m2, combinator=lambda x, y: (x, y)):
    result = dict()
    for key in m1:
        result[key] = combinator(m1[key], m2[key])
    return result


def lstr(lst, strfn=str, sep="\n"):
    """
    Converts a list to a string where list elements are separated with newlines.
    :param lst: the list
    :param strfn: the function used to generate a string from an element
    :return: a string
    """
    return sep.join([str(strfn(x)) for x in lst])


def lindex(items, key):
    """
    Creates a dict of lists where each item in the list has the value the key function returns.

    :param items: the items to index
    :param key: the key to index by
    :return: a dict of lists
    """
    index = defaultdict(list)

    for item in items:
        index[key(item)].append(item)

    return dict(index)


def tsum(tls):
    """
    Calculates the sum of a list of tuples and returns it as a tuple.

    :param tls:
    :return:
    """
    if not tls:
        return 0, 0
    return tuple(sum(x) for x in zip(*tls))


def transpose(d):
    """
    Swaps keys and values of a dictionary, and returns a dict of sets, where the set includes the keys that previously
    mapped the same values.
    :param d: the dict to transpose
    :return: a new dict where the values are now keys
    """
    t = defaultdict(set)

    for k, v in d.items():
        t[v].add(k)

    return dict(t)


def dmap(d, fn):
    """
    Wraps a dict comprehension.

    :param d: the dictionary
    :param fn: the function to map on to each item
    :return:
    """
    return {k: fn(v) for k, v in d.items()}


def ddmap(dd, fn):
    """
    Wraps a nested dict comprehension, i.e. maps the given function to each
    value in a dictionary of dictionaries

    :param dd: dict[i, dict[j, u]]
    :param fn: a function that takes both keys and the leaf value and returns a new value
    :return: a new dict of dicts
    """
    return {i: {j: fn(v) for j, v in d.items()} for i, d in dd.items()}


def nub(seq, idfun=None):
    """
    The nub function removes duplicate elements from a list. In particular, it keeps only the first occurrence of each
    element. (nub means 'essence')

    function: https://www.peterbe.com/plog/uniqifiers-benchmark
    name: http://hackage.haskell.org/package/base-4.9.0.0/docs/Data-List.html#v:nub

    :param seq: the sequence to de-duplicate
    :param idfun:
    :return:
    """
    # order preserving
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        # in old Python versions:
        # if seen.has_key(marker)
        # but in new ones:
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result


class PickleJar:
    def __init__(self, path):
        self.path = path

    def load(self, default=dict):
        if os.path.exists(self.path):
            with open(self.path, 'rb') as f:
                return pickle.load(f)

        return default()

    def dump(self, obj):
        with open(self.path, 'wb') as f:
            pickle.dump(obj, f)
