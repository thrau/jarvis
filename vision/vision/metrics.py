from math import log2
from vision.commitclassifier import classify, Activity
from vision.git import Git, Commit


def entropy(values):
    s = sum(values)

    if s == 0:
        return - 0

    p = [v / s for v in values]
    return - sum([pk * log2(pk) for pk in p if pk > 0])


class CommitMetrics:
    commit = None
    stats = None

    csv_header = ["sha", "activity", "is_merge", "num_files", "num_added_files", "num_modified_files",
                  "num_deleted_files", "sum_lines_added", "sum_lines_deleted", "modification_entropy"]

    def __init__(self, commit: Commit):
        self.commit = commit
        self.stats = commit.file_stats

    def activity(self):
        return Activity.to_string(classify(self.commit.attributes['s']))

    def is_merge(self):
        return len(self.commit.attributes['P'].split(" ")) > 1

    def sum_lines_added(self):
        return sum([int(added) for (_, added, _) in self.stats.values() if added != "-"])

    def sum_lines_deleted(self):
        return sum([int(deleted) for (_, _, deleted) in self.stats.values() if deleted != "-"])

    def num_modified_files(self):
        return self.num_files("M")

    def num_added_files(self):
        return self.num_files("A")

    def num_deleted_files(self):
        return self.num_files("D")

    def num_files(self, flag=None):
        if flag is None:
            return len(self.stats)

        return len([f for (f, _, _) in self.stats.values() if f == flag])

    def modification_entropy(self):
        mods = [int(a) + int(d) for (_, a, d) in self.stats.values() if a != "-" and d != "-"]
        return entropy(mods)

    def to_dict(self):
        return {
            "activity": self.activity(),
            "is_merge": self.is_merge(),
            "num_files": self.num_files(),
            "num_added_files": self.num_added_files(),
            "num_modified_files": self.num_modified_files(),
            "num_deleted_files": self.num_deleted_files(),
            "sum_lines_added": self.sum_lines_added(),
            "sum_lines_deleted": self.sum_lines_deleted(),
            "modification_entropy": self.modification_entropy(),
        }

    def to_csv(self, delim='\t'):
        keys = self.csv_header[1:]

        c = self.commit
        m = self.to_dict()
        return c.sha + delim + delim.join([str(m[key]) for key in keys])
