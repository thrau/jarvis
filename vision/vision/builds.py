import atexit
import itertools
import os
from collections import defaultdict

import pymongo

from vision.common import as_utc_date, cached, utc_now


class MongoDb:
    open = False

    @property
    def is_open(self):
        return self.open

    @cached
    def client(self):
        self.open = True
        return pymongo.MongoClient("mongodb://localhost:27017")

    @cached
    def db(self):
        return self.client.jarvis

    @property
    def builds(self):
        return self.db.buildjsonobject

    @property
    def repos(self):
        return self.db.repositoryjsonobject

    @property
    def jobs(self):
        return self.db.jobjsonobject

    def close(self):
        if self.is_open:
            self.open = False
            self.client.close()


mongo = MongoDb()
atexit.register(lambda: mongo.close())


class BuildRepoCommit:
    def __init__(self, commit):
        self.commit = commit

    @property
    def sha(self):
        return self.commit['sha']

    @property
    def asha(self):
        return self.sha[:7]

    @property
    def message(self):
        return self.commit['message']

    @cached
    def summary(self):
        lines = self.message.splitlines()

        if len(lines) == 1:
            return lines[0]

        return " ".join(itertools.takewhile(lambda l: l.strip(), lines))

    @property
    def author_name(self):
        return self.commit['author_name']

    @property
    def author_email(self):
        return self.commit['author_email']

    @property
    def date(self):
        return as_utc_date(self.commit['committed_at'])

    @property
    def branch(self):
        return self.commit['branch']

    @property
    def is_pull_request(self):
        return self.commit['pull_request_number'] is not None

    @property
    def is_push(self):
        return self.commit['pull_request_number'] is None

    @property
    def pull_request_number(self):
        return self.commit['pull_request_number'] if self.is_pull_request else None

    @property
    def is_pr_merge(self):
        return self.summary.startswith("Merge pull request #")

    @property
    def commit_range(self):
        if self.is_push:
            return self.revs.split("...")

        return None

    @property
    def revs(self):
        return self.commit['compare_url'].split("/")[-1] if self.is_push else None

    def pretty(self):
        return "{} {}: {} ({})".format(self.asha, self.author_name, self.summary, self.date)

    def __str__(self, *args, **kwargs):
        return self.commit.__str__(*args, **kwargs)


class Build:
    def __init__(self, build):
        self.build = build

    @property
    def id(self):
        return int(self.build['id'])

    @property
    def number(self):
        return int(self.build['number'])

    @cached
    def commit(self) -> BuildRepoCommit:
        return BuildRepoCommit(self.build['commit'])

    @property
    def event_type(self):
        return self.build['event_type']

    @property
    def is_broken(self):
        return self.state in ['errored', 'failed']

    @property
    def is_passed(self):
        return self.state in ['passed']

    @property
    def is_pull_request(self):
        return self.build['event_type'] == "pull_request"

    @property
    def is_push(self):
        return self.build['event_type'] == "push"

    @property
    def pull_request_number(self):
        return self.build['pull_request_number']

    @property
    def state(self):
        return self.build['state']

    @property
    def result(self):
        build = self.build

        if 'category' in build:
            return build['category']
        else:
            return build['state']

    @property
    def duration(self):
        return self.build['duration']

    @property
    def started_at(self):
        datestring = self.build['started_at']
        return as_utc_date(self.build['started_at']) if datestring else None

    @property
    def finished_at(self):
        datestring = self.build['started_at']
        return as_utc_date(self.build['finished_at']) if datestring else None

    def pretty(self):
        return "#{} ({}) {} [{}]".format(self.number, self.id, self.state, self.commit.sha[0:7])

    def __str__(self, *args, **kwargs):
        return self.build.__str__(*args, **kwargs)


class BuildRepo:
    def __init__(self, slug, database=mongo, repo=None):
        self.database = database

        self.repos = self.database.repos
        self.builds = self.database.builds
        self.jobs = self.database.jobs

        if repo is None:
            if isinstance(slug, int):
                self.repo = self.find_repo_by_id(slug)
                self.slug = self.repo['slug']
            else:
                self.repo = self.find_repo(slug)
                self.slug = slug
        else:
            self.repo = repo
            self.slug = repo['slug']

    @cached
    def build_index(self):
        """
        Creates an index of build ids to Build objets, i.e. int -> Build where keys are the build ids.
        :return: a dict
        """
        return {b['id']: Build(b) for b in self.get_builds()}

    @cached
    def pull_requests(self):
        return {nr: PullRequest(nr, self) for nr in self.get_pull_request_numbers()}

    @cached
    def pull_request_index(self):
        index = defaultdict(list)

        for pr in self.get_pull_requests():
            index[pr['pull_request_number']].append(pr)

        return index

    def get_git_path(self, root="/data/jarvis/repos"):
        return os.path.join(root, self.slug)

    def has_git_path(self):
        return os.path.exists(self.get_git_path())

    def find_repo(self, slug):
        for r in self.repos.find({"slug": slug}):
            return r

        raise Exception("No such repo '%s'" % slug)

    def find_repo_by_id(self, rid):
        for r in self.repos.find({"id": rid}):
            return r

        raise Exception("No repo with id '%s'" % rid)

    def get_id(self):
        return self.get_repo()['id']

    def get_repo(self):
        return self.repo

    def get_builds(self):
        return self.builds.find({"repository_id": self.get_id()})

    def get_pull_request_numbers(self):
        query = {
            "repository_id": self.get_id(),
            "event_type": "pull_request"
        }
        numbers = [int(nr) for nr in self.builds.distinct("pull_request_number", query)]
        numbers.sort()
        return numbers

    def get_pull_requests(self, number=None):
        query = {
            "repository_id": self.get_id(),
            "event_type": "pull_request"
        }

        if number:
            query['pull_request_number'] = number

        return self.builds.find(query)

    def get_push_builds(self):
        return self.builds.find({
            "repository_id": self.get_id(),
            "event_type": "push"
        })

    def create_commit_index(self, git=None):
        builds = {}

        for build in self.builds.find({"repository_id": self.get_id()}):
            """ what to do with builds that were restarted? """
            builds[build['commit']['sha']] = build

        if git is not None:
            builds.update(self.reconstruct_pull_request_commits(git))

        return builds

    def reconstruct_pull_request_commits(self, git):
        prs = self.get_pull_requests()
        log = git.log

        index = {}

        for build in prs:
            c = build['commit']
            cdate = as_utc_date(c['committed_at'])
            cmsg = c['message'].splitlines()[0]

            candidates = [c for c in log if c[2] == cmsg and c[1] == cdate]

            if len(candidates) == 0:
                """ was removed either by renaming or rebasing (timestamp changes) """
                continue

            c = candidates[0]
            index[c[0]] = build

        return index

    def get_build_frequency(self):
        builds = self.get_builds().sort([("id", pymongo.ASCENDING)])
        total = builds.count()
        first = builds[0]
        last = self.get_builds().sort([("id", pymongo.DESCENDING)])[0]

        a = as_utc_date(last['started_at'])
        b = as_utc_date(first['started_at'])
        days = (a - b).days

        return total / days

    def get_event_stats(self):
        aggr = self.builds.aggregate([
            {"$match": {
                "repository_id": self.get_id()
            }},
            {"$group": {
                "_id": {"event_type": "$event_type"},
                "count": {"$sum": 1}
            }}
        ], useCursor=False)

        return self.aggr_to_map(aggr, lambda o: o['_id']['event_type'], lambda o: o['count'])

    def get_state_stats(self):
        aggr = self.builds.aggregate([
            {"$match": {
                "repository_id": self.get_id()
            }},
            {"$group": {
                "_id": {"state": "$state"},
                "count": {"$sum": 1}
            }}
        ])

        stats = self.aggr_to_map(aggr, lambda o: o['_id']['state'], lambda o: o['count'])

        stats['broken'] = stats['failed'] + stats['errored']
        stats['most'] = stats['passed'] + stats['failed'] + stats['errored']

        return stats

    @staticmethod
    def aggr_to_map(aggr, kfn, vfn):
        result = aggr if isinstance(aggr, pymongo.command_cursor.CommandCursor) else aggr['result']

        m = {}
        for obj in result:
            m[kfn(obj)] = vfn(obj)
        return m


class PullRequest:
    def __init__(self, number, repo: BuildRepo):
        self.number = number
        self.repo = repo

    @cached
    def builds(self):
        builds = [self.repo.build_index[build['id']] for build in self.repo.pull_request_index[self.number]]
        builds.sort(key=lambda b: int(b.number))
        return builds

    @property
    def num_builds(self):
        return len(self.builds)

    @property
    def duration(self):
        if self.num_builds <= 1:
            return 0

        start, end = self.builds[0].started_at, self.builds[-1].started_at

        return (end - start).days if start and end else 0

    @property
    def failure_ratio(self):
        passed = 0
        broken = 0

        for build in self.builds:
            if build.is_broken:
                broken += 1
            elif build.is_passed:
                passed += 1

        return broken / (passed + broken) if passed + broken > 0 else (1 if broken > 0 else 0)

    def __str__(self, *args, **kwargs):
        return "PullRequest[#{} ({})]".format(self.number, ",".join([str(build.id) for build in self.builds]))


class DateFilteredPullRequest(PullRequest):
    def __init__(self, number, repo: BuildRepo, date_threshold=utc_now()):
        super().__init__(number, repo)

        if isinstance(date_threshold, str):
            date_threshold = as_utc_date(date_threshold)

        self.date_threshold = date_threshold

    @cached
    def builds(self):
        return [build for build in super().builds if build.started_at <= self.date_threshold]


def find_repo(id_or_slug):
    """
    Finds a repo in the mongodb with the given slug or id
    :param id_or_slug:
    :return: BuildRepo
    """
    if isinstance(id_or_slug, int):
        q = {"id": id_or_slug}
    else:
        q = {"slug": str(id_or_slug)}

    for r in mongo.repos.find(q):
        return BuildRepo(id_or_slug, mongo, r)

    raise ValueError("No repo found with query:", q)


def get_all_repos():
    for r in mongo.repos.find():
        yield BuildRepo(r['slug'], mongo, r)
