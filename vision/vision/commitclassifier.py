from collections import Counter
# these are from Hattori and Lanza 2008
from vision.common import tsum, ddmap
from vision.filetypeclassifier import FileTypeClassifier

mm = ["clean", "license", "merge", "release", "structure", "integrat", "copyright", "documentation", "manual",
      "javadoc", "comment", "migrat", "repository", "code review", "polish", "upgrade", "style", "formatting",
      "organiz", "TODO"]
re = ["optimiz", "adjust", "update", "delet", "remov", "chang", "refactor", "replac", "modif", "is now", "are now",
      "enhance", "improv", "design change", "renam", "eliminat", "duplicat", "restrutur", "simplif", "obsolete",
      "rearrang", "miss", "enhanc", "improv"]
ce = ["bug", "fix", "issue", "error", "correct", "proper", "deprecat", "broke"]
fe = ["implement", "add", "request", "new", "test", "start", "includ", "initial", "introduc", "creat", "increas"]

# these were added by checking unknown commits from spring-boot
mm += ["document", "bump", "downgrade", "clarify", "readme"]
re += ["extract", "mov", "rework"]
ce += ["revert"]
fe += ["support", "allow", "develop"]


def transpose(arr, val):
    m = {}
    for key in arr:
        m[key] = val
    return m


class Activity:
    UNKNOWN = 0
    MM = 1  # Management
    RE = 2  # Reengineering
    CE = 3  # Corrective Engineering
    FE = 4  # Forward Engineering

    keywords = mm + re + ce + fe

    index = {}
    index.update(transpose(mm, MM))
    index.update(transpose(re, RE))
    index.update(transpose(ce, CE))
    index.update(transpose(fe, FE))

    @staticmethod
    def to_string(activity):
        if activity is 1:
            return "MM"
        elif activity is 2:
            return "RE"
        elif activity is 3:
            return "CE"
        elif activity is 4:
            return "FE"
        else:
            return "UNKNOWN"


def classify(message):
    haystack = message.lower()

    for keyword in Activity.keywords:
        if keyword in haystack:
            return Activity.index[keyword]

    return Activity.UNKNOWN


class CommitAnalyzer:
    def __init__(self, commit, file_type_classifier=None):
        self.commit = commit
        if file_type_classifier:
            self.file_type_analyzer = file_type_classifier
        else:
            self.file_type_analyzer = FileTypeClassifier()

    def is_merge(self):
        return len(self.commit.git.tree[self.commit.sha]) > 1

    def get_stereotype(self):
        if self.is_merge():
            return "merge"

        file_types = self.get_file_types()
        cnt = Counter(file_types.values())

        if not file_types:
            return "empty"  # empty commit

        keys = tuple(sorted(cnt.keys()))

        if len(keys) == 1:
            return keys[0]

        if len(keys) == 2:
            return "+".join(keys)

        if keys == ('system', 'test'):
            return "system+test"

        return "tangled+%s" % cnt.most_common(1)[0][0]

    def get_file_types(self):
        return {file: self.file_type_analyzer.classify(file) for file in self.commit.file_stats}

    def get_file_type_changes(self):
        """
        Returns a dict that, for each file type, holds a set of change types
        executed on the file, regardless of the change size and impact.
        :return: a dict
        """

        return {ct: list(d.keys()) for ct, d in self.aggregate_file_type_changes().items()}

    def aggregate_file_type_changes(self):
        """
        Returns a two-dimensional dict[filetype][changetype] = (a,d)
        :return:
        """
        file_types = self.get_file_types()
        changes = self.aggregate_file_changes()

        aggr = dict()

        for file, change in changes.items():
            filetype = file_types[file] if file in file_types else "unknown"

            if filetype not in aggr:
                aggr[filetype] = dict()

            for changetype, ad in change.items():
                if changetype not in aggr[filetype]:
                    aggr[filetype][changetype] = list()

                aggr[filetype][changetype].append(ad)

        return ddmap(aggr, tsum)

    def aggregate_file_changes(self):
        """
        Returns a two-dimensional dict[file][change_type] = (a,d)
        :return:
        """
        if self.commit.sha not in self.commit.git.patch_index:
            return dict()

        patch = self.commit.git.patch_index[self.commit.sha]

        aggr = dict()

        for file, diffs in patch.items():
            aggr[file] = {t: list() for (_, _, t) in diffs}

            for (_, ad, t) in diffs:
                aggr[file][t].append((1, 0) if ad < 0 else (0, 1))

        return ddmap(aggr, tsum)


class ChangeAnalyzer:
    change_types = ["newline", "comment", "content", "code", "import"]

    @staticmethod
    def get_change_type(line: str, file: str):
        # TODO: block comments are be important (e.g. license headers)

        if not line:
            return "newline"

        line = line.strip()

        if not line:
            return "newline"

        if file.endswith(".java"):
            if line.startswith("import "):
                return "import"
            elif line.startswith("//") or line.startswith("/*") or line.startswith("*"):
                return "comment"
            else:
                return "code"

        elif file.endswith(".gradle"):
            if line.startswith("//") or line.startswith("/*"):
                return "comment"

        elif file.endswith(".xml"):
            if line.startswith("<!--") or line.endswith("-->"):
                return "comment"

        elif file.endswith(".properties"):
            if line.startswith(";") or line.startswith("#"):
                return "comment"

        return "content"
