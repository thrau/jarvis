from click import BadParameter

from vision.builds import find_repo, BuildRepo
from vision.cachedump import load_git
from vision.git import Git, git_path


def require_resources(slug, cache=True) -> (Git, BuildRepo):
    """
    Requres that both the git repository and the build repository exists for the given slug. If so, it returns a tuple
    of the respective resources. If not, it throws a click `BadParameter` exception.

    :param slug: the repository slug
    :return: (Git, BuildRepo)
    """
    try:
        builds = find_repo(slug)
    except ValueError:
        raise BadParameter("No repository data for %s" % slug, param_hint="slug")

    git = load_git(git_path(slug), cache)
    if not git.exists():
        raise BadParameter("Path %s is not a directory" % git.path, param_hint="slug")

    return git, builds
