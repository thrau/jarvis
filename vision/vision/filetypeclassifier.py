import fnmatch
import os
import types

from vision.builds import BuildRepo

_base_model = [
    ('system', ['src/main/java/**/*.java', '**/src/main/java/**/*.java']),
    ('test', ['src/test/java/**/*.java', '**/src/test/java/**/*.java']),
    ('benchmark', '**/src/jmh/java/*.java'),
    ('test_resources', ['src/test/resources/*', '**/src/test/resources/*']),
    ('system_resources', ['src/main/resources/*', '**/src/main/resources/*']),
    ('build_config',
     ['*pom.xml', 'mvnw', 'mvnw.cmd', 'mvnw.bat', '*.gradle', 'gradlew', 'gradlew.bat', '*checkstyle.xml']),
    ('git', ['.gitmodules', '.gitattributes', '.gitignore', '**/.gitignore', '.mailmap']),
    ('ci_config', '.travis.yml'),
    ('properties', '*.properties'),
    ('documentation',
     ['*.md', '*.markdown', '*.txt', '*.rst', '*.adoc', 'NOTICE', '**/NOTICE', 'VERSION', 'README', 'AUTHORS', 'LICENSE',
      '*LICENSE', 'DEVELOPERS'])
]


class FileTypeClassifier:
    def __init__(self, model: list = None):
        if model:
            self.model = model
        else:
            self.model = _base_model

    def add_loader(self, loader):
        if loader.model_exists():
            self.extend_model(loader.load())

    def extend_model(self, model):
        self.model = self.__merge_models(self.model, model)

    def classify(self, instance):
        for key, items in self.model:
            if self.matches(instance, items):
                return key

        return "unknown"

    def get_type_names(self):
        return [k for k, _ in self.model] + ["unknown"]

    @staticmethod
    def matches(instance, fn):
        if isinstance(fn, str):
            return fnmatch.fnmatch(instance, fn)
        if isinstance(fn, types.FunctionType):
            return fn(instance)
        if isinstance(fn, list):
            return any([FileTypeClassifier.matches(instance, fnp) for fnp in fn])

        raise ValueError("Don't know how to use %s to match" % type(fn))

    @staticmethod
    def __update_model(into: dict, obj: dict):
        for key in obj.keys():
            a = into.get(key)
            b = obj.get(key)

            if a is None:
                into[key] = b
            elif isinstance(a, list):
                if isinstance(b, list):
                    into[key] = a + b
                else:
                    into[key].append(b)
            else:
                into[key] = list()
                into[key].append(a)
                if isinstance(b, list):
                    into[key].extend(b)
                else:
                    into[key].append(b)

    @staticmethod
    def __merge_models(target: list, source: list):
        into = dict(target)
        obj = dict(source)

        FileTypeClassifier.__update_model(into, obj)

        keys = []
        for k, _ in target + source:
            # sorted set...
            if k not in keys:
                keys.append(k)

        return [(k, into[k]) for k in keys]


class FileTypeModelLoader:
    def __init__(self, repo: BuildRepo):
        self.repo = repo

    def get_path(self):
        return "/data/jarvis/filetypes/filetypes.%s.py" % self.repo.get_id()

    def model_exists(self):
        return os.path.isfile(self.get_path())

    def load(self):
        with open(self.get_path()) as f:
            return eval(f.read())
