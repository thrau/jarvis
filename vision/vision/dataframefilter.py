class AbstractFilter:
    def get_filtered(self, df):
        return df

    def __str__(self):
        return type(self).__name__


class CompoundFilter(AbstractFilter):
    filters = []

    def __init__(self, filters: list):
        for f in filters:
            self.add(f)

    def add(self, f: AbstractFilter):
        self.filters.append(f)

    def get_filtered(self, df):
        for f in self.filters:
            df = f.get_filtered(df)

        return df

    def __str__(self):
        return ",".join([str(f) for f in self.filters])


class RemoveMergeCommits(AbstractFilter):
    def get_filtered(self, df):
        df = df[df['is_merge'].isin([False])]
        del df['is_merge']
        return df


class RemoveResultClasses(AbstractFilter):
    excluded_results = []

    def __init__(self, excluded_results: list):
        self.excluded_results = excluded_results

    def get_filtered(self, df):
        allowed_results = set(df['result'])
        allowed_results = allowed_results.difference(set(self.excluded_results))

        return df[df['result'].isin(allowed_results)]

    def __str__(self):
        return "%s%s" % (super().__str__(), self.excluded_results)


class RemoveUncategorized(RemoveResultClasses):
    def __init__(self):
        super().__init__(["errored", "failed"])


class OriginalErrors(AbstractFilter):
    def get_filtered(self, df):
        df = df[df['state'].isin(["errored", "failed"])]
        df = df[df['is_original'].isin([True])]

        return df
