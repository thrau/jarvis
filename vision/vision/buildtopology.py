from vision.builds import BuildRepo, PullRequest
from vision.common import cached, as_utc_date, transpose
from vision.git import Git, Commit, CommitRange


class Build:
    def __init__(self, topology, sha: str):
        self.topology = topology
        self.sha = sha

    @property
    def build(self):
        return self.topology.index[self.sha]

    @property
    def number(self):
        return int(self.build['number'])

    @cached
    def date(self):
        return as_utc_date(self.build['commit']['committed_at'])

    @cached
    def commit(self) -> Commit:
        return self.topology.git.get_commit(self.sha)

    def is_passed(self):
        return self.state == "passed"

    def is_broken(self):
        return self.state in ["errored", "failed"]

    @property
    def state(self):
        return self.build['state']

    @property
    def result(self):
        build = self.build

        if 'category' in build:
            return build['category']
        else:
            return build['state']

    def get_result(self, originals_only=False):
        if originals_only and self.sha not in self.topology.original_errors:
            return 'passed'

        return self.result

    @property
    def range(self):
        return self.topology.build_range_index[self.sha]

    @property
    def previous_builds(self):
        hist = self.topology.build_history_index
        if self.sha not in hist:
            return []

        return [Build(self.topology, sha) for sha in hist[self.sha]]


class PullRequestBuild:
    def __init__(self, build: Build, pr: PullRequest, git: Git):
        self.build = build
        self.pr = pr
        self.git = git

    @property
    def sha(self):
        return self.build.commit.sha

    @property
    def is_in_tree(self):
        return self.sha in self.git.tree

    @cached
    def sha_range(self):
        summary = self.git.attribute_index[self.sha]['s']
        source, target = summary.split(" into ")
        return source[6:], target  # remove 'Merge ' prefix from source

    @cached
    def commit_range(self):
        return CommitRange(self.git, *self.sha_range[::-1]) if self.is_in_tree else None

    @cached
    def merge_base(self):
        return self.git.merge_base(*self.sha_range)

    def __str__(self):
        r = self.commit_range if self.is_in_tree else "not in tree"
        return "pr#{} build#{} ({}) {}".format(self.pr.number, self.build.number, r, self.build.state)


class BuildTopologyAnalyzer:
    def __init__(self, git: Git, builds: BuildRepo):
        self.git = git
        self.builds = builds

        self.tree = self.git.tree
        # self.index = self.builds.create_commit_index(self.git)

    @cached
    def build_list(self):
        return [Build(self, sha) for sha in self.index]

    @cached
    def build_commit_index(self):
        """
        Analyses the 'jarvis-*' tags to get the info which commits have been built, and returns a dict of
        build-id -> sha.

        :return:
        """
        return {int(k[7:]): v for (k, v) in self.git.tags.items() if k.startswith("jarvis-")}

    @cached
    def commit_build_index(self):
        """
        Transposed version of build_commit_index.

        :return:
        """
        return transpose(self.build_commit_index)

    def get_pr_builds(self, pr: PullRequest):
        return [PullRequestBuild(build, pr, self.git) for build in pr.builds]

    def find_prev_built_commits(self, sha, check_root=False):
        """
        Traverses the tree from the given commit and finds, for each branch, commits that have already been included in
        a build.
        :param sha:
        :param check_root:
        :return:
        """
        marked = set()

        def rec(sha, take=True):
            marked.add(sha)
            ret = set()

            if not sha:
                return ret

            if sha in self.commit_build_index and take:
                ret.add(sha)
            else:
                for ancestor in self.git.tree[sha]:
                    if ancestor not in marked:
                        ret.update(rec(ancestor))

            return ret

        return rec(sha, check_root)

    def find_unique_commit(self, build):
        c = build.commit
        cdate = c.date
        cmsg = c.summary

        candidates = [c for c in self.git.log if c[2] == cmsg]

        if len(candidates) == 1:
            return Commit(self.git, candidates[0][0])

        for candidate in candidates:
            if candidate[1] == cdate:
                return Commit(self.git, candidate[0])

        return None

    def get_topology_sorted_builds(self):
        return [self.build_index[sha]
                for sha in self.git.topology
                if sha in self.build_index]

    def get_previous(self, build):
        """
        Returns the build preceding the given.

        :param build: Build
        :return: Build
        """
        # FIXME: does not consider tree topology
        if build.sha not in self.build_history_index:
            return None

        pshas = self.build_history_index[build.sha]

        if not pshas:
            return None

        return self.build_index[pshas[0]]

    @cached
    def build_index(self):
        # FIXME commits can have multiple builds!
        return dict([(build.sha, build) for build in self.build_list])

    @cached
    def build_history_index(self):
        """
        Returns a (cached) index that maps sha -> [sha] where the value is a list of shas that denote builds that were
        triggered immediately prior (in the commit topology) to the build of the value. It returns a list because merge
        commits can have two predecessor builds.

        :return: dict
        """
        return self.__create_build_history_index()

    @cached
    def build_range_index(self):
        return self.__create_build_range_index()

    @cached
    def original_errors(self):
        """
        Returns the original errors, that is, errors that occurred when the previous builds had a different state.

        :return: a list of commit shas that caused original build errors
        """
        index = self.index

        original = set()

        for sha, shas in self.build_history_index.items():
            state = index[sha]['state']

            if state in ['failed', 'errored']:
                if any([index[s]['state'] != state for s in shas]):
                    original.add(sha)

        return original

    @cached
    def original_error_ratio(self):
        original = self.original_errors
        index = self.index

        errors = len([sha for sha, build in index.items() if build['state'] in ['failed', 'errored']])
        return len(original) / errors

    def __create_build_range_index(self):
        """
        Returns an index that maps sha -> [sha], where the keys are commits that triggered builds, and the value is a
        list of commits that were included in the build.
        :return: dict
        """
        tree = self.tree
        index = self.index

        def unchecked(root):
            s = list()
            visited = set()

            visited.add(root)
            s.extend(tree[root])

            while len(s) > 0:
                sha = s.pop()

                if sha in visited or sha in index:
                    continue

                visited.add(sha)
                s.extend(tree[sha])

            return visited

        return dict([(sha, unchecked(sha)) for sha in index if sha in tree])

    def __create_build_history_index(self):
        index = self.index
        tree = self.tree

        build_commits = self.__create_build_range_index()
        previous_builds = dict()

        for sha, build in self.git.prune_index(index).items():

            rng = build_commits[sha]
            expand = set()
            previous = []
            for c in rng:
                expand.update(tree[c])
            for c in expand:
                if c in index:
                    previous.append(c)

            previous_builds[sha] = previous

        return previous_builds
