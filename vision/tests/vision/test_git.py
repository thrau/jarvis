import os
import unittest

from vision.git import parse_rename, Git, Commit, CommitRange


class TestGit(unittest.TestCase):
    def setUp(self):
        self.git = Git(".")

    def test_exists(self):
        self.assertTrue(self.git.exists())

    def test_call(self):
        ret = self.git.call("--version")
        self.assertTrue(ret.startswith("git version"))


class TestGitParseRename(unittest.TestCase):
    def test_filename(self):
        a, b = parse_rename("A => B")

        self.assertEqual(a, "A")
        self.assertEqual(b, "B")

    def test_dir_filename(self):
        a, b = parse_rename("path/{a.txt => b.txt}")

        self.assertEqual(a, "path/a.txt")
        self.assertEqual(b, "path/b.txt")

    def test_dir_rename(self):
        a, b = parse_rename("path/{a => b}/file.txt")

        self.assertEqual(a, "path/a/file.txt")
        self.assertEqual(b, "path/b/file.txt")

    def test_dir_remove(self):
        a, b = parse_rename("path/{inner => }/file.txt")

        self.assertEqual(a, "path/inner/file.txt")
        self.assertEqual(b, "path/file.txt")

    def test_dir_add(self):
        a, b = parse_rename("path/{ => inner}/file.txt")

        self.assertEqual(a, "path/file.txt")
        self.assertEqual(b, "path/inner/file.txt")


class TestRepo(unittest.TestCase):
    path = "/data/jarvis/repos"
    slug = "spring-projects/spring-boot"

    @classmethod
    def setUpClass(cls):
        path = os.path.join(cls.path, cls.slug)
        cls.git = Git(path)
        cls.assertTrue(cls.git.exists(), "can't find repository {}".format(path))

    def test_get_remote_slug(self):
        git = Git(os.path.join(self.path, self.slug))
        self.assertEqual(self.slug, git.get_remote_slug())

    def test_commit_exists(self):
        c = Commit(self.git, "bc5a4bcc1e2e3442cd24e799b9b05237944b6ab9")
        self.assertTrue(c.exists)
        self.assertEqual("bc5a4bcc1e2e3442cd24e799b9b05237944b6ab9", c.sha)

        c = Commit(self.git, "f00bar421e2e3442cd24e799b9b05237944b6ab9")
        self.assertFalse(c.exists)

    def test_get_commit_returns_indexed_commit(self):
        c = self.git.get_commit("bc5a4bcc1e2e3442cd24e799b9b05237944b6ab9")
        c1 = self.git.get_commit("bc5a4bcc1e2e3442cd24e799b9b05237944b6ab9")
        self.assertTrue(c is c1)

    def test_get_commit_returns(self):
        c = self.git.get_commit("bc5a4bcc1e2e3442cd24e799b9b05237944b6ab9")
        self.assertTrue(c.exists)

        c = self.git.get_commit("f00bar421e2e3442cd24e799b9b05237944b6ab9")
        self.assertIsNotNone(c)
        self.assertFalse(c.exists)

    def test_commit_is_merge(self):
        c = Commit(self.git, "bc5a4bcc1e2e3442cd24e799b9b05237944b6ab9")
        self.assertFalse(c.is_merge)

        c = Commit(self.git, "3ad334ed814725aa6dcd16dd578bc443a25c082b")
        self.assertTrue(c.is_merge)

    def test_commit_parents_order(self):
        c = Commit(self.git, "156e4e9df6587856718d1f22961a5107af668a9c")
        parents = c.parents

        self.assertEqual(2, len(parents))
        self.assertEqual("fdc2e84", parents[0].sha[:7])
        self.assertEqual("e4cead6", parents[1].sha[:7])

    def test_commit_merge_base(self):
        c = Commit(self.git, "156e4e9df6587856718d1f22961a5107af668a9c")
        mb = c.merge_base

        self.assertEqual("02e989c", mb.sha[:7])
        self.assertIsNone(mb.merge_base)

    def test_commit_range_of(self):
        c = Commit(self.git, "46d855d5972086cec58435ae0ee13cab4cc422e3")

        r0 = CommitRange.of(c.merge_base, c.parents[0])
        r1 = CommitRange.of(c.merge_base, c.parents[1])

        self.assertEqual(['64bbefd', '7afe1d1', '50ca35b'], [sha[:7] for sha in r0.flat])
        self.assertEqual(['1b6ad7c'], [sha[:7] for sha in r1.flat])
