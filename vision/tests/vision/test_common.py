import unittest
from collections import defaultdict

from vision.common import *


class TestCommon(unittest.TestCase):
    def test_nub(self):
        self.assertEqual([1], nub([1]))
        self.assertEqual([1], nub([1, 1, 1]))
        self.assertEqual([1, 2, 3], nub([1, 2, 3]))
        self.assertEqual([2, 1, 3], nub([2, 1, 2, 1, 3, 3]))
        self.assertEqual([], nub([]))

    def test_pseudo_id(self):
        self.assertEqual("d41d8cd", pseudo_id(""))
        self.assertEqual("d41d8cd98f00b204e9800998ecf8427e", pseudo_id("", 32))
        self.assertEqual("acbd18d", pseudo_id("foo"))

    def test_as_utc_date(self):
        date = as_utc_date("2016-05-31 07:49:00Z")
        self.assertEqual(2016, date.year)
        self.assertEqual(5, date.month)
        self.assertEqual(31, date.day)
        self.assertEqual(7, date.hour)
        self.assertEqual(49, date.minute)

    def test_tsum(self):
        self.assertEqual((0, 0), tsum([]))
        self.assertEqual((2, 3), tsum([(1, 2), (1, 1)]))

    def test_join(self):
        m1 = {
            'a': 1,
        }
        m2 = {
            'a': 2,
            'b': 3
        }

        self.assertEqual({'a': (1, 2)}, join(m1, m2))

    def test_transpose(self):
        d1 = {
            'a': 'x',
            'b': 'y',
            'c': 'x'
        }

        t1 = transpose(d1)

        self.assertDictEqual({'x': {'a', 'c'}, 'y': {'b'}}, t1)
