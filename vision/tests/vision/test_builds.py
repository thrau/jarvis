import unittest

from vision.builds import BuildRepo, PullRequest


class TestBuildRepo(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.repo = BuildRepo("thrau/jarchivelib")

    def test_build_index(self):
        index = self.repo.build_index

        self.assertIsNotNone(index)
        self.assertIn(18707665, index.keys())
        self.assertNotIn(1, index.keys())

        obj = index[18707665]
        self.assertEqual(1, obj.number)

    def test_get_id(self):
        self.assertEqual(1889385, self.repo.get_id())

    def test_get_git_path(self):
        self.assertEqual("/data/jarvis/repos/thrau/jarchivelib", self.repo.get_git_path())

    def test_pull_request(self):
        pr = PullRequest(31, self.repo)

        self.assertEqual(3, len(pr.builds))
        self.assertEqual(40, pr.builds[0].number)
        self.assertEqual(41, pr.builds[1].number)
        self.assertEqual(42, pr.builds[2].number)
