import unittest

from vision.builds import BuildRepo
from vision.buildtopology2 import BuildTopologyAnalyzer, TopoBuild, PullRequestBuild
from vision.git import Git


class TopoTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.repo = BuildRepo("spring-projects/spring-boot")
        cls.assertTrue(cls.repo.has_git_path(), "Can't test this: no git repo %s" % cls.repo.slug)

        cls.git = Git(cls.repo.get_git_path())
        cls.topo = BuildTopologyAnalyzer(cls.git, cls.repo)


class TestBuildTopology(TopoTestCase):
    def test_existing(self):
        topo_build = TopoBuild(self.topo, 144032644)

        self.assertEqual("jarvis-144032644", topo_build.tag)
        self.assertEqual(144032644, topo_build.id)
        self.assertTrue(topo_build.is_in_tree)
        self.assertTrue(topo_build.is_tagged)
        self.assertTrue(topo_build.is_tagged)
        self.assertEqual("43afc149a14219c13f6830b6e503ac2c9c218f44", topo_build.sha)

    def test_git_commit(self):
        topo_build = TopoBuild(self.topo, 144032644)

        commit = topo_build.git_commit
        self.assertIsNotNone(commit)
        self.assertEqual("43afc149a14219c13f6830b6e503ac2c9c218f44", commit.sha)

    def test_pr(self):
        topo_build = PullRequestBuild(self.topo, 141258697)

        notin, isin = topo_build.commit_range.range

        self.assertEqual("d084803", notin[:7])
        self.assertEqual("33c1839", isin[:7])
        self.assertEqual("d084803", topo_build.merge_base[:7])

    def test_topo_build_index(self):
        self.assertIsInstance(self.topo.topo_build_index[144032644], TopoBuild)
        self.assertNotIsInstance(self.topo.topo_build_index[144032644], PullRequestBuild)
        self.assertIsInstance(self.topo.topo_build_index[141258697], PullRequestBuild)

    def test_commit_build_index(self):
        self.assertEqual(2, len(self.topo.commit_builds_index["135ebb8ded98a679e94ef90fb0db79df525e2276"]))
        self.assertEqual(1, len(self.topo.commit_builds_index["43afc149a14219c13f6830b6e503ac2c9c218f44"]))

    def test_commit_build_index_shadowed(self):
        # this is the pr merge commit
        b0 = self.topo.commit_builds_index_shadowed["45ee07d2fcfb4492e0c65b9ea2fe807aa72d7f77"][0]
        # this is head of the branch being merged
        b1 = self.topo.commit_builds_index_shadowed["52aea6814d39b393094f42b4394df8d6306d407c"][0]

        self.assertEqual(b0, b1)

    def test_find_prev_built_commits_default_pr(self):
        prev = self.topo.find_prev_built_commits("549144eb6b7ac271d4b4534ace9642d0e6e2d13f")
        self.assertEqual(1, len(prev))
        self.assertIn("25ba9a56e64cb3d0be7678d1f69eab628a579a25", prev)

    def test_find_prev_build_commits_pr_twoway(self):
        prev = self.topo.find_prev_built_commits("637d4f443375dae2a5425c56640983bb3f25332d")
        self.assertEqual(2, len(prev))
        self.assertIn("ccaa19d51f060938980a03501590f9efeb76b6d9", prev)
        self.assertIn("ce3f4bd068ea2ea7752b9e0b6ee530d13f7daffc", prev)

    def test_find_prev_build_commits_order(self):
        prev = self.topo.find_prev_built_commits("637d4f443375dae2a5425c56640983bb3f25332d")
        self.assertEqual("ccaa19d", prev[0][:7])
        self.assertEqual("ce3f4bd", prev[1][:7])

    def test_find_prev_build_commits_pr_updated(self):
        prev = self.topo.find_prev_built_commits("9850fac1718ceb776b4f51d59cb4ed5c8179036e")

        # TODO: alternatively, this should maybe include 443e9f6, because it was built by bff2c8d?
        self.assertEqual(1, len(prev))
        self.assertIn("328676007374e579470ade81e1d2ba007c8b4b97", prev)

    def test_topo_build_prev_builds_default_pr(self):
        topo_build = PullRequestBuild(self.topo, 141258697)

        builds = topo_build.prev_builds
        self.assertEqual(1, len(builds))
        build = builds[0]

        self.assertEqual(141244307, build.id)
        self.assertEqual("d084803", build.sha[:7])

    def test_topo_build_prev_builds_pr_twoway(self):
        topo_build = PullRequestBuild(self.topo, 143025683)

        builds = topo_build.prev_builds
        self.assertEqual(2, len(builds))

        b_cca = [build for build in builds if build.sha[:3] == "cca"][0]
        b_ce3 = [build for build in builds if build.sha[:3] == "ce3"][0]

        self.assertEqual(143000699, b_cca.id)
        self.assertEqual(142182933, b_ce3.id)

    def test_prev_pr_build(self):
        t1 = PullRequestBuild(self.topo, 134441289)
        t0 = t1.prev_pr_build
        self.assertEqual(134381342, t0.id)
        self.assertIsNone(t0.prev_pr_build)

    def test_pr_build_prev_built_commits(self):
        prb = PullRequestBuild(self.topo, 133951303)
        commits = prb.prev_built_commits

        # it contains the sha of the last commit in the pr that was built by a pr update
        self.assertEqual(['b49c6ca61610edb2e18777ac5f6c3284b390c890', '06a8c93b6fa68703b8a054c32303aa5b633aacec'],
                         commits)

    def test_pr_build_prev_built_commits_rebase(self):
        prb = PullRequestBuild(self.topo, 133782130)
        commits = prb.prev_built_commits

        # contains only the merge base
        self.assertEqual(["4a73eb6045ff346b5eafecda07618b87b8eaacf3"],
                         commits)

    def test_pr_build_prev_builds_rebase(self):
        prb = PullRequestBuild(self.topo, 133782130)
        builds = prb.prev_builds

        # this includes the build of the merge base *and* the previous pr commits (in that order)
        self.assertEqual(2, len(builds))
        self.assertEqual(133432438, builds[0].id)
        self.assertEqual(133483354, builds[1].id)

    def test_prev_built_commits_decoupled_pr(self):
        prb = PullRequestBuild(self.topo, 142836299)

        commits = prb.prev_built_commits
        self.assertEqual("3286760", commits[0][:7])
        self.assertEqual("443e9f6", commits[1][:7])  # not the actual build!

    def test_dev_prev_builds_decoupled_pr(self):
        prb = PullRequestBuild(self.topo, 142836299)

        builds = prb.prev_builds
        self.assertEqual(142797852, builds[0].id)
        self.assertEqual(142834474, builds[1].id)

    def test_last_broken(self):
        build = self.topo.topo_build_index[142836299]
        self.assertEqual(142750077, build.last_broken.id)

        build = self.topo.topo_build_index[141365841]
        self.assertEqual(141319179, build.last_broken.id)

        build = self.topo.topo_build_index[140494960]
        self.assertEqual(139702057, build.last_broken.id)
