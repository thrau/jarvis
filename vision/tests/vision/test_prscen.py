import unittest

from vision.builds import BuildRepo
from vision.buildtopology2 import BuildTopologyAnalyzer
from vision.git import Git
from vision.prscen import Scenario, determine_scenario, determine_scenarios


class TestPrScen(unittest.TestCase):
    def test_constr(self):
        self.assertEqual(Scenario.NONE, Scenario([]))
        self.assertEqual(Scenario.SIMPLE_1, Scenario([True]))
        self.assertEqual(Scenario.SIMPLE_2, Scenario([False]))
        self.assertEqual(Scenario.A, Scenario([True, True, True, True]))
        self.assertEqual(Scenario.B, Scenario([True, True, True, False]))

    def test_is_implausible(self):
        self.assertFalse(Scenario.A.is_implausible)
        self.assertTrue(Scenario.E.is_implausible)
        self.assertTrue(Scenario.F.is_implausible)
        self.assertTrue(Scenario.G.is_implausible)
        self.assertTrue(Scenario.M.is_implausible)
        self.assertTrue(Scenario.O.is_implausible)


class TestPrScenRepo(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.repo = BuildRepo("spring-projects/spring-boot")
        cls.assertTrue(cls.repo.has_git_path(), "Can't test this: no git repo %s" % cls.repo.slug)

        cls.git = Git(cls.repo.get_git_path())
        cls.topo = BuildTopologyAnalyzer(cls.git, cls.repo)

    def test_determine_scenario_simple2(self):
        pr = self.repo.pull_requests[6234]

        build = self.topo.get_pr_builds(pr)[0]
        scenario = determine_scenario(build)
        self.assertEqual(Scenario.SIMPLE_2, scenario)

    def test_determine_scenarios_simple2(self):
        pr = self.repo.pull_requests[6234]

        scenarios = determine_scenarios(pr, self.topo)
        self.assertEqual(1, len(scenarios))

        scenario = scenarios[0]
        self.assertEqual(Scenario.SIMPLE_2, scenario)

    def test_determine_scenarios_complex(self):
        pr = self.repo.pull_requests[6154]

        scenarios = determine_scenarios(pr, self.topo)

        self.assertEqual(5, len(scenarios))
        self.assertEqual(Scenario.SIMPLE_1, scenarios[0])
        self.assertEqual(Scenario.D, scenarios[1])
        self.assertEqual(Scenario.D, scenarios[2])
        self.assertEqual(Scenario.A, scenarios[3])
        self.assertEqual(Scenario.D, scenarios[4])

    def test_determine_scenario_complex(self):
        pr = self.repo.pull_requests[6154]

        builds = self.topo.get_pr_builds(pr)

        self.assertEqual(5, len(builds))
        self.assertEqual(Scenario.SIMPLE_1, determine_scenario(builds[0]))
        self.assertEqual(Scenario.D, determine_scenario(builds[1]))
        self.assertEqual(Scenario.D, determine_scenario(builds[2]))
        self.assertEqual(Scenario.A, determine_scenario(builds[3]))
        self.assertEqual(Scenario.D, determine_scenario(builds[4]))
