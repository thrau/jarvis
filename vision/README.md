VISION
======

Vision is the data analysis and extraction module of jarvis.
Python offers flexibility in data processing and access to both mongo and git via shell bindings.

Build
-----

To install vision as a python package and register the executable scripts for
the current user, run

    python3 setup.py install --user --record .install-files.txt

the setup script will register everything into `~/.local/lib` and
`~/.local/bin`.

If you have `~./local/bin` in your path, you can directly execute the various
commands. Otherwise navigate to `~./local/bin` and execute them there.


### Uninstall

To remove the installation, run

    cat .install-files.txt | xargs rm -rf

Commands
--------

* `vision-extract-features`

* `vision-git-log`

* `vision-logcat-import`

  Import one or more json files generated via `logcat` and enrich the MongoDB
  job instances with the categories found in the model.

* `vision-marvin`

* TODO: ...

Modules
-------

* `scripts`

  Executable scripts that utilize vision to do 'useful' things.

* `tests`

  Unit tests

* `vision`

  Represents the core package and contains the framework code used for the
  scripts
