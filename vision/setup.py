#!/usr/bin/env python3

from setuptools import setup, find_packages

config = {
    'description': 'jarvis - vision',
    'author': 'Thomas Rausch',
    'url': 'https://bitbucket.org/thrau/jarvis',
    'download_url': 'https://bitbucket.org/thrau/jarvis',
    'author_email': 'thomas@rauschig.org',
    'version': '0.2.0-SNAPSHOT',
    'requires': [
        'pymongo',
        'sh',
        'whatthepatch',
        'pickle',
        'dateutil',
        'requests',
        'click'
    ],
    'packages': find_packages(),
    'scripts': [
        'scripts/vision-extract-features',
        'scripts/vision-git-log',
        'scripts/vision-logcat-import',
        'scripts/vision-marvin',
        'scripts/vision-job-stats',
        'scripts/vision-plot-pr-data',
        'scripts/vision-prepare-indices',
        'scripts/vision-prscenario-stats',
        'scripts/vision-repo-stats',
    ],
    'test_suite': 'tests',
    'name': 'vision',
}

setup(**config)
