#!/usr/bin/python3

from pymongo import MongoClient
from datetime import datetime
import calendar

client = MongoClient("mongodb://localhost:27017")
db = client.jarvis

repos = db.repositoryjsonobject
builds = db.buildjsonobject

def to_epoch(s):
    date = datetime.strptime(s, '%Y-%m-%dT%H:%M:%S%fZ')
    return calendar.timegm(date.timetuple())


def print_build_stats():
    header = "repo_id;slug;number;started_at;state;category;event_type;duration"
    print(header)

    for repo in repos.find():
        for build in builds.find({"repository_id": repo['id']}):

            if str(build['duration']) == "None":
                continue
            if not build['started_at']:
                continue

            category = ""
            if not 'category' in build:
                category = build['state']
            else:
                category = build['category']

            row = ';'.join([str(x) for x in [
                repo['id'],
                repo['slug'],
                build['number'],
                to_epoch(build['started_at']),
                build['state'],
                category,
                build['event_type'],
                build['duration']
            ]])

            print(row)

def print_commit_stats():

    print("repository_id;slug;id;number;state;event_type;sha;branch;author;compare")

    aggr = builds.aggregate([
        {"$lookup": {
            "from": "repositoryjsonobject",
            "localField": "repository_id",
            "foreignField": "id",
            "as": "repository"
        }},
        {"$project": {
            "_id": 0,
            "repository_id": 1,
            "slugs": "$repository.slug",
            "id":  1,
            "number": 1,
            "state": 1,
            "event_type": 1,
            "sha": "$commit.sha",
            "branch": "$commit.branch",
            "author": "$commit.author_email",
            "compare": "$commit.compare_url"
        }},
        {"$sort": {
            "repository_id": 1,
            "id": 1
        }}
    ])

    for build in aggr['result']:
        row = ';'.join([str(x) for x in [
            build["repository_id"],
            build["slugs"][0],
            build["id"],
            build["number"],
            build["state"],
            build["event_type"],
            build["sha"],
            build["branch"],
            build["author"],
            build["compare"]
        ]])

        print(row)


print_build_stats()
