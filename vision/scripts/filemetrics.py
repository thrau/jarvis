from vision.git import Git


class File:
    git = None
    path = None

    def __init__(self, git, path):
        self.git = git
        self.path = path

    def numstat(self, sha=None):
        if sha:
            return self.git.read("log", "--format=%H", "--follow", "-w", "--numstat", sha, "--", self.path)
        else:
            return self.git.read("log", "--format=%H", "--follow", "-w", "--numstat", "--", self.path)

    def commit_stats(self, sha=None):
        entries = self.numstat(sha)
        i = 0
        commits = {}
        while i < len(entries):
            sha = entries[i]
            (a, d, _) = entries[i + 2].split("\t")
            i += 3

            if a == '-' or d == '-':
                a, d = 0, 0

            commits[sha] = (int(a), int(d))

        return commits


slug = "Netflix/Hystrix"
repo_path = "/data/jarvis/repos/%s" % slug

git = Git(repo_path)

for file in [File(git, file) for file in git.read("ls-files")]:
    print(file.path, file.commit_stats())
