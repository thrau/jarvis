import re
import time
from collections import defaultdict

from vision.builds import BuildRepo, BuildRepoCommit
from vision.common import cached, as_utc_date
from vision.git import Git


class RepoAnalyzer:
    def __init__(self, slug):
        self.slug = slug
        self.repo_path = "/data/jarvis/repos/%s" % slug

        self.builds = BuildRepo(slug)
        self.git = Git(self.repo_path)
        # self.topology = BuildTopologyAnalyzer(self.git, self.builds)

    @cached
    def commit_build_index(self):
        index = defaultdict(list)

        message_index = defaultdict(list)
        for (sha, date, msg) in git.log:
            message_index[msg].append((sha, date, msg))

        def link_existing_push(build):
            if build.is_push:
                sha = build.commit.sha
                if sha in self.git.tree.keys():
                    return sha

            return None

        def link_equal_pull(build):
            if not build.is_pull_request:
                return None

            msg = build.commit.summary
            date = build.commit.date

            for (csha, cdate, cmsg) in message_index[msg]:
                if date == cdate:
                    index[csha].append(build)
                    return csha

            return None

        def link_fuzzy(build):
            # print("missing", build.pretty(), build.event_type)
            #
            # print("  ", build.commit.author_email)
            # print("  ", build.commit.date)
            # print("  ", build.commit.summary)

            similar = [(csha, cdate, cmsg) for (csha, cdate, cmsg) in message_index[build.commit.summary]
                       if git.attribute_index[csha]['ae'] == build.commit.author_email]

            if not similar:
                return None
            elif len(similar) == 1:
                return similar[0][0]
            else:
                similar = list([c for c in similar if c[0] in git.reachable])
                if len(similar) == 1:
                    """ this most likely means that the branch was rebased and then merged """
                    return similar[0][0]

            return None

        for bid, build in self.builds.build_index.items():
            for fn in [
                link_existing_push,
                link_equal_pull,
                link_fuzzy,
            ]:
                sha = fn(build)
                if sha:
                    index[sha].append(build)
                    break

            if not sha:
                print("Could not find commit for", build.pretty(), build.commit.pretty())

        return index

    @cached
    def build_commit_index(self):
        index = dict()

        for sha, builds in self.commit_build_index.items():
            for build in builds:
                index[build.id] = sha

        return index

    def commit_build_stats(self):
        builds = self.builds.build_index.values()

        stats = {
            'total': len(builds),
            'push': len([build for build in builds if build.is_push]),
            'pr': len([build for build in builds if build.is_pull_request]),
            'found_push': 0,
            'found_pr': 0
        }

        for build in builds:
            if build.id in self.build_commit_index.keys():
                if build.is_push:
                    stats['found_push'] += 1
                else:
                    stats['found_pr'] += 1

        return stats

    @cached
    def commit_count(self):
        return self.git.get_commit_count()

    @cached
    def merge_count(self):
        return len([sha for sha, parents in self.git.tree.items() if len(parents) >= 2])

    @cached
    def build_list(self):
        return list(self.builds.get_builds())

    @cached
    def pr_builds(self):
        return [build for build in self.build_list if build['event_type'] == "pull_request"]

    @cached
    def push_builds(self):
        return [build for build in self.build_list if build['event_type'] == "push"]

    @property
    def builds_count(self):
        return len(self.build_list)

    @property
    def pr_count(self):
        return len(self.pr_builds)

    @property
    def push_count(self):
        return len(self.push_builds)

    def get_push_reconstructed(self):
        builds = []

        for build in self.push_builds:

            commit = BuildRepoCommit(build['commit'])
            print(build['number'], " -- ", commit.pretty())

            if build['commit']['sha'] in self.git.topology:
                builds.append(build)
                continue

            similar = self.get_similar(build['commit'])

            if len(similar) == 1:
                builds.append(build)
                # print("found potential rebase candidate in build %s (%s)" % (build['number'], similar[0][0][:7]))
                # self.print_commit(build)

        return builds

    def print_commit(self, build):
        commit = build['commit']
        print("  %s %s: %s (%s)" %
              (commit['sha'][:7], commit['author_email'], commit['message'], commit['branch']))

    def get_similar(self, commit):
        log = self.git.log
        msg = commit['message']

        candidates = [c for c in log if c[2] == msg]

        return candidates

    def get_pr_reconstructed(self):
        prs = self.pr_builds
        log = self.git.log

        index = {}

        for build in prs:
            c = build['commit']
            cdate = as_utc_date(c['committed_at'])
            cmsg = c['message'].splitlines()[0]

            candidates = [c for c in log if c[2] == cmsg and c[1] == cdate]

            if len(candidates) == 0:
                """ was removed either by renaming or rebasing (timestamp changes) """
                continue

            c = candidates[0]
            index[c[0]] = build

        return index

    def print(self):
        print(self.slug)
        print("=" * len(self.slug))
        print("Commits: %6d" % self.commit_count)
        print("Merges:  %6d (%.2f)" % (self.merge_count, self.merge_count / self.commit_count))
        print()
        print("Builds:  %6d" % self.builds_count)
        print("Push:    %6d (%.2f)" % (self.push_count, self.push_count / self.builds_count))
        print("PullReq: %6d (%.2f)" % (self.pr_count, self.pr_count / self.builds_count))

        print()
        push_existing = self.get_push_reconstructed()
        pr_existing = self.get_pr_reconstructed()

        print("Push events in tree: %d (%.2f)" % (len(push_existing), len(push_existing) / self.push_count))
        print("Pull requests in tree: %d (%.2f)" % (len(pr_existing), len(pr_existing) / self.pr_count))


slugs = [
    "Netflix/Hystrix", "spring-projects/spring-boot", "crate/crate", "JabRef/jabref", "ReactiveX/RxAndroid"
]

slug = slugs[1]
analyzer = RepoAnalyzer(slug)
git = analyzer.git

then = time.time()
stats = analyzer.commit_build_stats()
print("took %ds" % (time.time() - then))

print("Total:", stats['total'])
print("Push: ", stats['push'])
print("PRs:  ", stats['pr'])
print()
print("Linked Push: %.3f" % (stats['found_push'] / stats['push']))
print("Linked PRs:  %.3f" % (stats['found_pr'] / stats['pr']))

exit(0)

lines = git.read("log", "--all", "--color", "--graph", "--format=%H %s")

for row in lines:
    m = re.search(r'([0-9a-z]{40})', row)

    if not m:
        print(row)
        continue

    sha = m.group(0)

    builds = analyzer.commit_build_index[sha]

    if builds:
        repl = "{} ({})".format(sha[:7], ",".join([str(build.id) for build in builds]))
    else:
        repl = sha[:7]

    print(row.replace(sha, repl))
