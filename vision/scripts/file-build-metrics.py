from vision.builds import BuildRepo
from vision.buildtopology import BuildTopologyAnalyzer
from vision.cachedump import load_git

slug = "JabRef/jabref"
repo_path = "/data/jarvis/repos/%s" % slug

builds = BuildRepo(slug)
git = load_git(repo_path)

topology = BuildTopologyAnalyzer(git, builds)

file_build_index = {k: set() for k in git.read("ls-files")}

for sha, range in topology.build_range_index.items():
    build = topology.build_index[sha]
    change_range = [git.change_index[s] for s in range]

    for changes in change_range:
        for file, change in changes.items():
            if file in file_build_index:
                file_build_index[file].add(build)

file_fail_ratios = []
for file, builds in file_build_index.items():
    all = len(builds)
    broken = len([build for build in builds if build.state in ["errored", "failed"]])
    fail_ratio = broken / all

    file_fail_ratios.append((all, fail_ratio, file))

z = len(topology.build_index)

for all, ratio, file in sorted(file_fail_ratios):
    print("%3s %.4f %s" % (all, ratio * (all / z), file))
