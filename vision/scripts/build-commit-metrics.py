from vision.builds import BuildRepo
from vision.buildtopology import BuildTopologyAnalyzer
from vision.common import as_utc_date
from vision.git import Git
from vision.metrics import CommitMetrics
from collections import Counter

slug = "spring-projects/spring-boot"
repo_path = "/data/jarvis/repos/%s" % slug

git = Git(repo_path)
builds = BuildRepo(slug)
topology = BuildTopologyAnalyzer(git, builds)

build_index = builds.create_commit_index(git)

delim = ","


def collect(attr, dictlist):
    return [d[attr] for d in dictlist]


def aggr(fn, attr, dictlist):
    return fn(collect(attr, dictlist))


def build_result(sha):
    if sha not in topology.original_errors:
        return 'passed'

    build = build_index[sha]

    if 'category' in build:
        return build['category']
    else:
        return build['state']


keys = ["size", "activity", "is_merge", "is_pr", "author",
        "num_deleted_files", "num_added_files", "num_modified_files", "num_activities",
        "previous_time_delta", "previous_state", "previous_author",
        "state"]

print(delim.join(keys))

# print(delim.join(CommitMetrics.csv_header + ['state', 'is_orig']))
for sha in git.prune_index(build_index):
    build = build_index[sha]
    is_merge = len(git.tree[sha]) > 1

    if is_merge:
        continue

    # these are the commits that were covered by the build
    build_commits = [git.get_commit(c) for c in topology.build_range_index[sha]]
    commit_metrics = [CommitMetrics(c).to_dict() for c in build_commits]
    activities = collect('activity', commit_metrics)

    # these are the shas of preceding builds
    previous_build_shas = [s for s in topology.build_history_index[sha]]

    if not previous_build_shas:
        continue  # skipping the initial build is

    pbsha = previous_build_shas[0]  # "heuristic"
    pb = build_index[pbsha]
    timedelta = as_utc_date(build['commit']['committed_at']) - as_utc_date(pb['commit']['committed_at'])

    build_metrics = {
        # 'state': 'neg' if orig else 'pos',
        'state': build_result(sha),
        'is_merge': is_merge,
        'author': build['commit']['author_email'],
        'is_pr': build['pull_request'],
        'size': len(build_commits),
        'num_deleted_files': aggr(sum, 'num_deleted_files', commit_metrics),
        'num_added_files': aggr(sum, 'num_added_files', commit_metrics),
        'num_modified_files': aggr(sum, 'num_modified_files', commit_metrics),
        'num_activities': len(set(activities)),
        'activity': Counter(activities).most_common(1)[0][0],
        'previous_time_delta': round(timedelta.seconds / 60),
        'previous_state': build_result(pbsha),
        'previous_author': pb['commit']['author_email']
    }

    print(delim.join([str(build_metrics[key]) for key in keys]))
