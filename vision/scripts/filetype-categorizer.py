from vision.builds import BuildRepo
from vision.filetypeclassifier import FileTypeClassifier
from vision.git import Git

slug = "google/iosched"
repo_path = "/data/jarvis/repos/%s" % slug

builds = BuildRepo(slug)
git = Git(repo_path)

classifier = FileTypeClassifier()

extension = [

]

classifier.extend_model(extension)

print(builds.slug, builds.get_id())

for file in git.files:
    cls = classifier.classify(file)

    if cls == "unknown":
        print(cls, file)

print('echo "%s" > /data/jarvis/filetypes/filetypes.%s.py' % (extension, builds.get_id()))
