from vision.cachedump import load_git
# slug = "spring-projects/spring-boot"
from vision.commitclassifier import CommitAnalyzer

slug = "Netflix/Hystrix"
# slug = "timmolter/XChart"
repo_path = "/data/jarvis/repos/%s" % slug

git = load_git(repo_path)
#
# for sha, patch in git.patch_index.items():
#     if len(git.tree[sha]) > 1:
#         continue
#
#     print(sha)
#     for file, diffs in patch.items():
#         print(file, diffs)
#
#     break

for commit in git.commits.values():
    analyzer = CommitAnalyzer(commit)
    if analyzer.is_merge():
        continue

    changes = analyzer.aggregate_file_type_changes()
    print(commit.sha, changes)
