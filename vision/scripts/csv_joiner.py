import csv

f1 = "/home/thomas/uni/da/data/hystrix_buildresults.csv"
f2 = "/home/thomas/uni/da/data/hystrix_commitmetrics.csv"
output = "/home/thomas/uni/da/data/hystrix_joined.csv"

commits = {}
with open(f1) as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')

    for row in reader:
        commits[row[0]] = row[1:]

with open(f2) as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')

    for row in reader:
        sha = row[0]
        if sha in commits:
            commits[sha] = commits[sha] + row[1:]

keys = ["result", "activity", "is_merge", "num_files", "num_added_files", "num_modified_files", "num_deleted_files",
        "sum_lines_added", "sum_lines_deleted", "modification_entropy"]

with open(output, 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    writer.writerow(keys)

    for vals in commits.values():
        writer.writerow(vals)
