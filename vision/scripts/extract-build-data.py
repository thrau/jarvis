from collections import Counter
from datetime import timedelta

from vision.builds import get_all_repos
from vision.buildtopology2 import BuildTopologyAnalyzer, TopoBuild
from vision.cli import require_resources
from vision.data import DataFrameFactory, SimpleColumn, TopologyAwareColumnGroup, dump_csv


class PreviousBuildResultTruncated(TopologyAwareColumnGroup):
    def get_names(self):
        return [
            "prev_left",
            "prev_right",
            "days_since_last_fail"
        ]

    def get_observation(self, build: TopoBuild):
        l, r = PreviousBuildResultTruncated.get_prev_pair(build)

        def dslf(b):
            if not b or build.started_at is None or b.started_at is None:
                return -1

            return (build.started_at - b.started_at).days

        return {
            'prev_left': l.state if l else "unknown",
            'prev_right': r.state if r else "unknown",
            'days_since_last_fail': dslf(build.last_broken)
        }

    @staticmethod
    def get_prev_pair(build):
        builds = build.prev_builds

        if len(builds) == 1:
            return builds[0], builds[0]

        if len(builds) >= 2:
            return builds[0], builds[-1]

        return None, None


class TimeOfDay(TopologyAwareColumnGroup):
    def get_names(self):
        return [
            'tod_weekday',
            'tod_hour',
        ]

    def get_observation(self, build: TopoBuild):
        date = None

        if build.is_in_tree:
            date = build.git_commit.date
        else:
            tz = self.guess_timezone(build)
            if tz:
                date = build.commit.date + timedelta(seconds=int(tz) / 100 * 60 * 60)

        if date:
            return {
                'tod_weekday': date.weekday(),
                'tod_hour': date.hour,
            }
        else:
            return {
                'tod_weekday': -1,
                'tod_hour': -1
            }

    author_cache = dict()

    def guess_timezone(self, b: TopoBuild):
        if b.commit.author_email in self.author_cache:
            return self.author_cache[b.commit.author_email]

        # reconstruct the most plausible timezone of the author
        # (map the most common tz per month, for each user, take the one closest to the month)
        commits = [commit for commit in self.topology.git.commits.values() if
                   commit.attributes['ce'] == b.commit.author_email]

        if not commits:
            return None

        cnt = Counter(([commit.date.strftime('%z') for commit in commits]))
        # TODO: group by month and check most common for month (dst precision)
        val = cnt.most_common()[0][0]

        self.author_cache[b.commit.author_email] = val

        return val


def do_extract(slug):
    topo = BuildTopologyAnalyzer(*require_resources(slug, False))

    factory = DataFrameFactory(topo)

    factory.add(SimpleColumn("id"))
    factory.add(SimpleColumn("number"))
    factory.add(SimpleColumn("is_in_tree"))
    factory.add(SimpleColumn("build_type", lambda b: b.build_type.name))
    factory.add(SimpleColumn("num_prev_builds", lambda b: len(b.prev_builds)))
    factory.add(PreviousBuildResultTruncated)
    factory.add(SimpleColumn("author", lambda b: b.build['commit']['author_email']))
    factory.add(SimpleColumn("duration"))
    factory.add(TimeOfDay)
    factory.add(SimpleColumn("is_passed"))
    factory.add(SimpleColumn("state"))

    return factory.create_data_frame(topo.builds_sorted[10:])


path = "/data/jarvis/builds"
prefix = "build_data"

slug = "spring-projects/spring-boot"
df = do_extract(slug)
print(df)
dump_csv(df, path, prefix, slug)

exit(0)

for slug in [repo.slug for repo in get_all_repos()]:
    try:
        print("extracting %s..." % slug)
        df = do_extract(slug)
        dump_csv(df, path, prefix, slug)
        print("done!")
    except Exception as e:
        print("Could not extract for %s" % slug)
        print(e)
