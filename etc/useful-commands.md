Useful commands
---------------

Just a cheat-sheet of useful commands

### Build the docker image locally

    mvn package docker:build


### Start MongoDB as daemon

    sudo /opt/mongodb-linux-x86_64-amazon-3.2.1/bin/mongod --fork --logpath /var/log/mongodb.log

add `--rest --jsonp` to expose the REST interface (add `jsonp: 'jsonp'` as AJAX request parameter)

### Run the docker image

    docker run -e JAVA_OPTS='-Xmx2g' -e "SPRING_PROFILES_ACTIVE=docker" -p 2000:2000 -t thrau/jarvis-parent

### Connect to the console

    sshpass -p "password" ssh -p 2000 user@localhost

### Drop collections

	db.getCollection("buildjsonobject").drop()
	db.getCollection("jobjsonobject").drop()
	db.getCollection("logjsonobject").drop()
	db.getCollection("repositoryjsonobject").drop()

or to drop all collections in the db

    db.getCollectionNames().forEach(function(item) { db.getCollection(item).drop() })
