
Queries
=======

Sample MongoDB queries

### Get, of a given repository, all failed jobs including their logs

    db.jobjsonobject.aggregate([
    {
        $match: {
            repository_id: 1889385,
            state: { "$ne": "passed" }
        }
    },
    {
        $limit: 1
    },
    {
        $lookup: {
            from: "logjsonobject",
            localField: "log_id",
            foreignField: "id",
            as: "build"
        }
    }
    ]).pretty()


### Get counts of different build states

    db.buildjsonobject.aggregate([
    {
        $match: {
            repository_id : repoid
        }
    },
    {
        $group: {
            _id: "$state",
            count: { $sum: 1 }
        }
    }
    ]).pretty()


or as Java code


    List<Document> pipeline = Arrays.asList(
            new Document("$match", new Document()
                    .append("repository_id", 1889385)),
            new Document("$group", new Document()
                    .append("_id", "$state")
                    .append("count", new Document("$sum", 1))));


### Get all logs that have test failures

    db.logjsonobject.find({
       body: /There are test failures\.$/
    },
    { id: 1 });

    db.logjsonobject.find({
       body: /^\[ERROR\] Failed to execute goal/
    },
    { id: 1 });


### Logs that have test failures but passed (control query)

    db.jobjsonobject.aggregate([
        {
            $lookup: {
                from: "logjsonobject",
                localField: "log_id",
                foreignField: "_id",
                as: "log"
            }
        },
        {
            $project: {
                id: 1,
                job_id: 1,
                log
            }
        },
        {
            $match: {
                "state": "passed"
            }
        },
        {
            $match: {
                "log.body": /There are test failures\.$/
            }
        }
    ]).pretty();


    db.jobjsonobject.aggregate([
        {
            $match: {
                "state": { $eq: "passed" }
            }
        },
        {
            $lookup: {
                from: "logjsonobject",
                localField: "log_id",
                foreignField: "_id",
                as: "log"
            }
        },
        {
            $match: {
                "log.body": /There are test failures\.$/
            }
        },
        {
            $project: {
                id: 1,
                log_id: 1
            }
        }
    ]).pretty();


### Group events vs state


    db.buildjsonobject.aggregate([
        {
            $match: {
                "repository_id": repoid,
                "state": {$in: ["passed", "failed", "errored"]}
            }
        },
        {
            $group: {
                _id: {
                    "event": "$event_type",
                    "state": "$state"
                },
                count: {$sum: 1}
            }
        }
    ]).toArray();


### Purge repository from collections

    var repoId = 2902072; // set id

    var logs = db.jobjsonobject.find({repository_id: repoId}, {log_id: 1}).toArray().map(function (item) {
        return item.log_id
    });

    db.logjsonobject.remove({id: {$in: logs}});
    db.jobjsonobject.remove({repository_id: repoId});
    db.buildjsonobject.remove({repository_id: repoId});
    db.repositoryjsonobject.remove({id: repoId});


### Count push-triggered builds that failed per committer

    db.buildjsonobject.aggregate([
        {
            $match: {
                "event_type": "push",
                "state": { $in: ["failed", "errored"] }
            }
        },
        {
            $group: {
                _id: "$commit.committer_email",
                count: {$sum: 1}
            }
        },
        {
            $sort: {
                "count": -1
            }
        }
    ]);


### Commit SHAs of all pull requests that failed

    db.buildjsonobject.aggregate([
        {
            $match: {
                "event_type": "pull_request",
                "state": { $in: ["failed", "errored"] }
            }
        },
        {
            $project: {
                sha: "$commit.sha"
            }
        }
    ]);

